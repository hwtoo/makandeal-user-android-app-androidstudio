package com.citrineplexus.bello2;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.util.CustomFilter;

public class FilterAdapter extends BaseEntityAdpater implements Filterable {

	private ArrayList<BaseEntity> fliterEntityAry = new ArrayList<BaseEntity>();

	// private CustomFilter customFilter;

	public FilterAdapter(Context context) {
		super(context, null);
		// customFilter = new CustomFilter();
	}

	public void resetFeatureCount() {
		// if (customFilter != null)
		CustomFilter.featureCount = 1;
	}

	public int getEntityDataCount() {
		return super.getCount();
	}
	

	@Override
	public void clearData() {
		super.clearData();
		fliterEntityAry.clear();
	}

//	public void onClearUp() {
//		if (!fliterEntityAry.isEmpty())
//			fliterEntityAry.clear();
//	}

	// @Override
	// public ArrayList<BaseEntity> getEntityList() {
	// return fliterEntityAry;
	// }

	public ArrayList<BaseEntity> getFilterEntityList() {
		return fliterEntityAry;
	}

	@Override
	public int getCount() {
		return fliterEntityAry.size();
	}

	@Override
	public Object getItem(int position) {
		return fliterEntityAry.get(position - 1);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// if (listener != null)
		// listener.loadItemNumber(position);
		return binder.getView(position, convertView, parent, context,
				fliterEntityAry, ImageLoader.getInstance(context));
	}

	@Override
	public Filter getFilter() {
		return filter;
	}

	private Filter filter = new Filter() {
		FilterResults filterResults = new FilterResults();

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			if (!fliterEntityAry.isEmpty())
				fliterEntityAry.clear();
			// for (BaseEntity entity : (ArrayList<BaseEntity>) results.values)
			// fliterEntityAry.add(entity);
			
			
			fliterEntityAry.addAll((ArrayList<BaseEntity>) results.values);
			
			// fliterEntityAry = (ArrayList<BaseEntity>) results.values;
			if (fliterEntityAry.size() > 0) {
				notifyDataSetChanged();
			} else {
				notifyDataSetInvalidated();
			}
		}

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			ArrayList<BaseEntity> tempList = new ArrayList<BaseEntity>();
			for (BaseEntity entity : getEntityList()) {
				int filterText = Integer.valueOf((String) constraint);
				int contentID = -111;
				if (filterText == -1000)
					contentID = -1000;
				else if (filterText != -111)
					contentID = 99;

				if (CustomFilter.filterOption(contentID,
						Integer.valueOf((String) constraint), entity))
					tempList.add(entity);
			}
			filterResults.values = tempList;
			filterResults.count = tempList.size();
			return filterResults;
		}
	};

}
