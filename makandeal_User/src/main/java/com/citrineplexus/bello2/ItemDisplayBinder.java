package com.citrineplexus.bello2;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.util.Calculator;
import com.citrineplexus.makandeal.util.GlobalService;
import com.citrineplexus.makandeal.view.widget.ProductImageView;

public class ItemDisplayBinder extends AdapterBaseBinder {
	private Context context = null;

	public View getView(int position, View convertView, ViewGroup parent,
			Context context, ArrayList<BaseEntity> dataSet,
			ImageLoader imgLoader) {
		this.context = context;
		BaseEntity entity = (BaseEntity) dataSet.get(position);
		if (entity.get("CAMPAIGN_NAME") == null)
			convertView = renderDealLayout(convertView, parent, dataSet,
					imgLoader, entity);
		else
			convertView = renderCrossPromo(convertView, parent, dataSet,
					imgLoader, entity);

		return convertView;
	}

	// @Override
	// public void onItemClick(int position, BaseEntity entity, Bundle param) {
	// if (entity.get("ENTITY_ID") != null) {
	// if (param == null)
	// param = new Bundle();
	// param.putString("ENTITY_ID", entity.get("ENTITY_ID").toString());
	// // TODO: open Service_Detail Page
	// // ViewController.getInstance(context).openView(
	// // ViewName.SERVICE_DETAIL, param);
	// }
	// }

	private View renderDealLayout(View convertView, ViewGroup parent,
			ArrayList<BaseEntity> dataSet, ImageLoader imgLoader,
			BaseEntity entity) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = ((LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
					.inflate(R.layout.child_category, parent, false);
			holder = new ViewHolder();
			holder.productImage = (ProductImageView) convertView
					.findViewById(R.id.productImage);
			holder.productTitle = (TextView) convertView
					.findViewById(R.id.productTitle);
			holder.currentPrice = (TextView) convertView
					.findViewById(R.id.currentPrice);
			holder.location = (TextView) convertView
					.findViewById(R.id.location);
			holder.discount = (TextView) convertView
					.findViewById(R.id.discount);
			holder.originalPrice = (TextView) convertView
					.findViewById(R.id.originalPrice);
			holder.totalSold = (TextView) convertView
					.findViewById(R.id.totalSold);
			holder.discountLinearLayout = (LinearLayout) convertView
					.findViewById(R.id.discountLinearLayout);
			holder.dotLine = (ImageView) convertView.findViewById(R.id.dotLine);
			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();

		if (holder.productImage != null)
			imgLoader.DisplayImage(entity.get("IMAGE_URL", "").toString(),
					holder.productImage, ScaleType.CENTER_CROP, false);

		if (holder.productTitle != null)
			holder.productTitle.setText(entity.get("NAME", "").toString());

		double price = Double.parseDouble(entity.get("PRICE", "0.00")
				.toString());

		if (GlobalService.checkDiscout(entity)) {
			// Deal got discount
			double specialPrice = Double.parseDouble(entity.get(
					"SPECIAL_PRICE", "0.00").toString());

			holder.originalPrice.setPaintFlags(holder.originalPrice
					.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			holder.originalPrice.setVisibility(View.VISIBLE);
			int discountValue = ((int) (100.0D - (specialPrice / price) * 100.0D));
			if (holder.currentPrice != null)
				holder.currentPrice.setText("RM "
						+ Calculator.round(specialPrice));

			if (holder.originalPrice != null)
				holder.originalPrice.setText("RM " + Calculator.round(price));

			if (holder.discount != null) {
				holder.discount.setVisibility(View.VISIBLE);
				holder.discount.setText(discountValue + "%OFF");
			}

			if (holder.totalSold != null)
				holder.totalSold.setText("/ "
						+ entity.get("FSF", "0").toString() + " sold");

			holder.discountLinearLayout.setVisibility(View.VISIBLE);
			holder.dotLine.setVisibility(View.VISIBLE);
		} else {
			if (holder.currentPrice != null)
				holder.currentPrice.setText("RM " + Calculator.round(price));

			if (holder.totalSold != null)
				holder.totalSold.setText(entity.get("FSF", "0").toString()
						+ " sold");

			holder.discountLinearLayout.setVisibility(View.GONE);
			holder.dotLine.setVisibility(View.GONE);
		}

		if (holder.location != null)
			holder.location.setText(entity.get("LOCATION", "0").toString());

		return convertView;
	}

	private View renderCrossPromo(View convertView, ViewGroup parent,
			ArrayList<BaseEntity> dataSet, ImageLoader imgLoader,
			BaseEntity entity) {
		CrossPromoHolder holder = null;
		if (convertView == null) {
			holder = new CrossPromoHolder();
			convertView = ((LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
					.inflate(R.layout.child_crosspromo, parent, false);
			holder.crossPromo = (ImageView) convertView
					.findViewById(R.id.crossPromoImageView);
			convertView.setTag(holder);
		} else
			holder = (CrossPromoHolder) convertView.getTag();

		if (holder.crossPromo != null)
			imgLoader.DisplayImage((String) entity.get("IMAGE_URL"),
					holder.crossPromo, ScaleType.CENTER_CROP, false);

		return convertView;

	}

	private class ViewHolder {
		ProductImageView productImage;
		TextView currentPrice;
		TextView productTitle;
		TextView location;
		TextView discount;
		TextView originalPrice;
		TextView totalSold;
		LinearLayout discountLinearLayout;
		ImageView dotLine;
	}

	private class CrossPromoHolder {
		ImageView crossPromo;
	}

}
