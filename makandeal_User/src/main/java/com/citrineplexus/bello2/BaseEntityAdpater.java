package com.citrineplexus.bello2;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.entity.SubItemTagEntity;

public class BaseEntityAdpater extends BaseAdapter implements OnClickListener {

	AdapterBaseBinder binder = null;
	Context context = null;
	private ArrayList<BaseEntity> entityList = new ArrayList<BaseEntity>();

	private CustomAdapterCallback listener = null;

	// private OnLoadNextPageListener listener = null;

	public BaseEntityAdpater(Context context,
			CustomAdapterCallback adapterListener) {
		this.context = context;
		this.listener = adapterListener;
	}

	public ArrayList<BaseEntity> getEntityList() {
		return entityList;
	}

	// public void setOnLoadNextPageListener(OnLoadNextPageListener
	// adapterListener) {
	// this.listener = adapterListener;
	// }

	public static abstract interface CustomAdapterCallback {
		public abstract void onClickCustomAdapterItem(int position, int idTag);

	}

	public void setDataSet(ArrayList<BaseEntity> dataSet) {
		if (dataSet != null)
			entityList = dataSet;
	}

	public void addData(BaseEntity entity) {
		if (entity.get("JSONARRAY") != null) {
			ArrayList<BaseEntity> dataAry = (ArrayList<BaseEntity>) entity
					.get("JSONARRAY");
			for (BaseEntity en : dataAry)
				addDataToEntityList(en);
		} else
			addDataToEntityList(entity);
		// PersistentData.getInstance().dataChange();
		notifyDataSetChanged();
	}

	private void addDataToEntityList(BaseEntity entity) {
		if (entity.get("ENTITY_ID") != null)
			for (int i = 0; i < entityList.size(); i++)
				if (entityList.get(i).get("ENTITY_ID")
						.equals(entity.get("ENTITY_ID")))
					return;
		entityList.add(entity);
	}

	public void addData(ArrayList<BaseEntity> entities, String uniqueKey) {
		for (BaseEntity entity : entities)
			if (entity.get(uniqueKey != null ? uniqueKey : "ENTITY_ID") != null) {
				for (int i = 0; i < entityList.size(); i++)
					if (entityList
							.get(i)
							.get(uniqueKey != null ? uniqueKey : "ENTITY_ID")
							.equals(entity.get(uniqueKey != null ? uniqueKey
									: "ENTITY_ID")))
						return;
				entityList.add(entity);
				// PersistentData.getInstance().dataChange();
				notifyDataSetChanged();
			}
	}

	public void removeData(int paramInt) {
		entityList.remove(paramInt);
		notifyDataSetChanged();
	}

	public void clearData() {
		entityList.clear();
		notifyDataSetChanged();
	}

	public void setBinder(AdapterBaseBinder customBinder) {
		binder = customBinder;
		if (customBinder != null)
			customBinder.setOnClickListener(this);
	}

	public AdapterBaseBinder getBinder() {
		return binder;
	}

	@Override
	public int getCount() {
		return entityList.size();
	}
	
	@Override
	public Object getItem(int position) {
		return entityList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// if (listener != null)
		// listener.loadItemNumber(position);

		return binder.getView(position, convertView, parent, context,
				entityList, ImageLoader.getInstance(context));
	}

	void onItemClickListener() {

	}

	public void onChange(BaseEntity paramBaseEntity, String paramString) {
		notifyDataSetChanged();
	}

	@Override
	public void onClick(View v) {
		if (listener != null)
			listener.onClickCustomAdapterItem(
					((SubItemTagEntity) v.getTag()).getPosition(),
					((SubItemTagEntity) v.getTag()).getId());
	}

}
