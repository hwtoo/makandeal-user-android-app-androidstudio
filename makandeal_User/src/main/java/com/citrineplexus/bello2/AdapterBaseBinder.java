package com.citrineplexus.bello2;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.entity.BaseEntity;

public abstract class AdapterBaseBinder {
	private View.OnClickListener onclickListener = null;

	public View.OnClickListener getOnClickListener() {
		return onclickListener;
	}

	public abstract View getView(int position, View convertView,
			ViewGroup parent, Context context, ArrayList<BaseEntity> dataSet,
			ImageLoader imgLoader);

	// TODO: param might no use.. can remove
	// public abstract void onItemClick(int position, BaseEntity entity,
	// Bundle param);

	public void setOnClickListener(OnClickListener listener) {
		onclickListener = listener;
	}

}
