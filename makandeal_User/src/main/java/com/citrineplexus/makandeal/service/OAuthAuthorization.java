package com.citrineplexus.makandeal.service;

//
//import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;

import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.callback.OAuthListener;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.util.GlobalService;
import com.citrineplexus.makandeal.util.SntpClient;
import com.citrineplexus.makandeal.view.MakanDealApplication;
import com.facebook.login.LoginManager;

import org.apache.http.client.methods.HttpUriRequest;

import java.util.Calendar;
import java.util.TimeZone;

import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
//
//import org.apache.http.client.methods.HttpUriRequest;
//
//import android.widget.Toast;
//

public class OAuthAuthorization implements OnDataRequestListener {
	private String ACCESS_TOKEN_URL;
	private String AUTH_URL;
	private String REQUEST_URL;

	private final String CALLBACK_URL = "http://makandeal.callback";

	private final String CONSUMER_KEY = "gbbcdyz3rtl0t8ywxna5wei44r0pbxgm";
	private final String CONSUMER_SECRET = "2zeaddoe93xcgwctjm4hkkgn6tpaejma";

	// For Staging
	private final String LOGIN_TOKEN_KEY_STAGING = "zbxps8usi95uaajoxv6syzkutqz1bekc";
	private final String LOGIN_TOKEN_SECRET_STAGING = "dj0b5pfrrdntvt6kth0y2ejckqhgoet5";
	// For Live
	private final String LOGIN_TOKEN_KEY = "r80wsh6gdejgq86wmdvq26fxw5xmr6bh";
	private final String LOGIN_TOKEN_SECRET = "os2m26kbgnuqnt2vdrd6lcc08ielupxj";
	private String TOKEN_KEY = "";
	private String TOKEN_SERCRET = "";
	//
	private CommonsHttpOAuthConsumer consumer = null;
	private CommonsHttpOAuthProvider provider = null;
	private static OAuthAuthorization instance = null;

	private boolean isLogin = false;
	private BaseEntity userProfile = null;
	private OAuthListener listener = null;
	// private int retryCounter = 0;
	private Context context = null;

	// private OAuthListener mOauthLister = null;

	private OAuthAuthorization() {
		ACCESS_TOKEN_URL = GlobalService.getApiUrl() + "/oauth/token";
		AUTH_URL = GlobalService.getApiUrl() + "/oauth/authorize";
		REQUEST_URL = GlobalService.getApiUrl() + "/oauth/initiate";
	}

	public static OAuthAuthorization getInstance() {
		return instance = instance == null ? new OAuthAuthorization()
				: instance;
	}

	public void setOAuthListener(OAuthListener lis) {
		this.listener = lis;
	}

	public long getUTCTime() {
		long nowAsPerDeviceTimeZone = Calendar.getInstance().getTimeInMillis();
		SntpClient sntpClient = new SntpClient();
		if (sntpClient.requestTime("0.africa.pool.ntp.org", 30000)) {
			nowAsPerDeviceTimeZone = sntpClient.getNtpTime();
			Calendar cal = Calendar.getInstance();
			TimeZone timeZoneInDevice = cal.getTimeZone();
			int differentialOfTimeZones = timeZoneInDevice.getOffset(System
					.currentTimeMillis());
			nowAsPerDeviceTimeZone -= differentialOfTimeZones;
		}
		return nowAsPerDeviceTimeZone;
	}

	public void setTokenAndSercretss(String token, String sercret) {
		TOKEN_KEY = token;
		TOKEN_SERCRET = sercret;
	}

	public void Login(final OAuthListener lis, final String token,
			final String secret) throws Exception {
		TOKEN_KEY = token;
		TOKEN_SERCRET = secret;
		if (isLogin)
			throw new Exception("User already sign in");
		new Thread(new Runnable() {
			@Override
			public void run() {
				Looper myLooper = null;
				try {
					Looper.prepare();
					consumer = getConsumer();
					// consumer = new CommonsHttpOAuthConsumer(CONSUMER_KEY,
					// CONSUMER_SECRET);
					if (token != null && secret != null) {
						consumer.setTokenWithSecret(TOKEN_KEY, TOKEN_KEY);
						isLogin = true;
						lis.oauthReady(true, null);
					} else {
						provider = new CommonsHttpOAuthProvider(REQUEST_URL,
								ACCESS_TOKEN_URL, AUTH_URL);
						provider.setOAuth10a(true);
						String token = provider.retrieveRequestToken(consumer,
								CALLBACK_URL);

						if (lis != null)
							lis.returnMessage(1, token);
					}
					myLooper = Looper.myLooper();
					Looper.loop();
				} catch (Exception e) {
					e.printStackTrace();
					logoutUser();
				} finally {
					if (myLooper != null)
						myLooper.quit();
				}
			}
		}).start();
	}

	public void Login(Context context, OAuthListener lis, String email,
			String password) {
		this.context = context;
		listener = lis;
		Bundle bundle = new Bundle();
		bundle.putBoolean("LOADING", true);
		bundle.putBoolean("GETUSERSALT", true);
		bundle.putBoolean("SIGN_ADMIN", true);
		bundle.putString("EMAIL", email);
		bundle.putString("PASSWORD", password);
		new ServiceGateway().getData(context, 1036, this, 100, bundle);
	}
	
	public void loginWithFacebook(Context context, OAuthListener lis,
			String FBToken, String FBExpires) {
		this.context = context;
		listener = lis;
		Bundle params = new Bundle();
		params.putString("FACEBOOK_TOKEN", FBToken);
		params.putString("FACEBOOK_EXPIRES", FBExpires);
		params.putBoolean("SAVE_LOGIN", true);
		params.putBoolean("SIGN_ADMIN", true);
		params.putBoolean("LOADING", true);
		new ServiceGateway().getData(context, 1035, this, 0, params);
	}

	public void signRequest(HttpUriRequest request) {
		try {
			if (!isLogin)
				throw new Exception("User not sign in, unable to sign: "
						+ request.getURI());
			CommonsHttpOAuthConsumer consumer = getConsumer();
			// CommonsHttpOAuthConsumer consumer = new CommonsHttpOAuthConsumer(
			// CONSUMER_KEY, CONSUMER_SECRET);
			consumer.setTokenWithSecret(TOKEN_KEY, TOKEN_SERCRET);
			consumer.sign(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private CommonsHttpOAuthConsumer getConsumer() {
		if (consumer == null)
			consumer = new CommonsHttpOAuthConsumer(CONSUMER_KEY,
					CONSUMER_SECRET);
		consumer.setTimeStamp(getUTCTime());
		return consumer;
	}

	public void signLoginRequest(HttpUriRequest request) {
		try {
			CommonsHttpOAuthConsumer consumer = getConsumer();
			// CommonsHttpOAuthConsumer consumer = new CommonsHttpOAuthConsumer(
			// CONSUMER_KEY, CONSUMER_SECRET);
			if (GlobalService.isLive())
				consumer.setTokenWithSecret(LOGIN_TOKEN_KEY, LOGIN_TOKEN_SECRET);
			else
				consumer.setTokenWithSecret(LOGIN_TOKEN_KEY_STAGING,
						LOGIN_TOKEN_SECRET_STAGING);

			consumer.sign(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void logoutUser() {
		isLogin = false;
		if (userProfile != null)
			userProfile.clearData();
		userProfile = null;
		MakanDealApplication.getInstance().getPreferences().edit()
				.remove("TOKEN").commit();
		MakanDealApplication.getInstance().getPreferences().edit()
				.remove("SECRET").commit();
		MakanDealApplication.getInstance().setCartItemNumber(0);

		LoginManager.getInstance().logOut();
		// Session session = Session.getActiveSession();
		// if (session != null)
		// if (session.isOpened())
		// session.closeAndClearTokenInformation();
	}

	public boolean isUserLogin() {
		return isLogin;
	}

	public void assignUserProfile(BaseEntity profile) {
		userProfile = profile;
	}

	public BaseEntity getUserProfileData() {
		return userProfile;
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		if (entity != null && entity.get("MESSAGES") != null) {
			if (entity.get("MESSAGES") != null) {
				if (listener != null)
					listener.returnMessage(-9999, entity.get("MESSAGES"));
			}
		} else {
			if (position == 1036 && entity != null) {
				if (params.get("EMAIL") != null
						|| params.get("PASSWORD") != null) {
					String password = entity.get("SALT")
							+ params.getString("PASSWORD");
					Bundle bundle = new Bundle();
					bundle.putBoolean("LOADING", true);
					bundle.putBoolean("SIGN_ADMIN", true);
					bundle.putBoolean("SAVE_LOGIN", true);
					bundle.putString("EMAIL", params.getString("EMAIL"));
					bundle.putString("PASSWORD",
							GlobalService.generateMD5(password));
					new ServiceGateway()
							.getData(context, 1037, this, 0, bundle);
					// ViewController.getInstance(context).getServiceData(1037,
					// this, 0, bundle);
				}
			} else if (position == 1037 || position == 1035) {
				if (entity != null && entity.get("SAVE_LOGIN") != null) {
					String[] tokenValue = ((String) entity.get("SAVE_LOGIN"))
							.split("---");
					isLogin = true;
					TOKEN_KEY = tokenValue[0];
					TOKEN_SERCRET = tokenValue[1];
					MakanDealApplication.getInstance().storeTokenAndSercret(
							TOKEN_KEY, TOKEN_SERCRET);
					getUserProfile(context, true);
				}
			} else if (position == 1015) {
				if (getUserProfileData() == null) {
					BaseEntity userEn = null;
					for (String key : entity.getKeySet()) {
						if (key.equalsIgnoreCase("MESSAGES"))
							return;

						userEn = (BaseEntity) entity.get(key);
						userEn.put("ID", key);
					}
					assignUserProfile(userEn);
				}
				userLoginComplete();
				context = null;
			}
		}
	}

	private void userLoginComplete() {
		MakanDealApplication.getInstance().requestUserCartItems();
		if (listener != null) {
			listener.onAcquireProfile();
			listener = null;
		}
	}

	public void getUserProfile(Context context, boolean loading) {
		if (isUserLogin() && getUserProfileData() != null) {
			userLoginComplete();
			return;
		}
		if (context == null)
			return;
		this.context = context;

		Bundle localBundle = new Bundle();
		localBundle.putBoolean("USER_INFO", true);
		localBundle.putBoolean("LOADING", loading);

		new ServiceGateway().getData(context, 1015, this, 0, localBundle);
	}

	String[] checkBillingList = { "FIRSTNAME", "LASTNAME", "EMAIL",
			"TELEPHONE", "STREET", "CITY", "POSTCODE", "REGION" };

	public boolean checkUserProfileDetail() {
		for (String key : checkBillingList) {
			if (userProfile.get(key) == null)
				return false;
		}
		return true;
	}

	// @Override
	// public void onAquireResult(
	// com.citrineplexus.makandeal.entity.BaseEntity entity, int position,
	// Bundle params) {
	//
	// }

	@Override
	public void onConnectionError(Exception ex) {

	}

}
