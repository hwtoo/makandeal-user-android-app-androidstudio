package com.citrineplexus.makandeal.service;

import android.content.Context;
import android.os.Bundle;
import android.util.Base64;

import com.citrineplexus.library.webservice.AbstractServiceGateway;
import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.util.GlobalService;
import com.citrineplexus.makandeal.view.MakanDealApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class ServiceGateway extends AbstractServiceGateway {

	public ServiceGateway() {
	}

	private String getAPIEntry(Context con) {
		return GlobalService.getApiUrl();
		// return con.getString(R.string.apiUrl);
	}

	public void getData(Context context, int contentID,
			OnDataRequestListener listener, int position, Bundle params) {
		this.context = context;
		if (params == null)
			params = new Bundle();
		params.putInt("CONTENT_ID", contentID);

		if (!isNetworkConnected(context)) {
			if (listener != null)
				listener.onConnectionError(null);
			return;
		}

		switch (contentID) {
		case 1000:// Get Latlog by Address
			getLocation(context, listener, params);
			return;

		case 1001:// EARN RM2
			postRecommendFriend(context, listener, params);
			return;

		case 1002:// DEAL
			getServiceList(context, listener, params);
			return;

		case 1003:// get deal gallery
			getProductGallery(context, listener, params);
			return;

		case 1004:// get Vendor Detail
			getVendor(context, listener, params);
			return;

		case 1005:// Get Deal Pacakage
			getDealPackage(context, listener, params);
			return;

		case 1008:// PURCHASED HISTORY ALL
			getPurchasedHistory(context, listener, params);
			return;

		case 1010:// GET WISHLIST
			getWishlist(context, listener, params);
			return;

		case 1011:// REMOVE WISHLIST
			deleteWishlist(context, listener, params);
			return;

		case 1012:// SERVICE DETAIL
			getServiceDetail(context, listener, params, null);
			return;

		case 1013:// GET COUPON
			getUserCoupon(context, listener, params);
			break;

		case 1014:// CHANGE CART NUMBER
			changeCartItemNum(context, listener, params);
			break;

		case 1015:
			getUserInfo(context, listener, params);
			return;

		case 1016:// REMOVE CART ITEM
			removeCartItem(context, listener, params);
			return;

		case 1017:// GET CART COUPON CODE
			getCartCoupon(context, listener, params);
			return;

		case 1018:// GET PRODUCT CHILD EXT
			getChildBranches(context, listener, params);
			return;

		case 1019:// GET MERCHANT BRANCH
			getMerchantBranches(context, listener, params);
			return;

		case 1020:// GET CART ITEM NUM
			getCartItems(context, listener, params);
			return;

		case 1021:// GET USER ADDRESS (REQUIRE CALL 1015 FOR ID BEFORE THIS)
			getUserAddress(context, listener, params);
			return;

		case 1026:// SEARCH SERVICE VOUCHER
			searchCoupon(context, listener, params);
			return;

		case 1027:// SERVICE DETAIL FINEPRINT
			getServiceFinePrint(context, listener, params);
			return;

		case 1030:// GET MERCHANT STORE
			getMerchantStore(context, listener, params);
			return;

		case 1031:// GET PRODUCT DESCRIPTION
			getProductDescription(context, listener, params);
			return;

		case 1032:// GET SUGGESTION LIST
			getSuggestList(context, listener, params);
			return;

		case 1033:// PUT USER GCM DEVICE ID
			putUserDeviceId(context, listener, params);
			return;

		case 1034:// UNREGISTER USER GCM DEVICE ID
			unregisterUserDeviceId(context, listener, params);
			return;

		case 1035:// LOGIN WITH FACEBOOK
			loginFacebook(context, listener, params);
			return;

		case 1036:// GET USER SALT STRING
			getUserSalt(context, listener, params);
			return;

		case 1037:// USER LOGIN
			userLogin(context, listener, params);
			return;

		case 1038:// RESET PASSWORD
			forgotPassword(context, listener, params);
			return;

		case 1039:// USER REGISTER
			userRegister(context, listener, params);
			return;

		case 1041:// ADD TO WISHLIST
			addWishlist(context, listener, params);
			return;

		case 1042:// ADD TO CART
			addToCart(context, listener, params);
			return;

		case 1050:// Get Event
			getCampaign(context, listener, params);
			return;

		case 1051:// Get SubCategory
			getSubCategory(context, listener, params);
			return;

		case 1052:// Get CampaignProductExt
			getCampaignProductExt(context, listener, params);
			return;

		case 1053:// Checkout
//			 checkOut(context, listener, params);
			return;

		case 1054:// put Coupon
			putCoupon(context, listener, params);
			return;

		case 1055:// remove all Coupon
			deleteAllCoupon(context, listener, params);
			return;
		}
	}

	private void getLocation(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam == null || extraParam.getString("ADDRESS") == null)
			return;
		String address = "";
		try {
			address = URLEncoder.encode(extraParam.getString("ADDRESS"),
					"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		new RequestAsyncTask().execute(Request.GET, listener,
				"https://maps.googleapis.com/maps/api/geocode/json?address="
						+ address + "&key="
						+ MakanDealApplication.getInstance().getGoogleApiKey(),
				extraParam.getInt("CONTENT_ID", -1), extraParam);
	}

	public void getPurchasedHistory(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		if (!OAuthAuthorization.getInstance().isUserLogin()
				|| extraParam.getInt("CONTENT_ID", -1) == -1)
			return;

		new RequestAsyncTask().execute(new Object[] { Request.GET, listener,
				getAPIEntry(context) + "/api/rest/purchasehistory",
				extraParam.getInt("CONTENT_ID"), extraParam });

	}

	public void getWishlist(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (!OAuthAuthorization.getInstance().isUserLogin())
			return;
		this.context = context;
		new RequestAsyncTask().execute(new Object[] { Request.GET, listener,
				getAPIEntry(context) + "/api/rest/wishlists?imgSize=200x150",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void deleteWishlist(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (!OAuthAuthorization.getInstance().isUserLogin())
			return;
		new RequestAsyncTask().execute(new Object[] {
				Request.DELETE,
				listener,
				(getAPIEntry(context) + "/api/rest/wishlists/" + extraParam
						.getString("ENTITY_ID")),
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void getServiceDetail(Context context,
			OnDataRequestListener listener, Bundle extraParam, BaseEntity entity) {
		if (extraParam == null || extraParam.getString("ENTITY_ID") == null)
			return;
		this.context = context;
		new RequestAsyncTask()
				.execute(new Object[] {
						Request.GET,
						listener,
						(getAPIEntry(context) + "/api/rest/productext/"
								+ extraParam.getString("ENTITY_ID") + "?imgSize=405x300"),
						1012, extraParam, entity });
	}

	public void getUserCoupon(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (!OAuthAuthorization.getInstance().isUserLogin())
			return;
		this.context = context;
		new RequestAsyncTask().execute(new Object[] { Request.GET, listener,
				getAPIEntry(context) + "/api/rest/customercoupon",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	private void getUserInfo(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (!OAuthAuthorization.getInstance().isUserLogin())
			return;

		new RequestAsyncTask().execute(new Object[] { Request.GET, listener,
				getAPIEntry(context) + "/api/rest/customers",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void removeCartItem(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam == null || extraParam.getString("ITEM_ID") == null)
			return;
		new RequestAsyncTask().execute(new Object[] {
				Request.DELETE,
				listener,
				getAPIEntry(context) + "/api/rest/cart/"
						+ extraParam.getString("ITEM_ID"),
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void getCartCoupon(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam.getString("QUOTE_ID") == null
				|| extraParam.getInt("CONTENT_ID", -1) == -1)
			return;

		new RequestAsyncTask().execute(new Object[] {
				Request.GET,
				listener,
				getAPIEntry(context) + "/api/rest/cart/"
						+ extraParam.getString("QUOTE_ID") + "/coupon",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	private void postRecommendFriend(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		if (!OAuthAuthorization.getInstance().isUserLogin())
			return;

		new RequestAsyncTask().execute(new Object[] { Request.GET, listener,
				getAPIEntry(context) + "/api/rest/sendfriendurl",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	private void getServiceList(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		if (extraParam == null || extraParam.getString("PAGE") == null)
			return;
		this.context = context;
		new RequestAsyncTask()
				.execute(new Object[] {
						Request.GET,
						listener,
						(getAPIEntry(context) + "/api/rest/productext/?page="
								+ extraParam.getString("PAGE") + "&imgSize=405x300&limit=50"),
						extraParam.getInt("CONTENT_ID"), extraParam });
	}

	private void getProductGallery(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		if (extraParam == null || extraParam.getString("ENTITY_ID") == null)
			return;
		new RequestAsyncTask()
				.execute(new Object[] {
						Request.GET,
						listener,
						(getAPIEntry(context) + "/api/rest/productimage/"
								+ extraParam.getString("ENTITY_ID") + "?imgSize=680x400"),
						extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void getVendor(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam == null || extraParam.getString("VENDOR_ID") == null)
			return;
		new RequestAsyncTask()
				.execute(new Object[] {
						Request.GET,
						listener,
						(getAPIEntry(context) + "/api/rest/merchants/"
								+ extraParam.getString("VENDOR_ID") + "?imgSize=680x400"),
						extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void getDealPackage(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam == null || extraParam.getString("ENTITY_ID") == null)
			return;
		new RequestAsyncTask()
				.execute(new Object[] {
						Request.GET,
						listener,
						(getAPIEntry(context) + "/api/rest/productchildext/" + extraParam
								.getString("ENTITY_ID")),
						extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void getMerchantStore(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		if (extraParam == null || extraParam.getString("VENDOR_ID") == null)
			return;

		try {
			JSONObject obj = new JSONObject();
			// obj.put("VENDOR_ID", extraParam.getString("VENDOR_ID"));
			extraParam.putString("POST_ENTITY", obj.toString());
			new RequestAsyncTask().execute(new Object[] {
					Request.GET,
					listener,
					getAPIEntry(context) + "/api/rest/merchantproduct/"
							+ extraParam.getString("VENDOR_ID")
							+ "?imgSize=405x300",
					extraParam.getInt("CONTENT_ID"), extraParam });
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getProductDescription(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		if (extraParam == null || extraParam.getString("ENTITY_ID") == null)
			return;
		new RequestAsyncTask().execute(new Object[] {
				Request.GET,
				listener,
				getAPIEntry(context) + "/api/rest/productdescription/"
						+ extraParam.getString("ENTITY_ID"),
				extraParam.getInt("CONTENT_ID", -1), extraParam });
	}

	private void getUserSalt(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam == null || extraParam.getString("EMAIL") == null)
			return;
		new RequestAsyncTask().execute(new Object[] {
				Request.GET,
				listener,
				getAPIEntry(context) + "/api/rest/customers/"
						+ extraParam.getString("EMAIL", ""),
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	private void userLogin(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam.get("EMAIL") == null
				|| extraParam.get("PASSWORD") == null)
			return;
		try {
			JSONObject obj = new JSONObject();
			obj.put("email", extraParam.getString("EMAIL"));
			obj.put("hash", extraParam.getString("PASSWORD"));
			System.out.println("%%%%% login obj = "+obj.toString());
			extraParam.putString("POST_ENTITY", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		new RequestAsyncTask().execute(new Object[] { Request.POST, listener,
				getAPIEntry(context) + "/api/rest/customerlogin",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void forgotPassword(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam.get("EMAIL") == null)
			return;
		try {
			JSONObject obj = new JSONObject();
			obj.put("email", extraParam.getString("EMAIL"));
			extraParam.putString("POST_ENTITY", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		new RequestAsyncTask().execute(new Object[] { Request.POST, listener,
				getAPIEntry(context) + "/api/rest/customerPasswordPost",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	private void userRegister(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam.get("EMAIL") == null
				|| extraParam.get("PASSWORD") == null
				|| extraParam.get("FIRSTNAME") == null
				|| extraParam.get("LASTNAME") == null)
			return;

		try {
			JSONObject obj = new JSONObject();
			obj.put("email", extraParam.getString("EMAIL"));
			obj.put("encrypted_password", Base64.encodeToString(extraParam
					.getString("PASSWORD").getBytes("UTF-8"), Base64.DEFAULT));
			obj.put("firstname", extraParam.getString("FIRSTNAME"));
			obj.put("lastname", extraParam.getString("LASTNAME"));
			extraParam.putString("POST_ENTITY", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		new RequestAsyncTask().execute(new Object[] { Request.POST, listener,
				getAPIEntry(context) + "/api/rest/customers",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	private void loginFacebook(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam == null
				|| extraParam.getString("FACEBOOK_TOKEN") == null)
			return;

		try {
			JSONObject obj = new JSONObject();
			obj.put("access_token", extraParam.getString("FACEBOOK_TOKEN"));
			obj.put("expires", extraParam.getString("FACEBOOK_EXPIRES"));
			// obj.put("customerId", extraParam.getString("CUSTOMER_ID"));
			extraParam.putString("POST_ENTITY", obj.toString());

			new RequestAsyncTask().execute(new Object[] { Request.POST,
					listener,
					getAPIEntry(context) + "/api/rest/customerfblogin",
					extraParam.getInt("CONTENT_ID", -1), extraParam });
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getChildBranches(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		if (extraParam == null
				|| extraParam.getString("UDROPSHIP_VENDOR") == null)
			return;

		// new RequestAsyncTask()
		// .execute(new Object[] {
		// Request.GET,
		// listener,
		// (getAPIEntry(context) + "/api/rest/productchildext/"
		// + extraParam.getString("UDROPSHIP_VENDOR") + "/branches"),
		// extraParam.getInt("CONTENT_ID", -1), extraParam });
		// TODO : stage 2
		new RequestAsyncTask()
				.execute(new Object[] {
						Request.GET,
						listener,
						(getAPIEntry(context) + "/api/rest/merchants/"
								+ extraParam.getString("UDROPSHIP_VENDOR") + "/branches"),
						extraParam.getInt("CONTENT_ID", -1), extraParam });
	}

	public void getMerchantBranches(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		if (extraParam == null
				|| extraParam.getString("UDROPSHIP_VENDOR") == null)
			return;
		new RequestAsyncTask()
				.execute(new Object[] {
						Request.GET,
						listener,
						(getAPIEntry(context) + "/api/rest/merchants/"
								+ extraParam.getString("UDROPSHIP_VENDOR") + "/branches"),
						1012, extraParam });
	}

	private void getCartItems(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		new RequestAsyncTask().execute(new Object[] { Request.GET, listener,
				getAPIEntry(context) + "/api/rest/cart", 1020, extraParam });
	}

	public void getUserAddress(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam == null || extraParam.get("USER_ID") == null)
			return;
		new RequestAsyncTask().execute(new Object[] {
				Request.GET,
				listener,
				getAPIEntry(context) + "/api/rest/customers/"
						+ extraParam.get("USER_ID") + "/addresses",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	private void searchCoupon(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam.getString("SEARCH_TEXT").length() <= 0
				|| extraParam.getInt("CONTENT_ID", -1) == -1)
			return;

		new RequestAsyncTask()
				.execute(new Object[] {
						Request.GET,
						listener,
						(getAPIEntry(context) + "/api/rest/searchengine/?q="
								+ extraParam.getString("SEARCH_TEXT") + "&imgSize=405x300&order=relevance"),
						extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void getServiceFinePrint(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		if (extraParam.getString("ENTITY_ID") == null
				|| extraParam.getInt("CONTENT_ID", -1) == -1)
			return;
		new RequestAsyncTask().execute(new Object[] {
				Request.GET,
				listener,
				(getAPIEntry(context) + "/api/rest/fineprint/" + extraParam
						.getString("ENTITY_ID")),
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void getSuggestList(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		new RequestAsyncTask().execute(new Object[] { Request.GET, listener,
				getAPIEntry(context) + "/api/rest/searchterms",
				extraParam.getInt("CONTENT_ID", -1), extraParam });
	}

	public void putUserDeviceId(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		try {
			JSONObject obj = new JSONObject();
			JSONObject inforObj = new JSONObject();
			inforObj.put("username", extraParam.getString("USER_EMAIL"));
			inforObj.put("customerId", extraParam.getString("CUSTOMER_ID"));
			inforObj.put("cmDeviceId", extraParam.getString("DEVICE_ID"));
			inforObj.put("devicePlatform", extraParam.getInt("DEVICE_PLATFORM"));

			obj.put("device", inforObj);
			extraParam.putString("POST_ENTITY", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		new RequestAsyncTask()
				.execute(new Object[] {
						Request.POST,
						listener,
						GlobalService.getNotificationHostNam()
								+ context.getResources().getString(
										R.string.notificationURL)
								+ "/devices/register",
						extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void unregisterUserDeviceId(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		try {
			JSONObject obj = new JSONObject();

			JSONObject inforObj = new JSONObject();
			inforObj.put("cmDeviceId", extraParam.getString("DEVICE_ID"));
			obj.put("device", inforObj);
			extraParam.putString("POST_ENTITY", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		new RequestAsyncTask().execute(new Object[] {
				Request.POST,
				listener,
				GlobalService.getNotificationHostNam()
						+ context.getResources().getString(
								R.string.notificationURL)
						+ "/devices/unregister",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void addWishlist(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (!OAuthAuthorization.getInstance().isUserLogin())
			return;
		if (extraParam == null || extraParam.getString("ENTITY_ID") == null
				|| extraParam.getString("QTY") == null)
			return;
		this.context = context;

		try {
			JSONObject obj = new JSONObject();
			obj.put("entity_id", extraParam.getString("ENTITY_ID"));
			obj.put("qty", extraParam.getString("QTY"));
			extraParam.putString("POST_ENTITY", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		new RequestAsyncTask().execute(new Object[] { Request.POST, listener,
				getAPIEntry(context) + "/api/rest/wishlists",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void getCampaign(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam == null || extraParam.get("Campaign_Event") == null)
			return;

		this.context = context;
		String campaign = "";

		if (extraParam.getBoolean("Campaign_Event"))
			campaign = "event";
		else
			campaign = "promo";
		new RequestAsyncTask().execute(new Object[] {
				Request.GET,
				listener,
				getAPIEntry(context) + "/api/rest/Campaign?campaignType="
						+ campaign + "&deviceId=1&imgSize=680x400",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void getCampaignProductExt(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		if (extraParam == null || extraParam.get("Campaign_Event") == null
				|| extraParam.get("Campaign_View") == null
				|| extraParam.get("PAGE") == null)
			return;

		this.context = context;

		new RequestAsyncTask().execute(new Object[] {
				Request.GET,
				listener,
				getAPIEntry(context) + "/api/rest/Campaign/"
						+ extraParam.getString("Campaign_View")
						+ "?campaignType="
						+ extraParam.getString("Campaign_Event")
						+ "&deviceId=1&imgSize=405x300&page="
						+ extraParam.getString("PAGE"),
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void getSubCategory(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		new RequestAsyncTask().execute(new Object[] { Request.GET, listener,
				getAPIEntry(context) + "/api/rest/subcategory",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void addToCart(Context context, OnDataRequestListener listener,
			Bundle param) {
		this.context = context;
		if (param == null || param.getString("ENTITY_ID") == null
				|| param.getString("QTY") == null
				|| param.getString("REDEEM_LOCATION") == null)
			return;

		String qty = param.getString("QTY"), item_id = null;
		if (MakanDealApplication.getInstance().getCartItem() != null)
			for (BaseEntity en : MakanDealApplication.getInstance()
					.getCartItem()) {
				if (en.get("ENTITY_ID", "").equals(param.get("ENTITY_ID"))
						&& en.get("REDEEM_LOCATION", "").equals(
								param.get("REDEEM_LOCATION"))) {
					qty = String.valueOf((Integer) en.get("QTY", 0)
							+ Integer.parseInt(param.getString("QTY")));
					item_id = (String) en.get("ITEM_ID", null);
					break;
				}
			}

		if (item_id != null) {
			param.remove("QTY");
			param.putString("QTY", qty);
			param.putString("ITEM_ID", item_id);
			changeCartItemNum(context, listener, param);
			return;
		}

		try {
			JSONObject obj = new JSONObject();
			obj.put("entity_id", param.getString("ENTITY_ID"));
			obj.put("qty", qty);
			obj.put("redeem_location", param.getString("REDEEM_LOCATION"));
			param.putString("POST_ENTITY", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		param.putBoolean("SAVE_QUOTE", true);

		new RequestAsyncTask().execute(new Object[] { Request.POST, listener,
				getAPIEntry(context) + "/api/rest/cart/",
				param.getInt("CONTENT_ID"), param });
	}

	public void changeCartItemNum(Context context,
			OnDataRequestListener listener, Bundle param) {
		this.context = context;
		String quoteId = null;

		if (OAuthAuthorization.getInstance().isUserLogin()
				&& OAuthAuthorization.getInstance().getUserProfileData() != null
				&& OAuthAuthorization.getInstance().getUserProfileData()
						.get("QUOTE_ID") != null)
			quoteId = OAuthAuthorization.getInstance().getUserProfileData()
					.get("QUOTE_ID").toString();

		if (param == null || param.getString("ENTITY_ID") == null
				|| param.getString("QTY") == null
				|| param.getString("REDEEM_LOCATION") == null
				|| param.getString("ITEM_ID") == null || quoteId == null)
			return;

		try {
			JSONObject obj = new JSONObject();
			obj.put("entity_id", param.getString("ENTITY_ID"));
			obj.put("qty", param.getString("QTY"));
			if (!OAuthAuthorization.getInstance().isUserLogin())
				if (quoteId != null)
					obj.put("quote_id", quoteId);
			obj.put("redeem_location", param.getString("REDEEM_LOCATION"));
			param.putString("POST_ENTITY", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (quoteId != null) {
			new RequestAsyncTask().execute(new Object[] {
					Request.PUT,
					listener,
					getAPIEntry(context) + "/api/rest/cart/" + quoteId
							+ "/items/" + param.getString("ITEM_ID"),
					param.getInt("CONTENT_ID"), param });
		}
	}

	public void checkOut(Context context, OnDataRequestListener listener,
			Bundle param, BaseEntity enti) {
		this.context = context;
		for (String value : new String[] { "PAYMENT_METHOD", "QUOTE_ID",
		/*
		 * "EMAIL", "FIRSTNAME", "LASTNAME", "REGION_ID", "TELEPHONE", "STREET"
		 */}) {
			if (enti.get(value) == null)
				return;
		}

		try {
			JSONObject obj = new JSONObject();
			obj.put("payment_method", enti.get("PAYMENT_METHOD"));
			obj.put("quote_id", enti.get("QUOTE_ID").toString());

			for (String value : new String[] { "EMAIL", "FIRSTNAME",
					"LASTNAME", "REGION_ID", "CITY", "POSTCODE", "COUNTRY_ID",
					"TELEPHONE" }) {
				if (enti.get(value) != null)
					obj.put(value.toLowerCase(), enti.get(value).toString());
				// return;
			}

			JSONArray addressAray = new JSONArray();
			if (enti.get("STREET") != null)
				addressAray.put(enti.get("STREET").toString());
			if (enti.get("STREET2") != null)
				addressAray.put(enti.get("STREET2").toString());

			obj.put("street", addressAray);

			// obj.put("email", enti.get("EMAIL").toString());
			// obj.put("firstname", enti.get("FIRSTNAME").toString());
			// obj.put("lastname", enti.get("LASTNAME").toString());
			// obj.put("region_id", (Integer) enti.get("REGION_ID"));
			// obj.put("city", enti.get("CITY").toString());
			// obj.put("postcode", enti.get("POSTCODE").toString());
			// obj.put("country_id", enti.get("COUNTRY_ID").toString());
			// obj.put("telephone", enti.get("TELEPHONE").toString());
			// // obj.put("street", new String[] {
			// enti.get("STREET").toString()});
			// if (enti.get("STREET") instanceof ArrayList<?>) {
			// ArrayList<String> streetAry = (ArrayList<String>) enti
			// .get("STREET");
			// obj.put("street", streetAry.get(0) + streetAry.get(1));
			// } else
			// obj.put("street", enti.get("STREET"));
			// obj.put("street", enti.get("STREET").toString());
			param.putString("POST_ENTITY", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

		param.putBoolean("SAVE_ORDER", true);
		new RequestAsyncTask().execute(new Object[] { Request.POST, listener,
				getAPIEntry(context) + "/api/rest/v2/checkout",
				param.getInt("CONTENT_ID"), param });

		if (OAuthAuthorization.getInstance().getUserProfileData() != null)
			OAuthAuthorization.getInstance().getUserProfileData()
					.remove("QUOTE_ID");
	}

	public void putCoupon(Context context, OnDataRequestListener listener,
			Bundle extraParam) {
		if (extraParam == null || extraParam.getString("COUPON_CODE") == null
				|| extraParam.getInt("CONTENT_ID", -1) == -1
				|| extraParam.getString("QUOTE_ID") == null)
			return;
		try {
			JSONObject obj = new JSONObject();
			obj.put("coupon_code", extraParam.getString("COUPON_CODE"));
			extraParam.putString("POST_ENTITY", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		new RequestAsyncTask().execute(new Object[] {
				Request.PUT,
				listener,
				getAPIEntry(context) + "/api/rest/cart/"
						+ extraParam.getString("QUOTE_ID") + "/coupon/"
						+ extraParam.getString("COUPON_CODE"),
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

	public void deleteAllCoupon(Context context,
			OnDataRequestListener listener, Bundle extraParam) {
		if (extraParam.getString("QUOTE_ID") == null
				|| extraParam.getInt("CONTENT_ID", -1) == -1)
			return;

		try {
			JSONObject obj = new JSONObject();
			obj.put("coupon_code", "");
			extraParam.putString("POST_ENTITY", obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

		new RequestAsyncTask().execute(new Object[] {
				Request.PUT,
				listener,
				getAPIEntry(context) + "/api/rest/cart/"
						+ extraParam.getString("QUOTE_ID") + "/coupon/empty",
				extraParam.getInt("CONTENT_ID"), extraParam });
	}

}
