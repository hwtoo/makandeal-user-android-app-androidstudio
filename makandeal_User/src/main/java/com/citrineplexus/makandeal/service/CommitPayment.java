package com.citrineplexus.makandeal.service;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.util.GlobalService;
import com.citrineplexus.makandeal.view.MakanDealApplication;
import com.ipay.Ipay;
import com.ipay.IpayPayment;
import com.ipay.IpayResultDelegate;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalInvoiceData;
import com.paypal.android.MEP.PayPalInvoiceItem;
import com.paypal.android.MEP.PayPalPayment;
import com.paypal.android.MEP.PayPalResultDelegate;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class CommitPayment {
    private static CommitPayment instance = null;
    private PaymentStatusCallback listener = null;

    public enum PaymentType {
        BANK_TRANSFER, CREDIT_CARD, PAYPAL
    }

    public enum PaymentStatus {
        FAILED, CANCELED, SUCCEEDED
    }

    public interface PaymentStatusCallback {
        public void onPaymentStatus(PaymentStatus status);
    }

    private CommitPayment() {

    }

    public static CommitPayment getInstance() {
        return instance = instance == null ? new CommitPayment() : instance;
    }

    public void registerPaymentResult(PaymentStatusCallback callback) {
        listener = callback;
    }

    private void fireCallback(PaymentStatus status) {
        if (instance != null && instance.listener != null)
            instance.listener.onPaymentStatus(status);
    }

    public void payNow(Context context, BaseEntity entity, PaymentType type) {
        GoogleAnalyticsLib.getInstance(context).trackEcommerceTransaction(
                entity);

        switch (type) {
            case BANK_TRANSFER:
                checkoutWithIPay88(context, entity, "16");
                return;
            case CREDIT_CARD:
                checkoutWithIPay88(context, entity, "2");
                return;
            case PAYPAL:
                checkoutWithPaypal(context, entity);
                return;
        }
    }

    private void checkoutWithIPay88(Context context, BaseEntity en,
                                    String paymentID) {
        try {
            if (context == null)
                throw new Exception("Context null");
            IpayPayment payment = new IpayPayment();
            BaseEntity checkoutEntitny = (BaseEntity) en.get("DATA");

            List<BaseEntity> entityAry = MakanDealApplication.getInstance()
                    .getCartItem();
            StringBuffer detailBuffer = new StringBuffer();
            for (int i = 0; i < entityAry.size(); i++) {
                BaseEntity baseEntity = entityAry.get(i);

                GoogleAnalyticsLib.getInstance(context).trackEcommerceItem(
                        baseEntity);

                detailBuffer.append((i + 1) + ". " + baseEntity.get("SKU")
                        + " Qty: " + baseEntity.get("QTY"));

                if ((i + 1) > entityAry.size())
                    detailBuffer.append(" ");
            }

            if (GlobalService.isLive()) {
                // live
                payment.setMerchantCode("M06492_S0004");
                payment.setMerchantKey("pmM72xrUvx");
            } else {
                // sandbox
//			payment.setMerchantCode("M05976");
//			payment.setMerchantKey("vmPLC6MGM4");
                payment.setMerchantCode("M00566");
                payment.setMerchantKey("vTDnaJ3Ler");
            }

            payment.setPaymentId(paymentID);
            payment.setCurrency("MYR");
            payment.setLang("ISO-8859-1");

            payment.setRemark(detailBuffer.toString());

            payment.setRefNo(checkoutEntitny.get("INCREMENT_ID").toString());
            payment.setAmount(checkoutEntitny.get("GRAND_TOTAL").toString());
            payment.setProdDesc("Makandeal Voucher");
            payment.setUserName(en.get("FIRSTNAME", "").toString() + " "
                    + en.get("LASTNAME", "").toString());
            payment.setUserEmail(en.get("EMAIL", "").toString());
            payment.setUserContact(en.get("TELEPHONE", "").toString());

            payment.setCountry("MY");
            // payment.setBackendPostURL(context.getString(R.string.apiUrl)
            // + "/ipay88/processing/status");
            payment.setBackendPostURL(context.getString(R.string.apiUrl)
                    + "/ipay88/processing/backendPost");

            context.startActivity(Ipay.getInstance().checkout(payment, context,
                    new ResultHandlerIPay88()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class ResultHandlerIPay88 implements IpayResultDelegate,
            Serializable {
        private static final long serialVersionUID = 10001L;

        @Override
        public void onPaymentCanceled(String TransId, String RefNo,
                                      String Amount, String Remark, String ErrDesc) {
            Log.e("makandeal", "<Ipay88 Payment Canceled> Error :" + ErrDesc);
            // CommitPayment.getInstance().closeLoading();
            CommitPayment.getInstance().fireCallback(PaymentStatus.CANCELED);
        }

        @Override
        public void onPaymentFailed(String TransId, String RefNo,
                                    String Amount, String Remark, String ErrDesc) {
            Log.e("makandeal", "<Ipay88 Payment Fail> Error :" + ErrDesc);
            // CommitPayment.getInstance().closeLoading();
            CommitPayment.getInstance().fireCallback(PaymentStatus.FAILED);
        }

        @Override
        public void onPaymentSucceeded(String TransId, String RefNo,
                                       String Amount, String Remark, String AuthCode) {
            Log.e("makandeal", "AuthCode =" + AuthCode);
            Log.e("makandeal", "Remark =" + Remark);
            Log.e("makandeal", "RefNo =" + RefNo);
            Log.e("makandeal", "TransId =" + TransId);
            Log.e("makandeal", "<Ipay88 Payment Succeeded>");
            // CommitPayment.getInstance().closeLoading();
            CommitPayment.getInstance().fireCallback(PaymentStatus.SUCCEEDED);
        }

        @Override
        public void onRequeryResult(String MerchantCode, String RefNo,
                                    String Amount, String Result) {
            Log.e("makandeal", "Merchnat Code =" + MerchantCode);
            Log.e("makandeal", "RefNo =" + RefNo);
            Log.e("makandeal", "Result =" + Result);

        }

        private void writeObject(ObjectOutputStream out) throws IOException {

        }

        private void readObject(ObjectInputStream in) throws IOException,
                ClassNotFoundException {

        }

    }

    private void checkoutWithPaypal(Context context, BaseEntity en) {
        try {
            if (context == null)
                throw new Exception("Context null");

            PayPalPayment payment = new PayPalPayment();
            BaseEntity checkoutEntitny = (BaseEntity) en.get("DATA");

            List<BaseEntity> entityAry = MakanDealApplication.getInstance()
                    .getCartItem();
            StringBuffer detailBuffer = new StringBuffer();
            for (int i = 0; i < entityAry.size(); i++) {
                BaseEntity baseEntity = entityAry.get(i);

                GoogleAnalyticsLib.getInstance(context).trackEcommerceItem(
                        baseEntity);

                detailBuffer.append((i + 1) + ". " + baseEntity.get("SKU")
                        + " Qty: " + baseEntity.get("QTY"));
                if (i + 1 > entityAry.size())
                    detailBuffer.append(", ");
            }

            if (GlobalService.isLive()) {
                // live
                payment.setCurrencyType("MYR");
                payment.setRecipient("payment@citrineplexus.com");
            } else {
                // sandbox
                payment.setCurrencyType("USD");
                // payment.setRecipient("payment-facilitator@citrineplexus.com");
//				payment.setRecipient("binary.scout-facilitator@gmail.com");
                // payment.setRecipient("noob_THW-facilitator@hotmail.com");
                // payment.setRecipient("account-facilitator@ozuraworld.com");
                payment.setRecipient("binary.makandeal-facilitator@gmail.com");
            }

//            payment.setSubtotal(new BigDecimal((Double) checkoutEntitny.get("GRAND_TOTAL")));
            payment.setSubtotal(new BigDecimal((Double) checkoutEntitny.get("GRAND_TOTAL")));
            payment.setPaymentType(PayPal.PAYMENT_TYPE_NONE);
            PayPalInvoiceData invoice = new PayPalInvoiceData();


            PayPalInvoiceItem item1 = new PayPalInvoiceItem();
            item1.setName("Makandeal Voucher");
            item1.setID(checkoutEntitny.get("INCREMENT_ID").toString());
            item1.setTotalPrice(new BigDecimal((Double) checkoutEntitny.get("GRAND_TOTAL")));
            item1.setUnitPrice(new BigDecimal((Double) checkoutEntitny.get("GRAND_TOTAL")));
            item1.setQuantity(1);
            invoice.getInvoiceItems().add(item1);

            payment.setInvoiceData(invoice);
            payment.setMerchantName("Citrine Plexus Sdn Bdh");
            payment.setCustomID(checkoutEntitny.get("INCREMENT_ID").toString());
            payment.setMemo(detailBuffer.toString());

            PayPal pp = PayPal.getInstance();
            if (pp == null) {
                if (GlobalService.isLive())
                    // Live
                    pp = PayPal.initWithAppID(context, "APP-8GE93976PV7069235",
                            PayPal.ENV_LIVE);
                else
                    // SandBox
                    pp = PayPal.initWithAppID(context, "APP-80W284485P519543T",
                            PayPal.ENV_SANDBOX);
            }
            pp.setLanguage("en_US");
            payment.setIpnUrl("http://staging.makandeal.com/paypal/ipn/");

            Intent paypalIntent = pp.checkout(payment, context,
                    new ResultHandlerPaypal());
            // context.startActivityForResult(paypalIntent, 1);

            // ViewController.getInstance(context).closeLoading();
            context.startActivity(paypalIntent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class ResultHandlerPaypal implements PayPalResultDelegate,
            Serializable {
        private static final long serialVersionUID = 10001L;

        public ResultHandlerPaypal() {
        }

        @Override
        public void onPaymentCanceled(String paymentStatus) {
            Log.e("makandeal", "<Paypal Payment Canceled> Error :"
                    + paymentStatus);
            // CommitPayment.getInstance().closeLoading();
            CommitPayment.getInstance().fireCallback(PaymentStatus.CANCELED);
        }

        @Override
        public void onPaymentFailed(String paymentStatus, String correlationID,
                                    String payKey, String errorID, String errorMessage) {
            Log.e("makandeal", "<Paypal Payment Fail> Error :" + errorMessage);
            // CommitPayment.getInstance().closeLoading();
            CommitPayment.getInstance().fireCallback(PaymentStatus.FAILED);
        }

        @Override
        public void onPaymentSucceeded(String payKey, String paymentStatus) {
            Log.e("makandeal", "<Paypal Payment Succeeded> Error :"
                    + paymentStatus);
            // CommitPayment.getInstance().closeLoading();
            CommitPayment.getInstance().fireCallback(PaymentStatus.SUCCEEDED);
        }

        private void writeObject(ObjectOutputStream out) throws IOException {

        }

        private void readObject(ObjectInputStream in) throws IOException,
                ClassNotFoundException {

        }

    }
}
