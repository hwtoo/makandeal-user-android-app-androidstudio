package com.citrineplexus.makandeal.service;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.util.GlobalService;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;
import java.util.Map;

public class GoogleAnalyticsLib extends Application {

	public enum TrackerName {
		APP_TRACKER, // Tracker used only in this app.
		GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg:
						// roll-up tracking.
		ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a
							// company.
	}

	private static GoogleAnalyticsLib _instance = null;
	private static Context context = null;
	private HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

	private final String PROPERTY_ID = "UA-61006033-2";// Live
	// private final String PROPERTY_ID = "UA-61006033-3";// Staging

	private final TrackerName TRACKER_ID = TrackerName.APP_TRACKER;
	private final String CATEGORY_PURCHASEDEALS = "Purchase Deals";

	private GoogleAnalyticsLib(Context context) {
		System.out.println("%%%% context 22= "+context);
		if(analytics==null)
			analytics = GoogleAnalytics.getInstance(context);

		this.context = context;
	}

	public static GoogleAnalyticsLib getInstance(Context context) {
//		if (_instance != null)
			_instance.context = context;
		return _instance == null ? new GoogleAnalyticsLib(context) : _instance;
	}

	GoogleAnalytics analytics;
	public synchronized Tracker getTracker(TrackerName trackerId) {

		if (!mTrackers.containsKey(trackerId)) {

			System.out.println("%%%%% context ="+context);
//			analytics = GoogleAnalytics.getInstance(context);
			analytics.setLocalDispatchPeriod(30);
			Tracker t = /*
						 * (trackerId == TrackerName.APP_TRACKER) ? analytics
						 * .newTracker(R.xml.app_tracker) :
						 */(trackerId == TrackerName.GLOBAL_TRACKER) ? analytics
					.newTracker(R.xml.global_tracker) : analytics
					.newTracker(PROPERTY_ID);

			// Tracker t = (trackerId == TrackerName.APP_TRACKER) ?
			// analytics.newTracker(PROPERTY_ID)
			// : (trackerId == TrackerName.GLOBAL_TRACKER) ?
			// analytics.newTracker(R.xml.global_tracker)
			// : analytics.newTracker(R.xml.ecommerce_tracker);
			t.enableAdvertisingIdCollection(true);
			mTrackers.put(trackerId, t);
		}
		return mTrackers.get(trackerId);
	}

	public void trackView(String viewName) {
		if (GlobalService.isLive()) {
			Tracker tracker = getTracker(TRACKER_ID);
			tracker.setScreenName(viewName);
			tracker.send(new HitBuilders.AppViewBuilder().build());
		}
	}

	public void reportActivityStart(Activity activity) {
		GoogleAnalytics.getInstance(this).reportActivityStart(activity);
	}

	public void reportActivityStop(Activity activity) {
		GoogleAnalytics.getInstance(this).reportActivityStop(activity);
	}

	public void trackEvent(String action, String label) {
		if (GlobalService.isLive()) {
			Tracker tracker = getTracker(TRACKER_ID);
			tracker.send(new HitBuilders.EventBuilder()
					.setCategory(CATEGORY_PURCHASEDEALS).setAction(action)
					.setLabel(label).build());
		}
	}

	public void trackNotification(String action, String label) {
		if (GlobalService.isLive()) {
			Tracker tracker = getTracker(TRACKER_ID);
			tracker.send(new HitBuilders.EventBuilder()
					.setCategory("Notification").setAction(action)
					.setLabel(label).build());
		}
	}

	private String order_Id = "";

	public void trackEcommerceTransaction(BaseEntity en) {
		if (en == null || en.get("ORDER_ID") == null)
			return;
		order_Id = en.get("ORDER_ID").toString();
		sendDataToTwoTrackers(new HitBuilders.TransactionBuilder()
				.setTransactionId(order_Id)
				.setRevenue(Double.valueOf(en.get("GRAND_TOTAL").toString()))
				.setTax(0).setShipping(0).build());
	}

	public void trackEcommerceItem(BaseEntity en) {
		sendDataToTwoTrackers(new HitBuilders.ItemBuilder()
				.setTransactionId(order_Id).setName(en.get("NAME").toString())
				.setSku(en.get("SKU").toString()).setCategory("Category")
				.setPrice(Double.valueOf(en.get("PRICE").toString()))
				.setQuantity(Integer.valueOf(en.get("QTY").toString())).build());
	}

	private void sendDataToTwoTrackers(Map<String, String> params) {
		Tracker appTracker = getTracker(TrackerName.APP_TRACKER);
		Tracker ecommerceTracker = getTracker(TrackerName.ECOMMERCE_TRACKER);
		appTracker.send(params);
		ecommerceTracker.send(params);
	}

}
