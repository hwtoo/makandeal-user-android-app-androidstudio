package com.citrineplexus.makandeal.view.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.ExpandableAdapter;
import com.citrineplexus.makandeal.callback.OnExpandableListRender;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.view.activity.VoucherPage;

public class VoucherFragment extends Fragment implements
		OnExpandableListRender, OnRefreshListener, OnChildClickListener {

	private boolean showAllVoucher = false;
	private ExpandableListView expandListview = null;
	private ExpandableAdapter adapter = null;
	private ProgressBar loadingBar = null;

	private List<String> listDataHeader = new ArrayList<String>();
	private HashMap<String, List<BaseEntity>> listDataChild = new HashMap<String, List<BaseEntity>>();

	private List<BaseEntity> unusedList = new ArrayList<BaseEntity>();
	private List<BaseEntity> usedList = new ArrayList<BaseEntity>();
	private List<BaseEntity> expiredList = new ArrayList<BaseEntity>();

	private SwipeRefreshLayout swipe_container = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null
				&& getArguments().get("SHOW_ALL_FLAG") != null)
			showAllVoucher = getArguments().getBoolean("SHOW_ALL_FLAG");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		listDataHeader.clear();
		listDataHeader = null;

		listDataChild.clear();
		listDataChild = null;

	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_expandable_listview,
				container, false);

		swipe_container = (SwipeRefreshLayout) view
				.findViewById(R.id.swipe_container);
		swipe_container.setOnRefreshListener(this);
		expandListview = (ExpandableListView) view
				.findViewById(R.id.expandListView);
		expandListview.setOnChildClickListener(this);
		TextView empty = (TextView) view.findViewById(R.id.expandEmptyView);
		empty.setText(getResources().getString(R.string.EmptyVoucherMsg));

		expandListview.setEmptyView(empty);

		loadingBar = (ProgressBar) view.findViewById(R.id.loadingBar);

		adapter = new ExpandableAdapter(this);
		adapter.set_listDataChild(listDataChild);
		adapter.set_listDataHeader(listDataHeader);

		expandListview.setAdapter(adapter);
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	private void clearList() {
		if (!unusedList.isEmpty())
			unusedList.clear();
		if (!usedList.isEmpty())
			usedList.clear();
		if (!expiredList.isEmpty())
			expiredList.clear();
		if (!listDataHeader.isEmpty())
			listDataHeader.clear();
		if (!listDataChild.isEmpty())
			listDataChild.clear();
	}

	public void notifyDataChanged(ArrayList<BaseEntity> voucherAry,
			boolean refreshFlag) {
		if (refreshFlag) {
			swipe_container.setRefreshing(false);
			clearList();
		}
		int allItemSize = voucherAry.size();
		for (int i = 0; i < allItemSize; i++) {
			BaseEntity en = voucherAry.get(i);
			if (en.get("STATUS") != null
					&& en.get("STATUS").toString().equals("A")) {
				unusedList.add(en);
				// voucherAry.remove(en);
			} else if (en.get("STATUS") != null
					&& en.get("STATUS").toString().equals("R")) {
				usedList.add(en);
			} else
				expiredList.add(en);
			// i++;
			// allItemSize = voucherAry.size();
		}
		// tempHashMap.put(regionId, groupArrayList);

		// if (voucherAry.isEmpty())
		// finishLoop = true;
		// }
		prepareListData();
	}

	private void prepareListData() {
		listDataHeader.add("Unused");
		listDataChild.put(listDataHeader.get(0), unusedList);

		if (showAllVoucher) {
			if (!usedList.isEmpty()) {
				listDataHeader.add("Used");
				listDataChild.put(listDataHeader.get(listDataHeader.size()-1), usedList);
			}
			if (!expiredList.isEmpty()) {
				listDataHeader.add("Expired");
				listDataChild.put(listDataHeader.get(listDataHeader.size()-1), expiredList);
			}
		}

		adapter.notifyDataSetChanged();
		if (!adapter.isEmpty())
			loadingBar.setVisibility(View.GONE);
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		String headerTitle = (String) adapter.getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) getActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.child_location_header,
					null);
		}

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.locationHeader);
		lblListHeader.setText(headerTitle);

		expandListview.expandGroup(groupPosition);
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		BaseEntity en = (BaseEntity) adapter.getChild(groupPosition,
				childPosition);
		voucherChildHolder holder = null;

		if (convertView == null) {
			convertView = LayoutInflater.from(getActivity()).inflate(
					R.layout.child_voucher_row, parent, false);
			holder = new voucherChildHolder();
			holder.productImage = (ImageView) convertView
					.findViewById(R.id.productImage);
			holder.productTitle = (TextView) convertView
					.findViewById(R.id.productTitle);
			holder.location = (TextView) convertView
					.findViewById(R.id.location);
			holder.expiryDate = (TextView) convertView
					.findViewById(R.id.expiryDate);
			convertView.setTag(holder);
		} else
			holder = (voucherChildHolder) convertView.getTag();
		if (en.get("IMAGE_URL") != null && holder.productImage != null)
			ImageLoader.getInstance(getActivity()).DisplayImage(
					en.get("IMAGE_URL").toString(), holder.productImage,
					ScaleType.CENTER_CROP, false);

		if (en.get("PRODUCT_NAME") != null && holder.productTitle != null)
			holder.productTitle.setText(en.get("PRODUCT_NAME").toString());

		if (en.get("REDEEM_LOCATION") != null && holder.location != null)
			holder.location.setText(en.get("REDEEM_LOCATION").toString());

		if (en.get("VALID_TO") != null && holder.expiryDate != null)
			holder.expiryDate.setText(en.get("VALID_TO").toString());

		return convertView;
	}

	class voucherChildHolder {
		ImageView productImage;
		TextView productTitle, location, expiryDate;
	}

	@Override
	public void onRefresh() {
		if (getActivity() instanceof VoucherPage) {
			((VoucherPage) getActivity()).initData(true);
		}
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		BaseEntity entity = (BaseEntity) adapter.getChild(groupPosition,
				childPosition);
		if (getActivity() instanceof VoucherPage) {
			((VoucherPage) getActivity()).openRedeemPage(entity.getAsBundle());
		}
		return false;
	}

}
