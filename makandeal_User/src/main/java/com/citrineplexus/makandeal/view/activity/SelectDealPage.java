package com.citrineplexus.makandeal.view.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout.LayoutParams;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.AbstractRecyclerAdapter.OnListAdapterItemClickListener;
import com.citrineplexus.makandeal.adapter.AdapterBinder;
import com.citrineplexus.makandeal.adapter.ListAdapter;
import com.citrineplexus.makandeal.binder.SelectDealBinder;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.service.OAuthAuthorization;

public class SelectDealPage extends DefaultRecyclerListView implements
		OnListAdapterItemClickListener {

	private boolean updateCart = false, successAddToCart = false;
	private BaseEntity selectedEntity = null;
	private AdapterBinder selectDealBinder = null;

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		super.onAquireResult(entity, position, params, true);
		if (entity.get("JSONARRAY") != null) {
			ArrayList<BaseEntity> dataAry = (ArrayList<BaseEntity>) entity
					.get("JSONARRAY");
			for (BaseEntity en : dataAry) {
				getListAdapter().addData(en);
			}
			getListAdapter().notifyDataSetChanged();
		}
	}

	@Override
	public void onRefresh() {

	}

	@Override
	protected void onResume() {
		super.onResume();
		updateCart = false;
		successAddToCart = false;
		if (selectDealBinder != null)
			selectDealBinder.setOnListAdapterItemClickListener(this);
	}

	private void addToCart(BaseEntity entity) {
		Bundle apiParam = new Bundle();
		apiParam.putString("ENTITY_ID", entity.get("ENTITY_ID").toString());
		apiParam.putString("QTY", "1");
		apiParam.putString("REDEEM_LOCATION", entity.get("BRANCH_ID")
				.toString());
		apiParam.putBoolean("CHECK_CART_NUM", true);
		successAddToCart = true;
		getServiceData(1042, 0, apiParam, true);
		GoogleAnalyticsLib.getInstance(this).trackEvent("Add to Cart",
				entity.get("NAME").toString());
	}

	@Override
	public void onItemClickListener(View view, int position) {
		if (OAuthAuthorization.getInstance().isUserLogin()) {
			// MakanDealApplication.getInstance().setSelectDealListener(this);
			selectedEntity = getListAdapter().getData(position);
			updateCart = true;
			Bundle param = new Bundle();
			param.putBoolean("CART_ITEMS", true);
			getServiceData(1020, 0, param, true);
			// addToCart(getListAdapter().getData(position));
		} else
			openView(ViewName.SIGN_IN, null);
	}

	@Override
	public void onRefreshCart(int cartItemCount) {
		super.onRefreshCart(cartItemCount);

		if (updateCart) {
			updateCart = false;
			if (selectedEntity != null) {
				addToCart(selectedEntity);
				return;
			}
		}

		if (successAddToCart) {
			handler.sendEmptyMessage(LOADING_CLOSE);
			openView(ViewName.CART_VIEW, null);
			successAddToCart = false;
		}
	}

	@Override
	int getContentView() {
		return R.layout.select_deal_recycler_listview;
	}

	@Override
	ListAdapter initListAdapter() {
		return new ListAdapter(this);
	}

	@Override
	String getPageTitle() {
		return "Select Deal";
	}

	@Override
	AdapterBinder getBinder() {
		selectDealBinder = new SelectDealBinder(this);
		return selectDealBinder;
	}

	@Override
	int getContentId() {
		return 1005;
	}

	@Override
	void onInit() {
		LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		layoutParams.setMargins(0, 50, 0, 0);
		recyclerView.setLayoutParams(layoutParams);
	}

	@Override
	void getServiceData(int contentID) {
		if (getIntent() != null && getIntent().getExtras() != null
				&& getIntent().getExtras().getString("ENTITY_ID") != null) {
			getServiceData(contentID, 0, getIntent().getExtras(), true);
		}
	}

	@Override
	String getUniqueKey() {
		return null;
	}

	@Override
	void adapterEmptyHandler(boolean adapterIsEmpty, RecyclerView recyclerView) {

	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.HIDE_ALL;
	}

	@Override
	int drawerID() {
		return -1;
	}

	@Override
	void handlerMessage(Message msg) {

	}

}
