package com.citrineplexus.makandeal.view.activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.text.SpannableString;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.util.GlobalService;

public class RedeemPage extends AbstractActivity implements OnClickListener {

	private ImageView qrCodeImg = null, qrStatus = null;
	private TextView voucherCode = null, securityCode = null, orderCode = null,
			productTitle = null, subProductTitle = null, location = null,
			validFrom = null, includeTitle = null, includeInfor,
			finePrintTitle = null, finePrintInfor = null, addressTitle = null,
			address = null, contactNumber = null, operationHour = null;
	private BaseEntity pageEntity = null;
	private LinearLayout packageIncludedLayout = null, finePrintLayout = null;
	private View qrUsedHolder = null;
	private String vendorAddress = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setTitle("Redeem");
		setContentView(R.layout.activity_redeem_page);
		qrUsedHolder = (View) findViewById(R.id.qrUsedHolder);
		qrCodeImg = (ImageView) findViewById(R.id.QRImage);
		qrStatus = (ImageView) findViewById(R.id.QRStatus);
		voucherCode = (TextView) findViewById(R.id.voucherCode);
		securityCode = (TextView) findViewById(R.id.SecurityCode);
		orderCode = (TextView) findViewById(R.id.orderCode);
		productTitle = (TextView) findViewById(R.id.productTitle);
		subProductTitle = (TextView) findViewById(R.id.subProductTitle);
		validFrom = (TextView) findViewById(R.id.validFrom);
		location = (TextView) findViewById(R.id.location);

		addressTitle = (TextView) findViewById(R.id.vendorLayout).findViewById(
				R.id.addressTitle);
		address = (TextView) findViewById(R.id.vendorLayout).findViewById(
				R.id.address);
		contactNumber = (TextView) findViewById(R.id.vendorLayout)
				.findViewById(R.id.contactNumber);
		operationHour = (TextView) findViewById(R.id.vendorLayout)
				.findViewById(R.id.operationHour);
		findViewById(R.id.vendorLayout).findViewById(R.id.contactUs_Phone)
				.setOnClickListener(this);
		findViewById(R.id.vendorLayout).findViewById(R.id.map)
				.setOnClickListener(this);

		packageIncludedLayout = (LinearLayout) findViewById(R.id.packageIncludedLayout);
		includeTitle = (TextView) packageIncludedLayout
				.findViewById(R.id.packageTitle);
		includeInfor = (TextView) packageIncludedLayout
				.findViewById(R.id.packageInfor);

		finePrintLayout = (LinearLayout) findViewById(R.id.finePrintLayout);
		finePrintTitle = (TextView) finePrintLayout
				.findViewById(R.id.packageTitle);
		finePrintInfor = (TextView) finePrintLayout
				.findViewById(R.id.packageInfor);

		findViewById(R.id.buyNowBtn).setOnClickListener(this);

		pageEntity = new BaseEntity();
		for (String key : getIntent().getExtras().keySet()) {
			if (key.equalsIgnoreCase("DEAL_ID")) {
				key = "ENTITY_ID";
				pageEntity.put(key, getIntent().getExtras().get("DEAL_ID"));
			} else
				pageEntity.put(key, getIntent().getExtras().get(key));
		}

		init(getIntent().getExtras());
	}

	private void init(Bundle bundle) {
		if (bundle == null) {
			finish();
			return;
		}

		parseEntityObject(pageEntity, 0);
		// ************ request service detail ************
		Bundle serviceDetailParams = new Bundle();
		serviceDetailParams.putString("ENTITY_ID", pageEntity.get("ENTITY_ID")
				.toString());
		serviceDetailParams.putInt("DOWNLOAD_STAGE", 1);
		getServiceData(1031, 0, serviceDetailParams, false);

		// ************ request service fineprint ************
		Bundle serviceDetailFineprint = new Bundle();
		serviceDetailFineprint.putString("ENTITY_ID",
				pageEntity.get("ENTITY_ID").toString());
		serviceDetailFineprint.putInt("DOWNLOAD_STAGE", 2);
		serviceDetailFineprint.putBoolean("LOADING", true);
		getServiceData(1027, 0, serviceDetailFineprint, true);
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		super.onAquireResult(entity, position, params,
				params.getBoolean("LOADING", false));

		Message msg = new Message();
		msg.what = params.getInt("DOWNLOAD_STAGE", 0);
		msg.obj = entity;
		handler.sendMessage(msg);
	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.HIDE_ALL;
	}

	@Override
	int drawerID() {
		return -1;
	}

	@Override
	void handlerMessage(Message msg) {
		parseEntityObject((BaseEntity) msg.obj, msg.what);
	}

	private SimpleDateFormat Orig = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
	private Date mDateFrom = null, mDateTo = null;

	private synchronized void parseEntityObject(BaseEntity entity, int stage) {
		if (entity == null)
			return;
		pageEntity.merge(entity);

		switch (stage) {
		case 0:// BASIC INFO

			if (pageEntity.get("GROUP_NAME") == null) {
				productTitle.setText((String) pageEntity.get("PRODUCT_NAME"));
			} else {
				productTitle.setText((String) pageEntity.get("GROUP_NAME"));
				subProductTitle
						.setText((String) pageEntity.get("PRODUCT_NAME"));
				subProductTitle.setVisibility(View.VISIBLE);
			}

			if (pageEntity != null && pageEntity.get("QR") != null)
				com.citrineplexus.library.imageloader.ImageLoader.getInstance(
						this).DisplayImage((String) pageEntity.get("QR"),
						qrCodeImg, ScaleType.CENTER, true);

			if (pageEntity.get("STATUS") != null
					&& ((String) pageEntity.get("STATUS")).equals("R")) {
				qrUsedHolder.setVisibility(View.VISIBLE);
				qrStatus.setVisibility(View.VISIBLE);
				qrStatus.setImageBitmap(BitmapFactory.decodeResource(
						getResources(), R.drawable.img_redeemed));
			} else if (pageEntity.get("STATUS") != null
					&& ((String) pageEntity.get("STATUS")).equals("E")) {
				qrUsedHolder.setVisibility(View.VISIBLE);
				qrStatus.setVisibility(View.VISIBLE);
				qrStatus.setImageBitmap(BitmapFactory.decodeResource(
						getResources(), R.drawable.img_expired));
			} else {
				qrStatus.setVisibility(View.GONE);
				qrUsedHolder.setVisibility(View.GONE);
			}

			// if (entity.get("UDROPSHIP_VENDOR") != null) {
			// // ************ request branch data ************
			// Bundle param = new Bundle();
			// param.putString("UDROPSHIP_VENDOR",
			// entity.get("UDROPSHIP_VENDOR", "").toString());
			// param.putInt("DOWNLOAD_STAGE", 3);
			// ViewController.getInstance(this).getServiceData(1019, this, 0,
			// param);
			// }

			addressTitle.setText((String) pageEntity.get("VENDOR_NAME"));
			addressTitle.setVisibility(View.VISIBLE);
			address.setText((String) pageEntity.get("STREET"));
			contactNumber.setText((String) pageEntity.get("TELEPHONE"));
			operationHour.setText((String) pageEntity.get("BUSINESS_HOUR"));

			vendorAddress = pageEntity.get("STREET", "").toString().trim()
					+ " " + pageEntity.get("ZIP", "").toString().trim() + " "
					+ pageEntity.get("CITY", "").toString().trim();

			location.setText(Html.fromHtml(vendorAddress));

			voucherCode.setText(pageEntity.get("SERVICE_VOUCHER_CODE", "")
					.toString());
			securityCode
					.setText(pageEntity.get("SECURITY_CODE", "").toString());
			orderCode.setText(pageEntity.get("ORDER_ID", "").toString());
			try {

				Calendar mCalendar = Calendar.getInstance();
				mCalendar.setTime(Orig.parse((String) getIntent().getExtras()
						.getString("VALID_FROM")));

				mDateFrom = mCalendar.getTime();
				mCalendar.setTime(Orig.parse((String) getIntent().getExtras()
						.getString("VALID_TO")));

				mDateTo = mCalendar.getTime();
				validFrom.setText(sdf.format(mDateFrom) + " to "
						+ sdf.format(mDateTo));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return;

		case 1:// Package Included
			if (entity.get("PACKAGE_INCLUDES") != null
					&& entity.get("PACKAGE_INCLUDES") instanceof ArrayList<?>) {
				ArrayList<String> benefitsAry = (ArrayList<String>) entity
						.get("PACKAGE_INCLUDES");
				if (!benefitsAry.isEmpty()) {
					SpannableString ss = GlobalService
							.convertToBulletText(benefitsAry);
					packageIncludedLayout.setVisibility(View.VISIBLE);
					includeTitle.setVisibility(View.VISIBLE);
					includeTitle.setText("Package Includeds");
					includeInfor.setText(GlobalService
							.convertToBulletText(benefitsAry));
				}
			}

			return;
		case 2:// Fine Prints
			if (entity.get("JSONARRAY") != null)
				if (entity.get("JSONARRAY") instanceof ArrayList<?>) {
					for (BaseEntity en : (ArrayList<BaseEntity>) entity
							.get("JSONARRAY")) {

						ArrayList<String> detailAry = (ArrayList<String>) en
								.get("DETAIL");
						if (!detailAry.isEmpty()) {
							finePrintLayout.setVisibility(View.VISIBLE);
							finePrintTitle.setVisibility(View.VISIBLE);
							finePrintInfor.setText(GlobalService
									.convertToBulletText(detailAry));
						}
					}
				}
			return;
		}
	}

	private void openMap() {
		GoogleAnalyticsLib.getInstance(this).trackEvent("Map",
				"From Voucher");
		Bundle param = new Bundle();
		param.putString("ADDRESS", vendorAddress);
		param.putString("SHOPNAME", (String) pageEntity.get("VENDOR_NAME"));
		openView(ViewName.MAP, param);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.map:
			openMap();
			break;

		case R.id.contactUs_Phone:
			GoogleAnalyticsLib.getInstance(this).trackEvent("Call",
					"From Voucher");
			GlobalService.callSupportIntent(this,
					(String) pageEntity.get("TELEPHONE"));
			break;

		case R.id.buyNowBtn:

			Bundle param = new Bundle();
			param.putString("ENTITY_ID", pageEntity.get("ENTITY_ID").toString());
			openView(ViewName.DEAL_PAGE, param);
			break;

		default:
			break;
		}

	}
}
