package com.citrineplexus.makandeal.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.view.activity.DealPage;

import java.util.ArrayList;

public class DetailFragment extends Fragment implements OnDataRequestListener,
		OnClickListener {

	private TextView vendorName = null, vendorAbout = null;
	private LinearLayout vendorGallery = null;
	private BaseEntity entity = null;
	private String entityID = "";

	// public static String vendorNameString = "";
	private boolean isFragmentShowing = false;

	public static Fragment newInstance(String entityId) {
		DetailFragment about = new DetailFragment();
		Bundle bundle = new Bundle();
		bundle.putString("ENTITY_ID", entityId);
		about.setArguments(bundle);
		return about;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.fragment_detail, container, false);
		vendorName = (TextView) view.findViewById(R.id.vendorName);
		vendorAbout = (TextView) view.findViewById(R.id.vendorAbout);

		// TODO: Stage 2
		// TextView packageIncluded = (TextView) (view
		// .findViewById(R.id.packageIncludedLayout)
		// .findViewById(R.id.packageTitle));
		// packageIncluded.setText("Package Includes");
		// packageInclude = (TextView) (view
		// .findViewById(R.id.packageIncludedLayout)
		// .findViewById(R.id.packageInfor));

		((LinearLayout) view.findViewById(R.id.merchantStoreBtn))
				.setOnClickListener(this);

		vendorGallery = (LinearLayout) view.findViewById(R.id.vendorGallery);
		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null
				&& getArguments().getString("ENTITY_ID") != null)
			entityID = getArguments().getString("ENTITY_ID");
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser&&context!=null)
			GoogleAnalyticsLib.getInstance(getActivity()).trackView("Details");
	}

	private Context context;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(this.context==null)
			this.context = context;
	}

	@Override
	public void onDetach() {
		isFragmentShowing = false;
		super.onDetach();
	}

	@Override
	public void onResume() {
		isFragmentShowing = true;
		super.onResume();
		if (getActivity() instanceof DealPage) {
			// ((DealPage) getActivity()).addFragmentDataRequestListener(this);
			entity = ((DealPage) getActivity()).getPageEntity();
		}
		// TODO: Stage 2
		// getPackageInclude();
		if (entity != null) {
			initMerchant((String) entity.get("UDROPSHIP_VENDOR"));// Merchant
																	// Detail
		} else {
			getVendorId();
		}
	}

	private void getVendorId() {
		Bundle bundle = new Bundle();
		bundle.putString("FragmentName", this.getClass().getName() + "|21");
		bundle.putString("ENTITY_ID", entityID);
		bundle.putInt("CONTENT_ID", 1012);
		bundle.putInt("DOWNLOAD_STAGE", 21);
		((DealPage) getActivity()).getFragmentData(bundle, this);

	}

	// TODO: Stage 2
	// private void getPackageInclude() {
	// Bundle bundle = new Bundle();
	// bundle.putString("FragmentName", "AboutFragment|2");
	// bundle.putString("ENTITY_ID", entityID);
	// bundle.putInt("CONTENT_ID", 1031);
	// bundle.putInt("DOWNLOAD_STAGE", 2);
	// ((DealPage) getActivity()).getFragmentData(bundle, this);
	// }

	private void initMerchant(String vendorID) {
		Bundle bundle = new Bundle();
		bundle.putString("FragmentName", this.getClass().getName() + "|20");
		bundle.putString("VENDOR_ID", vendorID);
		bundle.putInt("CONTENT_ID", 1004);
		bundle.putInt("DOWNLOAD_STAGE", 20);
		((DealPage) getActivity()).getFragmentData(bundle, this);
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		int downloadState = params.getInt("DOWNLOAD_STAGE", 0);
		if (params != null
				&& params.getString("FragmentName").equalsIgnoreCase(
						getClass().getName() + "|" + downloadState)
				|| downloadState == 2) {
			Message msg = new Message();
			msg.what = downloadState;
			msg.obj = entity;

			handler.sendMessage(msg);
		}
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			parseEntityObject((BaseEntity) msg.obj, msg.what);
			super.handleMessage(msg);
		}
	};

	@Override
	public void onConnectionError(Exception ex) {

	}

	private synchronized void parseEntityObject(BaseEntity entity, int stage) {
		if (entity == null)
			return;

		switch (stage) {
		// TODO: Stage 2
		// case 2:// package Include
		// if (entity.get("PACKAGE_INCLUDES") != null
		// && entity.get("PACKAGE_INCLUDES") instanceof ArrayList<?>) {
		// ArrayList<String> benefitsAry = (ArrayList<String>) entity
		// .get("PACKAGE_INCLUDES");
		// if (!benefitsAry.isEmpty()) {
		// packageInclude.setText(GlobalService
		// .convertToBulletText(benefitsAry));
		// packageInclude.setVisibility(View.VISIBLE);
		// }
		// }
		// break;

		case 20:// Merchant Detail
			if (entity.get("VENDOR_NAME") != null) {
				// vendorNameString = (String) entity.get("VENDOR_NAME");
				vendorName.setText((String) entity.get("VENDOR_NAME"));
			}
			if (entity.get("ABOUT") != null) {
				String about = (String) entity.get("ABOUT");
				if (about != null && about.length() > 0)
					vendorAbout.setText(about);
				else
					vendorAbout.setVisibility(View.GONE);

			}

			if (entity.get("GALLERY") != null
					&& entity.get("GALLERY") instanceof ArrayList<?>
					&& isFragmentShowing) {
				ArrayList<String> galleryImage = (ArrayList<String>) entity
						.get("GALLERY");
				initVendorGallery(galleryImage);
			}
			break;

		case 21:// Get Vendor Id
			if (entity.get("UDROPSHIP_VENDOR") != null && isFragmentShowing)
				initMerchant(entity.get("UDROPSHIP_VENDOR").toString());
			break;
		}
	}

	private void initVendorGallery(ArrayList<String> galleryAry) {
		for (String imgUrl : galleryAry) {
			imgUrl = imgUrl.replace(" ", "%20");
			LayoutParams layoutParams = new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			layoutParams.setMargins(0, 0, 0, 10);
			ImageView imageView = new ImageView(getActivity());
			imageView.setLayoutParams(layoutParams);
			imageView.setAdjustViewBounds(true);
			imageView.setScaleType(ScaleType.CENTER_CROP);
			ImageLoader.getInstance(getActivity()).DisplayImage(imgUrl,
					imageView, ScaleType.CENTER_CROP, false);
			vendorGallery.addView(imageView);
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.merchantStoreBtn)
			if (getActivity() instanceof DealPage) {
				((DealPage) getActivity()).openMerchantStore();
				GoogleAnalyticsLib.getInstance(getActivity()).trackEvent(
						"Other Deals", null);
			}
	}
}
