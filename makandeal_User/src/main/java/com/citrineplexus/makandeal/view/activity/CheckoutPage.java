package com.citrineplexus.makandeal.view.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.widget.Toast;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.CommitPayment;
import com.citrineplexus.makandeal.service.CommitPayment.PaymentStatus;
import com.citrineplexus.makandeal.service.CommitPayment.PaymentStatusCallback;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.service.OAuthAuthorization;
import com.citrineplexus.makandeal.view.MakanDealApplication;
import com.citrineplexus.makandeal.view.fragment.BillingFragment;
import com.citrineplexus.makandeal.view.fragment.PaymentFragment;

import java.util.ArrayList;

public class CheckoutPage extends AbstractActivity {

	private BaseEntity checkoutEntity = new BaseEntity();
	boolean opendedFail = false;
	private boolean checkoutCallbackFlag = false;

	private final int SUCCESS_HANDLER = 1050;
	private final int FAIL_HANDLER = 1051;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_checkout_page);

		if (OAuthAuthorization.getInstance().isUserLogin()
				&& OAuthAuthorization.getInstance().getUserProfileData() != null) {
			checkoutEntity.merge(OAuthAuthorization.getInstance()
					.getUserProfileData());

			checkoutEntity.put("QUOTE_ID",
					getIntent().getExtras().getString("QUOTE_ID"));
			checkoutEntity.put("GRAND_TOTAL", getIntent().getExtras()
					.getDouble("GRAND_TOTAL"));
			if (checkoutEntity.get("ID") != null) {
				getUserAddress();
			} else {
				getUserInfo();
			}
		}
	}

	public BaseEntity getCheckoutEntity() {
		return this.checkoutEntity;
	}

	public void setTitle(String title) {
		getSupportActionBar().setTitle(title);
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		super.onAquireResult(entity, position, params,
				params.getBoolean("LOADING", false));
		if (params == null || entity == null)
			return;

		if (position == 1053 && params.get("CHECKOUT") != null
				&& params.getBoolean("CHECKOUT", false)) {
			if (entity != null) {
				if (entity.get("MESSAGES") != null) {
					BaseEntity msgEntity = (BaseEntity) entity.get("MESSAGES");
					ArrayList<BaseEntity> errorAry = (ArrayList<BaseEntity>) msgEntity
							.get("ERROR");
					BaseEntity errorEntity = errorAry.get(0);
					String errorMessage = (String) errorEntity.get("MESSAGE");
					
					// int index1 = errorMessage.indexOf("\"");
					// errorMessage =
					// errorMessage.substring(index1+1,errorMessage.length());
					// int index2 = errorMessage.indexOf("\"");
					
					Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT)
							.show();
					this.finish();
					return;
				}

				final BaseEntity finalEntity = entity.merge(checkoutEntity);
//				finalEntity.put("PAYMENT_METHOD_ID",checkoutEntity.get("PAYMENT_METHOD_ID"));
				CommitPayment.getInstance().registerPaymentResult(
						new PaymentStatusCallback() {
							@Override
							public void onPaymentStatus(PaymentStatus status) {
								// String paymentResult = status ==
								// PaymentStatus.SUCCEEDED ? "Success"
								// : status == PaymentStatus.FAILED ? "Failed"
								// : "Cancelled";
								// Refresh Cart number
								MakanDealApplication.getInstance()
										.requestUserCartItems();

								checkoutCallbackFlag = false;
								if (status == PaymentStatus.FAILED
										|| status == PaymentStatus.CANCELED) {
									handler.sendEmptyMessage(FAIL_HANDLER);
								} else if (status == PaymentStatus.SUCCEEDED)
									handler.sendEmptyMessage(SUCCESS_HANDLER);
							}
						});

				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						if (!checkoutCallbackFlag) {
//							CommitPayment
//									.getInstance()
//									.payNow(CheckoutPage.this,
//											checkoutEntity.merge(finalEntity),
//											PaymentType.values()[(Integer) checkoutEntity
//													.get("PAYMENT_METHOD_ID",
//															-1)]);

                            CommitPayment.getInstance()
                                    .payNow(CheckoutPage.this,
                                            finalEntity,
                                            CommitPayment.PaymentType.values()[(Integer) finalEntity
                                                    .get("PAYMENT_METHOD_ID",
                                                            -1)]);
                        }
						checkoutCallbackFlag = true;
					}
				}, 500);
			}
		} else if (params.getBoolean("GET_USER", false)) {
			BaseEntity userEn = null;
			for (String key : entity.getKeySet()) {
				userEn = (BaseEntity) entity.get(key);
				userEn.put("ID", key);
			}
			this.checkoutEntity.merge(OAuthAuthorization.getInstance()
					.getUserProfileData());
			getUserAddress();
		} else if (params.getBoolean("GET_ADDRESS", false)) {
			if (entity.get("JSONARRAY") != null) {

				if (((ArrayList<BaseEntity>) entity.get("JSONARRAY")).size() > 0)
					this.checkoutEntity.merge(((ArrayList<BaseEntity>) entity
							.get("JSONARRAY")).get(0));
				OAuthAuthorization.getInstance().getUserProfileData()
						.merge(this.checkoutEntity);
				checkFragment();
			}
		}

	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.HIDE_ALL;
	}

	@Override
	int drawerID() {
		return -1;
	}

	@Override
	void handlerMessage(Message msg) {
		if (msg.what == SUCCESS_HANDLER)
			openSuccessPaymentDialog();
		else if (msg.what == FAIL_HANDLER)
			openFailPaymentDialog();
	}

	private void getUserAddress() {
		if (checkoutEntity != null && checkoutEntity.get("ID") == null)
			return;
		Bundle addressParam = new Bundle();
		addressParam.putBoolean("GET_ADDRESS", true);
		addressParam.putString("USER_ID", checkoutEntity.get("ID").toString());
		addressParam.putBoolean("LOADING", true);
		getServiceData(1021, 0, addressParam, true);
	}

	private void getUserInfo() {
		Bundle userParam = new Bundle();
		userParam.putBoolean("GET_USER", true);
		userParam.putBoolean("LOADING", true);
		getServiceData(1015, 0, userParam, true);
	}

	public void checkFragment() {
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();

		if (!OAuthAuthorization.getInstance().checkUserProfileDetail()) {
			transaction.replace(R.id.container, new BillingFragment());
		} else {
			transaction.replace(R.id.container, new PaymentFragment());
		}
		transaction.commit();
	}

	public void openView(ViewName viewName, Bundle param) {
		super.openView(viewName, param);
	}

	public void getDataFromServer(Bundle param, int contentID,
			boolean loadingFlag) {
		getServiceData(contentID, 0, param, loadingFlag);
	}

	public void checkout(Bundle param, BaseEntity entity) {
		GoogleAnalyticsLib.getInstance(this).trackEvent("Checkout Now", null);
		checkoutDeal(1053, param, true, entity);
	}

	private void openSuccessPaymentDialog() {
		AlertDialog.Builder mDialog = new AlertDialog.Builder(this);
		mDialog.setMessage("Payment successful, please check your email. Earn RM2?");
		mDialog.setPositiveButton("Sure!",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Bundle intent = new Bundle();
						intent.putBoolean("FROM_PAYMENT", true);
						openView(ViewName.EARN_RM2, intent);
						finishCheckoutPage();
					}
				});
		mDialog.setNegativeButton("No, thanks.",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Bundle intent = new Bundle();
						intent.putBoolean("FROM_PAYMENT", true);
						openView(ViewName.VOUCHER, intent);
						finishCheckoutPage();
					}
				});

		mDialog.setCancelable(false);
		mDialog.create().show();
	}

	private void openFailPaymentDialog() {
		if (opendedFail)
			return;
		opendedFail = true;
		AlertDialog.Builder mDialog = new AlertDialog.Builder(this);
		mDialog.setMessage("Sorry, payment has failed. Continue shopping?");
		mDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				openView(ViewName.HOMEPAGE, null);
				finishCheckoutPage();
			}
		});
		mDialog.setNegativeButton("Exit",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Bundle bundle = new Bundle();
						bundle.putBoolean("EXITAPP", true);
						openView(ViewName.HOMEPAGE, bundle);
					}
				});

		mDialog.setCancelable(false);
		mDialog.create().show();
	}

	private void finishCheckoutPage() {
		setResult(RESULT_OK, new Intent().putExtra("QUIT_CARTVIEW", true));
		finish();
	}

}
