package com.citrineplexus.makandeal.view.activity;

import java.util.ArrayList;
import java.util.Collections;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;

import com.citrineplexus.library.slidingTab.SlidingTabDividerLayout;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.VoucherViewPager;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.view.fragment.VoucherFragment;

public class VoucherPage extends AbstractActivity {
	private SlidingTabDividerLayout slidingTabLayout = null;
	private ViewPager viewPager = null;
	private VoucherViewPager pagerAdapter = null;

//	private BaseEntity pageEntity = null;
	private boolean dataLoadFinish = false;
	private ArrayList<BaseEntity> purchasedList = new ArrayList<BaseEntity>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_voucher_page);
		getSupportActionBar().setTitle("Voucher");

		GoogleAnalyticsLib.getInstance(this).trackView("Voucher Page");

		slidingTabLayout = (SlidingTabDividerLayout) findViewById(R.id.slidingTabLayout);
		slidingTabLayout.setDistributeEvenly(true);
		slidingTabLayout
				.setCustomTabColorizer(new SlidingTabDividerLayout.TabColorizer() {

					@Override
					public int getIndicatorColor(int position) {
						return Color.TRANSPARENT;
					}
				});

		pagerAdapter = new VoucherViewPager(getSupportFragmentManager());

		viewPager = (ViewPager) findViewById(R.id.viewPagerID);
		viewPager.setAdapter(pagerAdapter);
		slidingTabLayout.setViewPager(viewPager);
		initData(false);
	}

	public void initData(boolean swipeToRefresh) {
		Bundle getDataParam = new Bundle();
		getDataParam.putBoolean("INIT_REQUEST", true);
		getDataParam.putBoolean("HISTORY", true);
		dataLoadFinish = swipeToRefresh;
		getServiceData(1008, 0, getDataParam, false);
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		if (params.getBoolean("HISTORY")) {
			if (entity != null && entity.get("JSONARRAY") != null) {

				if (!purchasedList.isEmpty())
					purchasedList.clear();

				for (BaseEntity en : (ArrayList<BaseEntity>) entity
						.get("JSONARRAY")) {
					purchasedList.add(en);
				}
				Collections.sort(purchasedList,
						BaseEntity.COMPARE_BY_VALID_DATE);
//				Collections.sort(purchasedList, BaseEntity.COMPARE_BY_STATUS);
			}
			

			for (int i = 0; i < pagerAdapter.getCount(); i++) {
				((VoucherFragment) pagerAdapter.getItem(i)).notifyDataChanged(
						purchasedList, dataLoadFinish);
			}

		}
	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.NO_SEARCH;
	}

	@Override
	int drawerID() {
		return -1;
	}

	@Override
	void handlerMessage(Message msg) {

	}
	
	public void openRedeemPage(Bundle bundle){
		openView(ViewName.REDEEM_VIEW, bundle);
	}

}
