package com.citrineplexus.makandeal.view.activity;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.util.GlobalService;

public class ContactUsPage extends AbstractActivity implements OnClickListener {

	private TextView appVersion = null, appConfig = null;
	private final String title = "Contact Us";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setTitle(title);
		GoogleAnalyticsLib.getInstance(this).trackView(title);

		setContentView(R.layout.activity_contactus_page);
		findViewById(R.id.contactUs_Phone).setOnClickListener(this);
		findViewById(R.id.contactUs_Email).setOnClickListener(this);
		appVersion = (TextView) findViewById(R.id.appVersion);
		appConfig = (TextView) findViewById(R.id.appConfig);

		PackageInfo pInfo;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			appVersion.setText("version " + pInfo.versionName);
			String appurl = getResources().getString(R.string.apiUrl);
			appConfig.setText(appurl.substring(7, appurl.length()));
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.contactUs_Phone:
			GlobalService.callSupportIntent(this,
					getString(R.string.contactUsPhone).trim());
			break;

		case R.id.contactUs_Email:
			GlobalService.emailSupportIntent(this);
			break;
		default:
			break;
		}
	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_UNLOCKED;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.ALL;
	}

	@Override
	void handlerMessage(Message msg) {

	}

	@Override
	int drawerID() {
		return 112;
	}

}
