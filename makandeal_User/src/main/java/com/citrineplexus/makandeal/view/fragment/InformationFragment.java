package com.citrineplexus.makandeal.view.fragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citrineplexus.library.gallery.GalleryLooping;
import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.ExpandableAdapter;
import com.citrineplexus.makandeal.binder.LocationChildViewHolder;
import com.citrineplexus.makandeal.callback.OnExpandableListRender;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.util.GlobalService;
import com.citrineplexus.makandeal.view.activity.MerchantMicroSite;
import com.citrineplexus.makandeal.view.widget.BoxButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class InformationFragment extends Fragment implements
		OnExpandableListRender, OnDataRequestListener {

	private TextView vendorName = null, vendorAbout = null;
	private GalleryLooping galleryViewPager = null;
	private ExpandableListView expandListview = null;
	private ExpandableAdapter adapter = null;
	// private ProgressBar loadingBar = null;

	private List<String> listDataHeader = new ArrayList<String>();
	private HashMap<String, List<BaseEntity>> listDataChild = new HashMap<String, List<BaseEntity>>();
	private HashMap<String, List<BaseEntity>> tempHashMap = new HashMap<String, List<BaseEntity>>();
	private String vendorId = "";

	public static Fragment newInstance(String vendorId) {
		InformationFragment about = new InformationFragment();
		Bundle bundle = new Bundle();
		bundle.putString("VENDOR_ID", vendorId);
		about.setArguments(bundle);
		return about;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			vendorId = getArguments().getString("VENDOR_ID");
		}
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser&&context!=null) {
			GoogleAnalyticsLib.getInstance(getActivity()).trackView(
					"Information");
			((MerchantMicroSite) getActivity()).resetScrollView();
		}
	}
private Context context;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(this.context==null)
			this.context = context;
	}

	int totalHeight = 0;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_merchat_microsite, null);
		vendorName = (TextView) view.findViewById(R.id.vendorName);
		vendorAbout = (TextView) view.findViewById(R.id.vendorAbout);
		galleryViewPager = (GalleryLooping) view
				.findViewById(R.id.galleryViewPager);

		// View view = inflater.inflate(R.layout.fragment_expandable_listview,
		// container, false);
		view.setBackgroundColor(getResources().getColor(R.color.white));

		// SwipeRefreshLayout swipe_container = (SwipeRefreshLayout) view
		// .findViewById(R.id.swipe_container);
		// swipe_container.setEnabled(false);

		expandListview = (ExpandableListView) view
				.findViewById(R.id.expandListView);

		expandListview.setDividerHeight(1);
		// expandListview.addHeaderView(header);

		// loadingBar = (ProgressBar) view.findViewById(R.id.loadingBar);

		// prepareListData();
		adapter = new ExpandableAdapter(this);
		adapter.set_listDataHeader(listDataHeader);
		adapter.set_listDataChild(listDataChild);

		expandListview.setAdapter(adapter);

		loadData(1);
		loadData(2);
		return view;
	}

	private void loadData(int stage) {
		Bundle bundle = new Bundle();
		MerchantMicroSite activity = (MerchantMicroSite) getActivity();
		switch (stage) {
		case 1:// Load Merchant Detail
			bundle.putString("FragmentName", this.getClass().getName() + "|20");
			bundle.putString("VENDOR_ID", vendorId);
			bundle.putInt("CONTENT_ID", 1004);
			bundle.putInt("DOWNLOAD_STAGE", 20);
			break;

		case 2:// Load Branch
			bundle.putString("FragmentName", this.getClass().getName());
			bundle.putString("UDROPSHIP_VENDOR", vendorId);
			bundle.putBoolean("LOADING", false);
			bundle.putInt("CONTENT_ID", 1018);
			bundle.putInt("DOWNLOAD_STAGE", 21);
			break;

		default:
			break;
		}
		activity.getFragmentData(bundle, this);
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) adapter.getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) getActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.child_location_header,
					null);
		}
		LinearLayout backgroud = (LinearLayout) convertView
				.findViewById(R.id.headerBg);
		backgroud.setBackgroundColor(getResources().getColor(R.color.white));
		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.locationHeader);
		Drawable groupIndicator = isExpanded ? getResources().getDrawable(
				R.drawable.arrow_down) : getResources().getDrawable(
				R.drawable.arrow_up);
		lblListHeader.setCompoundDrawablesWithIntrinsicBounds(groupIndicator,
				null, null, null);
		lblListHeader.setText(headerTitle);
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final BaseEntity entity = (BaseEntity) adapter.getChild(groupPosition,
				childPosition);

		// final String childText = (String) getChild(groupPosition,
		// childPosition);

		LocationChildViewHolder holder = null;
		if (convertView == null) {

			convertView = ((LayoutInflater) getActivity().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE)).inflate(
					R.layout.child_location_row, parent, false);
			holder = new LocationChildViewHolder();
			holder.address = (TextView) convertView.findViewById(R.id.address);
			holder.contactNumber = (TextView) convertView
					.findViewById(R.id.contactNumber);
			holder.operationHour = (TextView) convertView
					.findViewById(R.id.operationHour);
			holder.contactUs_Phone = (BoxButton) convertView
					.findViewById(R.id.contactUs_Phone);
			holder.map = (BoxButton) convertView.findViewById(R.id.map);
			convertView.setTag(holder);
		} else
			holder = (LocationChildViewHolder) convertView.getTag();

		if (holder.address != null)
			holder.address.setText(Html.fromHtml(entity.get("STREET", "")
					.toString().trim()
					+ " "
					+ entity.get("ZIP", "").toString().trim()
					+ " "
					+ entity.get("CITY", "").toString().trim()));

		if (holder.contactNumber != null) {
			holder.contactNumber.setText((String) entity.get("TELEPHONE", ""));
		}

		if (entity.get("BUSINESS_HOUR") != null) {
			String businessHour = entity.get("BUSINESS_HOUR", "").toString()
					.trim();

			if (holder.operationHour != null) {
				holder.operationHour.setText(Html.fromHtml(businessHour));
			}
		}

		holder.contactUs_Phone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (entity.get("TELEPHONE") != null
						&& !entity.get("TELEPHONE", "").equals("")) {
					GoogleAnalyticsLib.getInstance(getActivity()).trackEvent(
							"Call", "From Merchant Micro Site");
					GlobalService.callSupportIntent(getActivity(),
							(String) entity.get("TELEPHONE"));
				}

			}
		});

		holder.map.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (getActivity() instanceof MerchantMicroSite) {
					String address = entity.get("STREET", "").toString().trim()
							+ " " + entity.get("ZIP", "").toString().trim()
							+ " " + entity.get("CITY", "").toString().trim();
					address = address.replaceAll("\r", "").replaceAll("\n", "");
					((MerchantMicroSite) getActivity()).openMap(address, "");
					GoogleAnalyticsLib.getInstance(getActivity()).trackEvent(
							"Map", "From Merchant Micro Site Page");
				}
			}
		});

		return convertView;
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		int downloadState = params.getInt("DOWNLOAD_STAGE", 0);
		parseEntityObject(entity, downloadState);
	}

	private synchronized void parseEntityObject(BaseEntity entity, int stage) {
		if (entity == null)
			return;

		switch (stage) {
		case 20:// Merchant Detail
			if (entity.get("VENDOR_NAME") != null) {
				// vendorNameString = (String) entity.get("VENDOR_NAME");
				vendorName.setText((String) entity.get("VENDOR_NAME"));
			}
			if (entity.get("ABOUT") != null) {
				String about = (String) entity.get("ABOUT");
				if (about != null && about.length() > 0)
					vendorAbout.setText(about);
				else
					vendorAbout.setVisibility(View.GONE);

			}

			if (entity.get("GALLERY") != null
					&& entity.get("GALLERY") instanceof ArrayList<?>) {
				ArrayList<String> galleryImage = (ArrayList<String>) entity
						.get("GALLERY");
				for (String imgUrl : galleryImage) {
					galleryViewPager.addData(imgUrl);
				}
				galleryViewPager.notifyDataChanged();
			}
			break;

		case 21:
			// loadingBar.setVisibility(View.GONE);
			if (((BaseEntity) entity).get("JSONARRAY") != null) {
				boolean finishLoop = false;
				ArrayList<BaseEntity> templist = (ArrayList<BaseEntity>) ((BaseEntity) entity)
						.get("JSONARRAY");
				ArrayList<BaseEntity> allBranchList = new ArrayList<BaseEntity>();
				for (BaseEntity abc : templist)
					allBranchList.add(abc);
				Collections.sort(allBranchList, BaseEntity.COMPARE_BY_REGION);
				if (!tempHashMap.isEmpty())
					tempHashMap.clear();
				if (!listDataHeader.isEmpty())
					listDataHeader.clear();
				if (!listDataChild.isEmpty())
					listDataChild.clear();
				while (!finishLoop) {
					// boolean startLooping = true;
					ArrayList<BaseEntity> groupArrayList = new ArrayList<BaseEntity>();
					String regionId = (String) allBranchList.get(0).get(
							"REGION_ID");
					int allItemSize = allBranchList.size();
					for (int i = 0; i < allItemSize;) {
						BaseEntity en = allBranchList.get(i);
						if (en.get("REGION_ID") != null
								&& en.get("REGION_ID").toString()
										.equals(regionId)) {
							groupArrayList.add(en);
							allBranchList.remove(en);
						} else
							i++;
						allItemSize = allBranchList.size();
					}
					tempHashMap.put(regionId, groupArrayList);
					if (allBranchList.isEmpty())
						finishLoop = true;
				}
				prepareListData();
			}
			adapter.notifyDataSetChanged();
			break;
		}
	}

	private void prepareListData() {
		SparseArray<String> myStringArray = parseStringArray(R.array.region);
		ArrayList<String> headerKey = new ArrayList<String>();
		// Adding Header data
		for (String key : tempHashMap.keySet()) {
			headerKey.add(key);
			listDataHeader.add(myStringArray.get(Integer.valueOf(key)));
		}

		for (int i = 0; i < listDataHeader.size(); i++) {
			// List<BaseEntity> tempData = tempHashMap.get(headerKey.get(i));
			listDataChild.put(listDataHeader.get(i),
					tempHashMap.get(headerKey.get(i))); // Header, Child
														// data
		}

		int listViewHeight = GlobalService
				.setExpandableListViewHeightBasedOnChildren(expandListview)
				+ vendorName.getHeight()
				+ vendorAbout.getHeight()
				+ galleryViewPager.getHeight();
		((MerchantMicroSite) getActivity()).setViewPagerSize(listViewHeight);
	}

	private SparseArray<String> parseStringArray(int stringArrayResourceId) {
		String[] stringArray = getResources().getStringArray(
				stringArrayResourceId);
		int[] intArray = getResources().getIntArray(R.array.region_id);

		SparseArray<String> outputArray = new SparseArray<String>(
				stringArray.length);
		for (int i = 0; i < intArray.length; i++) {
			outputArray.put(intArray[i], stringArray[i]);
		}
		// for (String entry : stringArray) {
		// outputArray.put(Integer.valueOf(splitResult[0]), intArray[1]);
		// }
		return outputArray;
	}

	@Override
	public void onConnectionError(Exception ex) {

	}

}
