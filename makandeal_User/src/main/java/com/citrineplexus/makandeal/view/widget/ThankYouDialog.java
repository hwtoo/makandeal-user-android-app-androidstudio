package com.citrineplexus.makandeal.view.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.callback.DialogCallback;
import com.citrineplexus.makandeal.view.activity.AbstractActivity.ViewName;

public class ThankYouDialog {

	private AlertDialog alertDialog = null;
	private String titleFlagString = "";

	public void show(final Context context, final DialogCallback listener,
			String[] msg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context,
				AlertDialog.THEME_HOLO_DARK);

		if (msg[1] != null)
			titleFlagString = msg[1];

		if (!titleFlagString.equals("ERROR"))
			builder.setTitle("Thank you");
		builder.setMessage(msg[0]);
		builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (listener != null)
					// listener.onDialogClick(which,
					// ViewName.CONNECTION_DIALOG);
					listener.onDialogClick(which,
							ViewName.THANKYOU_ONEBTN_DIALOG, titleFlagString);
				alertDialog.dismiss();
			}
		});

		alertDialog = builder.create();
		// alertDialog.setOnShowListener(new OnShowListener() {
		//
		// @Override
		// public void onShow(DialogInterface dialog) {
		// AlertDialog alertDialog = (AlertDialog) dialog;
		// Button button = alertDialog
		// .getButton(DialogInterface.BUTTON_NEUTRAL);
		// button.setTypeface(ViewController.getInstance(context)
		// .getTypeface(ViewController.ARIAL));
		// }
		// });
		alertDialog.show();

		int textViewId = alertDialog.getContext().getResources()
				.getIdentifier("android:id/alertTitle", null, null);
		if (textViewId != 0) {
			TextView tv = (TextView) alertDialog.findViewById(textViewId);
			// tv.setTypeface(ViewController.getInstance(context).getTypeface(
			// ViewController.ARIAL));
			tv.setTextColor(Color.WHITE);
		}

		int dividerId = alertDialog.getContext().getResources()
				.getIdentifier("android:id/titleDivider", null, null);
		if (dividerId != 0) {
			View divider = alertDialog.findViewById(dividerId);
			divider.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, 2));
			divider.setBackgroundColor(context.getResources().getColor(
					R.color.grey_03));
		}

		TextView messageText = (TextView) alertDialog
				.findViewById(android.R.id.message);
		// messageText.setTypeface(ViewController.getInstance(context)
		// .getTypeface(ViewController.ARIAL));
		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(15, 10, 15, 10);
		messageText.setLayoutParams(params);
		messageText.setTextSize(14);
		// messageText.setPadding(15, 10, 15, 10);
		messageText.setGravity(Gravity.CENTER);
	}
	// public void openThankYouDialog(final DialogCallback listener) {
	// AlertDialog.Builder builder = new AlertDialog.Builder(context,
	// AlertDialog.THEME_HOLO_DARK);
	// builder.setTitle("Thank you");
	// builder.setMessage(context.getResources().getString(
	// R.string.resetPasswordMsg));
	// builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// // if (listener != null)
	// // listener.onDialogClick(which, DialogName.CONNECTION_DIALOG);
	// alertDialog.dismiss();
	// }
	// });
	//
	// alertDialog = builder.create();
	// alertDialog.setOnShowListener(new OnShowListener() {
	//
	// @Override
	// public void onShow(DialogInterface dialog) {
	// AlertDialog alertDialog = (AlertDialog) dialog;
	// Button button = alertDialog
	// .getButton(DialogInterface.BUTTON_NEUTRAL);
	// button.setTypeface(ViewController.getInstance(context)
	// .getTypeface(ViewController.ARIAL));
	// }
	// });
	// alertDialog.show();
	//
	// int textViewId = alertDialog.getContext().getResources()
	// .getIdentifier("android:id/alertTitle", null, null);
	// if (textViewId != 0) {
	// TextView tv = (TextView) alertDialog.findViewById(textViewId);
	// tv.setTypeface(ViewController.getInstance(context).getTypeface(
	// ViewController.ARIAL));
	// tv.setTextColor(Color.WHITE);
	// }
	//
	// int dividerId = alertDialog.getContext().getResources()
	// .getIdentifier("android:id/titleDivider", null, null);
	// if (dividerId != 0) {
	// View divider = alertDialog.findViewById(dividerId);
	// divider.setLayoutParams(new LinearLayout.LayoutParams(
	// LayoutParams.MATCH_PARENT, 2));
	// divider.setBackgroundColor(context.getResources().getColor(
	// R.color.Grey_999));
	// }
	//
	// TextView messageText = (TextView) alertDialog
	// .findViewById(android.R.id.message);
	// messageText.setTypeface(ViewController.getInstance(context).getTypeface(
	// ViewController.ARIAL));
	// FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
	// LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	// params.setMargins(15, 10, 15, 10);
	// messageText.setLayoutParams(params);
	// messageText.setTextSize(14);
	// // messageText.setPadding(15, 10, 15, 10);
	// messageText.setGravity(Gravity.CENTER);
	// }
}