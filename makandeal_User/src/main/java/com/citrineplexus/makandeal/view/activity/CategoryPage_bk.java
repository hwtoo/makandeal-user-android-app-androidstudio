package com.citrineplexus.makandeal.view.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.citrineplexus.bello2.FilterAdapter;
import com.citrineplexus.bello2.ItemDisplayBinder;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.callback.OnDynamicListViewRefreshListener;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.view.MakanDealApplication;
import com.citrineplexus.makandeal.view.widget.DynamicDataListView;

public class CategoryPage_bk extends AbstractActivity implements
		OnDynamicListViewRefreshListener, OnItemClickListener {

	private DynamicDataListView mListview = null;
	private ProgressBar mLoadingProgressBar = null;
	private int filterInt = -1;
	private TextView mNoContentMsg = null;
	private ImageView noContentImg = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_page);
		mNoContentMsg = (TextView) findViewById(R.id.noContentMsg);
		noContentImg = (ImageView) findViewById(R.id.noConteImage);
		mLoadingProgressBar = (ProgressBar) findViewById(R.id.loadingProgressBar);

		if (getIntent() != null && getIntent().getExtras() != null) {
			Bundle bundle = getIntent().getExtras();
			String filterName = bundle.getString("FILTERNAME");
			getSupportActionBar().setTitle(filterName);
			GoogleAnalyticsLib.getInstance(this).trackView(filterName);
			filterInt = bundle.getInt("FLITERBY");
		}
		View emptyView = getLayoutInflater().inflate(
				R.layout.listview_empty_view, null);
		ListView listview = (ListView) findViewById(R.id.pullToRefreshID);
		listview.setEmptyView(emptyView);
		SwipeRefreshLayout swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);

		FilterAdapter adapter = new FilterAdapter(this);
		adapter.setDataSet(MakanDealApplication.getInstance()
				.getDealArrayList());

		mListview = new DynamicDataListView(this, this, listview, swipeLayout,
				adapter, new ItemDisplayBinder(), this, filterInt,-1);

		if (adapter.isEmpty()) {
			initProductExt();
		}

	}

	private void initProductExt() {
		mListview.setRequestData(true);
		mListview.setSwipeEnable(false);
		mListview.initScorllLIstener();
		Bundle localBundle = new Bundle();
		localBundle.putString("REQUEST_TYPE", "INITIAL");
		localBundle.putString("PAGE", "1");
		getServiceData(1002, 0, localBundle, false);
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		if (entity == null || params == null)
			return;
		Message msg = new Message();

		if (params.getString("REQUEST_TYPE").equals("INITIAL"))
			msg.what = 1001;
		else if (params.getString("REQUEST_TYPE").equals("ADDITIONAL"))
			msg.what = 1002;
		msg.obj = entity;
		handler.sendMessage(msg);
	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_UNLOCKED;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.ALL;
	}

	@Override
	int drawerID() {
		return 0;
	}

	@Override
	void handlerMessage(Message msg) {
		switch (msg.what) {
		case 1001:// Refresh All Data
			mListview.clearData();

		case 1002:// Additional Data
			if (msg.obj != null)
				if (msg.obj instanceof BaseEntity) {
					if (((BaseEntity) msg.obj).get("JSONARRAY") != null) {
						ArrayList<BaseEntity> dataAry = (ArrayList<BaseEntity>) ((BaseEntity) msg.obj)
								.get("JSONARRAY");
						// if (!(isLastPage =
						// mListview.checklastTime(dataAry
						// .get(dataAry.size() - 1))))
						int count = 0;
						for (BaseEntity en : dataAry)
							mListview.addData(en,false);
					}
				} else if (msg.obj instanceof ArrayList<?>) {
					mListview.addData((ArrayList<BaseEntity>) msg.obj, null,false);
				}

			mListview.notifyDataSetChanged();
			break;

		case 1003:
			if (msg.obj instanceof LoadResponseFlag)
				onLoadResponse((LoadResponseFlag) msg.obj);
			break;

		}
	}

	@Override
	public void onRefresh() {
		if (mListview != null && !mListview.isRequestingData()) {
			// mListview.resetFeatureCount();
			// if (requestingNewData) {
			// ((PullToRefreshListView) view).onRefreshComplete();
			// return;
			// }
			MakanDealApplication.lastRequestPage = 1;
			initProductExt();
			// mListview.addFooter();
			// requestInitialData();
		}

		// mListview.addFooter();
	}

	@Override
	public void onLoadMore() {
		int count = mListview.getCount();
		if (!mListview.isRequestingData() && /* !isLastPage */
		MakanDealApplication.lastRequestPage != (1 + count / 50)) {
			mListview.setRequestData(true);
			Bundle localBundle = new Bundle();
			localBundle.putString("REQUEST_TYPE", "ADDITIONAL");

			MakanDealApplication.lastRequestPage = (1 + count / 50);
			localBundle.putString("PAGE", String.valueOf((1 + count / 50)));
			getServiceData(1002, 0, localBundle, false);
		} else {
			if (mListview.isEmpty()) {
				Message msg = new Message();
				msg.what = 1003;
				msg.obj = LoadResponseFlag.NO_CONTENT;
				handler.sendMessage(msg);
			}
			mListview.removeFooter();
		}
	}

	@Override
	public void onLoadResponse(LoadResponseFlag flag) {
		mLoadingProgressBar.setVisibility(View.GONE);
		switch (flag) {
		case HAVE_CONTENT:
			mNoContentMsg.setVisibility(View.GONE);
			noContentImg.setVisibility(View.GONE);
			mListview.setVisible(View.VISIBLE);
			break;

		case NO_CONNECTION:
			mNoContentMsg.setText(R.string.noConnectionMsg);
			noContentImg.setVisibility(View.GONE);

		case NO_CONTENT:
			mNoContentMsg.setVisibility(View.VISIBLE);
			noContentImg.setVisibility(View.VISIBLE);
			mListview.setVisible(View.GONE);
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		BaseEntity entity = (BaseEntity) mListview.getAdatper().getItem(
				position);

		if (entity.get("ENTITY_ID") != null) {
			Bundle param = new Bundle();
			param.putString("ENTITY_ID", entity.get("ENTITY_ID").toString());
			openView(ViewName.DEAL_PAGE, param);
		}
	}
}
