package com.citrineplexus.makandeal.view.activity;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.citrineplexus.library.slidingTab.SlidingTabDividerLayout;
import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.DealPageViewPager;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.util.GlobalService;
import com.citrineplexus.makandeal.view.MakanDealApplication;
import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

public class DealPage extends AbstractActivity implements OnClickListener {
	private SlidingTabDividerLayout slidingTabLayout = null;
	private ViewPager viewPager = null;
	private DealPageViewPager pagerAdapter = null;
	private Button buyNowBtn = null;
	private String entityId = "";
	private String vendorId = "";

	private ArrayList<OnDataRequestListener> fragmentDataRequestListenerAry = new ArrayList<OnDataRequestListener>();
	private BaseEntity pageEntity = null;
	private String entityName = "", entityImage = "";
	private boolean isNotification = false;

	// TODO: Facebook SDK
	 private CallbackManager callbackManager;
	 private ShareDialog shareDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_deal_page);

		buyNowBtn = (Button) findViewById(R.id.buyNowBtn);
		buyNowBtn.setOnClickListener(this);

		slidingTabLayout = (SlidingTabDividerLayout) findViewById(R.id.slidingTabLayout);
		slidingTabLayout.setDistributeEvenly(true);
		slidingTabLayout
				.setCustomTabColorizer(new SlidingTabDividerLayout.TabColorizer() {

					@Override
					public int getIndicatorColor(int position) {
						return Color.TRANSPARENT;
					}
				});

		if (getIntent() != null && getIntent().getExtras() != null) {
			Bundle bundle = getIntent().getExtras();
			isNotification = bundle.getBoolean("GCM_MESSAGE");

			if (bundle.get("ENTITY_ID") != null)
				entityId = bundle.getString("ENTITY_ID");
			if (bundle.get("ENTITY_IMG") != null)
				entityImage = bundle.getString("ENTITY_IMG");
			if (bundle.get("VENDOR_ID") != null)
				vendorId = bundle.getString("VENDOR_ID");
		}

		pageEntity = MakanDealApplication.getInstance()
				.getServiceVoucherByEntityID(entityId);

		if (pageEntity != null && pageEntity.get("VENDOR_NAME") != null)
			setTitle((String) pageEntity.get("VENDOR_NAME"));

		// entityName = (String) pageEntity.get("NAME");
		// GoogleAnalyticsLib.getInstance(this).trackView(entityName);

		// pagerAdapter = new DealPageViewPager(getSupportFragmentManager(),
		// entityId);

		// TODO : stage 2
		pagerAdapter = new DealPageViewPager(getSupportFragmentManager(),
				entityId, vendorId);

		viewPager = (ViewPager) findViewById(R.id.viewPagerID);
		viewPager.setAdapter(pagerAdapter);

		slidingTabLayout.setViewPager(viewPager);

		// TODO: Facebook SDK
		 callbackManager = CallbackManager.Factory.create();
		 shareDialog = new ShareDialog(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	public String getEntityImageUrl() {
		return this.entityImage;
	}

	public BaseEntity getPageEntity() {
		return pageEntity;
	}

	public void setPageEntity(BaseEntity pageEntity) {
		this.pageEntity = pageEntity;
		if (pageEntity != null && pageEntity.get("VENDOR_NAME") != null)
			setTitle((String) pageEntity.get("VENDOR_NAME"));
		setEntityName((String) this.pageEntity.get("NAME"));
	}

	private void setEntityName(String entityName) {
		this.entityName = entityName;
		GoogleAnalyticsLib.getInstance(this).trackView(this.entityName);
	}

	public void setSaleAble(boolean saleable) {
		// GradientDrawable buyNowBackground = (GradientDrawable) buyNowBtn
		// .getBackground();
		if (saleable) {
			// buyNowBackground.setColor(getResources().getColor(R.color.red_01));
			buyNowBtn.setBackgroundColor(getResources()
					.getColor(R.color.red_01));
			buyNowBtn.setText("Buy Now");
		} else {
			// buyNowBtn.setBackgroundResource(R.drawable.round_grey_bg);
			// buyNowBackground.setColor(getResources().getColor(R.color.grey_03));
			buyNowBtn.setBackgroundColor(getResources().getColor(
					R.color.grey_03));
			buyNowBtn.setText("Sold Out");
			buyNowBtn.setOnClickListener(null);
		}
	}

	HashMap<String, BaseEntity> allDataHashMap = new HashMap<String, BaseEntity>();

	public void getFragmentData(Bundle bundle, OnDataRequestListener listener) {
		if (!fragmentDataRequestListenerAry.contains(listener))
			fragmentDataRequestListenerAry.add(listener);
		String fragmentClass = bundle.getString("FragmentName");

		if (allDataHashMap.containsKey(fragmentClass)) {
			for (OnDataRequestListener listeners : fragmentDataRequestListenerAry)
				listeners.onAquireResult(allDataHashMap.get(fragmentClass), 0,
						bundle);
		} else {
			getDataFromServer(bundle, bundle.getInt("CONTENT_ID"),
					bundle.getBoolean("LOADING", false));
		}
	}

	public void removeFragmentDataRequestListener(
			OnDataRequestListener fragmentDataRequestListener) {
		if (fragmentDataRequestListenerAry
				.contains(fragmentDataRequestListener))
			fragmentDataRequestListenerAry.remove(fragmentDataRequestListener);
	}

	public void getDataFromServer(Bundle param, int contentID,
			boolean loadingFlag) {
		getServiceData(contentID, 0, param, loadingFlag);
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		if (params.getBoolean("LOADING"))
			super.onAquireResult(entity, position, params, true);

		String fragmentName = params.getString("FragmentName");

		if (fragmentName.equalsIgnoreCase("AboutFragement")) {
			fragmentName = fragmentName + "|" + params.getInt("DOWNLOAD_STAGE");
		}
		if (!allDataHashMap.containsKey(fragmentName)) {
			allDataHashMap.put(fragmentName, entity);
		}
		BaseEntity en = new BaseEntity();
		en = en.merge(entity);
		for (OnDataRequestListener fragementListner : fragmentDataRequestListenerAry)
			fragementListner.onAquireResult(en, position, params);
	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_UNLOCKED;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.NO_SEARCH;
	}

	@Override
	int drawerID() {
		return -1;
	}

	@Override
	void handlerMessage(Message msg) {

	}

	public void openMap(String address, String shopName) {
		Bundle param = new Bundle();
		param.putString("ADDRESS", address);
		if (pageEntity != null && pageEntity.get("VENDOR_NAME") != null)
			param.putString("SHOPNAME", pageEntity.get("VENDOR_NAME")
					.toString());
		openView(ViewName.MAP, param);
	}

	public void openFinePrint() {
		openView(ViewName.FINEPRINT, null);
	}

	public void openMerchantStore() {
		Bundle merchantBundle = new Bundle();
		merchantBundle.putString("ENTITY_ID", entityId);
		merchantBundle.putString("VENDOR_ID",
				(String) pageEntity.get("UDROPSHIP_VENDOR", ""));
		// merchantBundle.putString("VENDOR_NAME",
		// (String) pageEntity.get("VENDOR_NAME", ""));
		openView(ViewName.MERCHANT_VIEW, merchantBundle);
	}

	@Override
	public void onBackPressed() {
		if (isNotification) {
			openView(ViewName.HOMEPAGE, null);
			finish();
		} else
			super.onBackPressed();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.buyNowBtn:
			GoogleAnalyticsLib.getInstance(this).trackEvent("Buy Now",
					entityName);
			Bundle bundle = new Bundle();
			bundle.putString("ENTITY_ID", entityId);
			openView(ViewName.SELECT_DEAL, bundle);
			break;

		case R.id.addWishlistBtn:
			GlobalService.AddToWishlist(this, pageEntity);
			break;

		default:
			break;
		}
	}

	// TODO: Facebook SDK
	public void shareToFacebook() {
		ShareLinkContent linkContent = new ShareLinkContent.Builder()
				.setContentTitle("Makandeal")
				.setContentUrl(Uri.parse(pageEntity.get("URL_KEY").toString()))
				.build();
	 shareDialog.show(linkContent);
	}

	public void shareToWhatsApp() {
		Intent sendIntent = new Intent();
		sendIntent.setPackage("com.whatsapp");
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, pageEntity.get("NAME")
				.toString() + " - " + pageEntity.get("URL_KEY").toString());
		sendIntent.setType("text/plain");
		startActivity(sendIntent);
	}

	public void shareTwitter() {
		// Twitter twitter = TwitterFactory.getSingleton();
		// Status status;
		// try {
		// status = twitter.updateStatus(pageEntity.get("NAME").toString()
		// + " - " + pageEntity.get("URL_KEY").toString());
		// System.out.println("Successfully updated the status to ["
		// + status.getText() + "].");
		// } catch (TwitterException e) {
		// e.printStackTrace();
		// }

		String tweetUrl = String.format(
				"https://twitter.com/intent/tweet?text=%s&url=%s",
				urlEncode(pageEntity.get("NAME").toString()),
				urlEncode(pageEntity.get("URL_KEY").toString()));
		Intent intent = new Intent(
				Intent.ACTION_VIEW,
				Uri.parse(tweetUrl
						+ "&related=twitterapi%2Ctwitter&via=Makandeal&hashtags=Makandeal"));

		List<ResolveInfo> matches = getPackageManager().queryIntentActivities(
				intent, 0);
		for (ResolveInfo info : matches) {
			if (info.activityInfo.packageName.toLowerCase().startsWith(
					"com.twitter")) {
				intent.setPackage(info.activityInfo.packageName);
			}
		}

		startActivity(intent);
	}

	public static String urlEncode(String s) {
		try {
			return URLEncoder.encode(s, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException("URLEncoder.encode() failed for " + s);
		}
	}
}
