package com.citrineplexus.makandeal.view.activity;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.support.v7.widget.SearchView.OnSuggestionListener;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.citrineplexus.library.gcm.GCMRegisterHandler;
import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.NavDrawerAdapter;
import com.citrineplexus.makandeal.adapter.NavDrawerAdapter.ViewHolder;
import com.citrineplexus.makandeal.callback.DialogCallback;
import com.citrineplexus.makandeal.callback.OnRefreshCartListener;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.entity.NavDrawerItem;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.service.OAuthAuthorization;
import com.citrineplexus.makandeal.service.ServiceGateway;
import com.citrineplexus.makandeal.view.MakanDealApplication;
import com.citrineplexus.makandeal.view.widget.MakanDealProgressDialog;
import com.citrineplexus.makandeal.view.widget.ResetPasswordDialog;
import com.citrineplexus.makandeal.view.widget.ThankYouDialog;

public abstract class AbstractActivity extends ActionBarActivity implements
        OnDataRequestListener, OnRefreshCartListener {

    protected Toolbar toolbar;
    private TextView cartItemNum;
    protected SearchView searchView = null;
    private MakanDealProgressDialog mProgressDialog;
    protected boolean loadingOpen = false;

    protected final int SHOW_EXCEPTION = -1000;
    protected final int LOADING_OPEN = -1011;
    protected final int LOADING_CLOSE = -1012;
    private final int OFF_PASSCODE = -1013;
    private final int UPDATE_CART = -1014;

    protected ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout = null;
    private FrameLayout contentLayout;

    private ListView navDrawerList;
    private NavDrawerAdapter navDrawerAdapter;

    private TextView userName = null, userEmail = null;
    protected boolean fromCartView = false;

    private OnClickListener actionBarListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.action_cart:
                    openView(ViewName.CART_VIEW, null);
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.abstract_activity);
        toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        if (getIntent() != null && getIntent().getExtras() != null
                && getIntent().getExtras().get("FROM_CARTVIEW") != null) {
            fromCartView = getIntent().getExtras().getBoolean("FROM_CARTVIEW",
                    false);
        }
        initDrawer();
        contentLayout = (FrameLayout) findViewById(R.id.frameLayout);
        setHomeAsIndicator();
    }

    protected void onResume() {
        super.onResume();
        // MakanDealApplication.getInstance().setCartListener(this);
        MakanDealApplication.getInstance().addCartListener(this);
        updateMenu();
        setMenuChecked(drawerID());
    }

    protected void openDrawer(boolean toOpen) {
        if (toOpen)
            drawerLayout.openDrawer(Gravity.LEFT);
        else
            drawerLayout.closeDrawer(Gravity.LEFT);
    }

    protected void setMenuChecked(int id) {
        for (int i = 0; i < navDrawerAdapter.getCount(); i++) {
            NavDrawerItem item = navDrawerAdapter.getItem(i);
            if (item.getId() == id) {
                navDrawerList.setItemChecked((i + 1), true);
                break;
            }
        }

    }

    private void setHomeAsIndicator() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icon_back);
    }

    private void initSearchView(final MenuItem searchItem) {
        SearchManager searchManger = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null)
            searchView = (SearchView) searchItem.getActionView();

        if (searchView != null) {
            searchView.setSearchableInfo(searchManger
                    .getSearchableInfo(getComponentName()));

            searchView.setOnSuggestionListener(new OnSuggestionListener() {

                @Override
                public boolean onSuggestionSelect(int position) {
                    return false;
                }

                @Override
                public boolean onSuggestionClick(int position) {
                    Cursor c = (Cursor) searchView.getSuggestionsAdapter()
                            .getItem(position);
                    String query = c.getString(1);
                    searchView.setQuery(query, true);
                    return true;
                }
            });

            searchView
                    .setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {

                            if (!hasFocus) {
                                if (searchItem != null) {
                                    searchItem.collapseActionView();
                                }
                                if (searchView != null) {
                                    searchView.setQuery("", false);

                                }
                            }

                        }
                    });

            if (setActionBarMethod() == ActionBarMethod.IS_SEARCHVIEW)
                searchView.setOnQueryTextListener(new OnQueryTextListener() {

                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        if (searchItem != null) {
                            searchItem.collapseActionView();
                        }
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String arg0) {
                        return false;
                    }
                });

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (setActionBarMethod() != ActionBarMethod.HIDE_ALL) {
            inflater.inflate(R.menu.action_bar_menu, menu);
            View cart = menu.findItem(R.id.action_cart).getActionView();
            cart.setOnClickListener(actionBarListener);
            cartItemNum = (TextView) cart.findViewById(R.id.cartItemNum);

            if (cartItemNum != null) {
                handler.sendEmptyMessage(UPDATE_CART);
            }

            if (setActionBarMethod() != ActionBarMethod.NO_SEARCH) {
                MenuItem searchItem = menu.findItem(R.id.action_search);
                searchItem.setVisible(true);
                initSearchView(searchItem);
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Options Item handler
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Init the drawer, if the DrawerLayout Lock mode ==
     * DrawerLayout.LOCK_MODE_UNLOCKED
     */
    private void initDrawer() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navDrawerList = (ListView) findViewById(R.id.left_drawer);
        drawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this,
                drawerLayout, toolbar, 0, 0) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                // invalidateOptionsMenu(); // creates call to
                // onPrepareOptionsMenu()
                syncState();
            }

            public void onDrawerOpened(View view) {
                super.onDrawerOpened(view);
                syncState();
            }
        };
        navDrawerAdapter = new NavDrawerAdapter(this, R.layout.navdrawer_item);
        View header = getLayoutInflater().inflate(R.layout.navdrawer_header,
                null, false);
        userEmail = (TextView) header.findViewById(R.id.drawerUserEmail);
        userName = (TextView) header.findViewById(R.id.drawerUserName);
        navDrawerList.addHeaderView(header, "header", false);

        navDrawerList.setAdapter(navDrawerAdapter);
        navDrawerList.setOnItemClickListener(drawerItemClick);
        boolean drawermode = setDrawerLockMode() == DrawerLayout.LOCK_MODE_LOCKED_CLOSED ? false
                : true;
        drawerToggle.setDrawerIndicatorEnabled(drawermode);
        if (!drawermode) {
            drawerToggle
                    .setToolbarNavigationClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            setResult(RESULT_OK);
                            finish();
                        }
                    });
        }

        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
        // updateMenu();
    }

    protected void updateMenu() {
        if (!navDrawerAdapter.isEmpty())
            navDrawerAdapter.clear();

        if (OAuthAuthorization.getInstance().isUserLogin()
                && OAuthAuthorization.getInstance().getUserProfileData() != null) {
            BaseEntity userProfile = OAuthAuthorization.getInstance()
                    .getUserProfileData();

            for (NavDrawerItem drawerItem : MakanDealApplication.onlineMenu) {
                navDrawerAdapter.add(drawerItem);
            }

            if (navDrawerList.getHeaderViewsCount() > 0 && userEmail != null
                    && userName != null) {
                userEmail.setVisibility(View.VISIBLE);
                userName.setVisibility(View.VISIBLE);
                userEmail.setText((String) userProfile.get("EMAIL", ""));
                userName.setText((String) userProfile.get("FIRSTNAME", "")
                        + " " + (String) userProfile.get("LASTNAME", ""));
            }
        } else {
            for (NavDrawerItem drawerItem : MakanDealApplication.offlineMenu) {
                navDrawerAdapter.add(drawerItem);
            }
            userEmail.setVisibility(View.GONE);
            userName.setVisibility(View.GONE);
            userEmail.setText("");
            userName.setText("");
        }

        navDrawerAdapter.notifyDataSetChanged();
    }

    /**
     * Set the Drawer Lock Mode DrawerLayout.LOCK_MODE_LOCKED_OPEN = Keep
     * opening DrawerLayout. DrawerLayout.LOCK_MODE_LOCKED_CLOSED = Disable the
     * DrawerLayout. DrawerLayout.LOCK_MODE_UNLOCKED = DrawerLayout to Normal.
     */
    abstract int setDrawerLockMode();

    abstract ActionBarMethod setActionBarMethod();

    abstract int drawerID();

    /**
     * Handle UI Thread
     *
     * @param msg
     */
    abstract void handlerMessage(Message msg);

    public void setContentView(int layoutResID) {
        contentLayout.addView(getLayoutInflater().inflate(layoutResID, null));
    }

    /**
     * Tracker back button. if Drawer open, then close the drawer
     */
    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        } else
            super.onBackPressed();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (drawerToggle != null)
            drawerToggle.syncState();
    }

    /**
     * LeftDrawer onItemClickListener
     */
    private OnItemClickListener drawerItemClick = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            Bundle params;
            NavDrawerItem item = navDrawerAdapter.getItem(position - 1);

            switch (item.getId()) {
                case 100:// Open Sign In Page
                    openView(ViewName.SIGN_IN, null);
                    break;

                case 101:// Open Sign Up Page
                    openView(ViewName.SIGN_UP, null);
                    break;

                case 102:// Open Home Page
                    openView(ViewName.HOMEPAGE, null);
                    break;

                case 103:// Open Category page
                    openView(ViewName.CATEGORY_PAGE, null);
                    break;

                case 104:// Open Voucher page
                    openView(ViewName.VOUCHER, null);
                    break;

                case 105:// Open Wishlist page
                    openView(ViewName.WISHLIST, null);
                    break;

                case 106:// Open My
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("FROM_CARTVIEW", false);
                    openView(ViewName.CREDIT, bundle);
                    break;

                case 107:// Open Earn RM2
                    openView(ViewName.EARN_RM2, null);
                    break;

                case 114:// Open Notification
                    // SwitchCompat mNotification = (SwitchCompat) view
                    // .findViewById(R.id.navSwitch);
                    // SwitchCompat mNotification = (SwitchCompat)
                    // parent.getChildAt(
                    // position).findViewById(R.id.navSwitch);
                    // SwitchCompat mSwitch = (SwitchCompat)
                    // parent.getChildAt(position)
                    // .findViewById(R.id.navSwitch);
                    SwitchCompat mNotification = ((ViewHolder) view.getTag()).switchHolder;

                    try {
                        if (MakanDealApplication.getInstance()
                                .getNotificationFlag()) {
                            Toast.makeText(AbstractActivity.this,
                                    "Notifications has been disabled.",
                                    Toast.LENGTH_SHORT).show();
                            new GCMRegisterHandler()
                                    .unregisterAppToServer(AbstractActivity.this);
                        } else {
                            Toast.makeText(AbstractActivity.this,
                                    "Notifications has been enable.",
                                    Toast.LENGTH_SHORT).show();
                            new GCMRegisterHandler()
                                    .registerApp(AbstractActivity.this);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    MakanDealApplication.getInstance().setNotificationFlag();
                    mNotification.setChecked(MakanDealApplication.getInstance()
                            .getNotificationFlag());
                    return;

                case 108:// Open Passcode
                    // SwitchCompat mSwitch = (SwitchCompat) view
                    // .findViewById(R.id.navSwitch);
                    // SwitchCompat mSwitch = (SwitchCompat) parent.getChildAt(
                    // position).findViewById(R.id.navSwitch);
                    SwitchCompat mSwitch = ((ViewHolder) view.getTag()).switchHolder;
                    if (mSwitch != null)
                        if (!mSwitch.isChecked()) {
                            Bundle localBundle = new Bundle();
                            localBundle.putInt("PASS_OPTION", 2);
                            openView(ViewName.PASSCODE, localBundle);
                            GoogleAnalyticsLib.getInstance(AbstractActivity.this)
                                    .trackEvent("Passcode", "ON");
                        } else {
                            MakanDealApplication.getInstance().getPreferences()
                                    .edit().remove("PASSCODE").commit();
                            handler.sendEmptyMessage(OFF_PASSCODE);
                            mSwitch.setChecked(MakanDealApplication.getInstance()
                                    .getPreferences().getString("PASSCODE", "")
                                    .equals("") ? false : true);
                            GoogleAnalyticsLib.getInstance(AbstractActivity.this)
                                    .trackEvent("Passcode", "OFF");
                            return;
                        }

                    return;

                case 109:// Open FAQ Page
                    params = new Bundle();
                    params.putInt("TITLE_ID", R.string.faqTitle);
                    params.putInt("MSG_ID", R.string.faqLink);
                    params.putInt("PAGE_ID", 109);
                    openView(ViewName.TERM_CONDITION, params);
                    break;

                case 110:// Open TNC page
                    params = new Bundle();
                    params.putInt("TITLE_ID", R.string.TermNCondtionTitle);
                    params.putInt("MSG_ID", R.string.tncLink);
                    params.putInt("PAGE_ID", 110);
                    openView(ViewName.TERM_CONDITION, params);
                    break;

                case 111:// Open Privacy policy Page
                    params = new Bundle();
                    params.putInt("TITLE_ID", R.string.privacyTitle);
                    params.putInt("MSG_ID", R.string.privacyLink);
                    params.putInt("PAGE_ID", 111);
                    openView(ViewName.TERM_CONDITION, params);
                    break;

                case 112:// Open Contact Us
                    openView(ViewName.CONTACT_US, null);
                    break;

                case 113:// Sign Out
                    OAuthAuthorization.getInstance().logoutUser();
                    updateMenu();
                    openView(ViewName.HOMEPAGE, null);
                    Toast.makeText(AbstractActivity.this,
                            "You have successfully sign out.", Toast.LENGTH_SHORT)
                            .show();
                    GoogleAnalyticsLib.getInstance(AbstractActivity.this)
                            .trackEvent("Sign Out", null);
                    break;


                case 116:// Makandeal Blog
                    params = new Bundle();
                    params.putInt("TITLE_ID", R.string.blogTitle);
                    params.putInt("MSG_ID", R.string.blogLink);
                    params.putInt("PAGE_ID", 116);
                    openView(ViewName.TERM_CONDITION, params);
                    break;

                default:
                    break;
            }

            navDrawerList.setItemChecked(position, true);
            drawerLayout.closeDrawer(Gravity.LEFT);
        }
    };

    /**
     * Open another activity
     *
     * @param viewName enum for activity name
     * @param param    parameter to pass
     */
    public void openView(ViewName viewName, Bundle param) {
        Intent localIntent = null;
        ActivityOptions options = null;
        switch (viewName) {
            case HOMEPAGE:
                // if (this instanceof HomePage)
                // break;
                localIntent = new Intent(this, HomePage.class);

                // if (param != null && param.get("EXITAPP") != null
                // && param.getBoolean("EXITAPP", false)) {
                localIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // } else
                // localIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
                // | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                break;
            case MAP:
                localIntent = new Intent(this, MapPage.class);
                break;

            case TERM_CONDITION:
                localIntent = new Intent(this, TermNConditionsPage.class);
                localIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                break;

            case SIGN_IN:
                localIntent = new Intent(this, SignInPage.class);
                break;

            case SIGN_UP:
                localIntent = new Intent(this, SignUpPage.class);
                break;

            case EARN_RM2:
                localIntent = new Intent(this, EarnRM2Page.class);
                if (param != null && param.get("FROM_PAYMENT") != null
                        && param.getBoolean("FROM_PAYMENT"))
                    localIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                else
                    localIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                break;

            case CONTACT_US:
                localIntent = new Intent(this, ContactUsPage.class);
                localIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                break;

            case CATEGORY_PAGE:
                localIntent = new Intent(this, CategoryPage.class);
                localIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                break;

            case PASSCODE:
                localIntent = new Intent(this, PasscodePage.class);
                localIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                break;

            case FILTER:
//			localIntent = new Intent(this, CategoryPage_bk.class);
                localIntent = new Intent(this, FilterPage.class);
                localIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                break;

            case DEAL_PAGE:
                localIntent = new Intent(this, DealPage.class);
                break;

            case WISHLIST:
                localIntent = new Intent(this, WishlistPage.class);
                localIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                break;

            case VOUCHER:
                localIntent = new Intent(this, VoucherPage.class);
                if (param != null && param.get("FROM_PAYMENT") != null
                        && param.getBoolean("FROM_PAYMENT"))
                    // localIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    // | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    localIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                else
                    localIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                break;

            case FINEPRINT:
                localIntent = new Intent(this, FinePrintPage.class);
                break;

            case MERCHANT_VIEW:
//			localIntent = new Intent(this, MerchantStorePage.class);
                localIntent = new Intent(this, MerchantMicroSite.class);
                break;

            // TODO: Remove CampaignPage for stage 2
            // case CAMPAIGN_VIEW:
            // localIntent = new Intent(this, CampaignPage.class);
            // localIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            // break;

            case SELECT_DEAL:
                localIntent = new Intent(this, SelectDealPage.class);
                break;

            case CART_VIEW:
                localIntent = new Intent(this, CartView.class);
                break;

            case CREDIT:
                localIntent = new Intent(this, CreditPage.class);
                break;

            case CHECKOUT:
                localIntent = new Intent(this, CheckoutPage.class);
                break;

            case REDEEM_VIEW:
                localIntent = new Intent(this, RedeemPage.class);
                break;
            default:
                break;

        }

        if (localIntent != null) {
            int requestCode = 1;
            if (param != null) {
                requestCode = param.getInt("REQUEST_CODE", 1);
                localIntent.putExtras(param);
            }
            startActivityForResult(localIntent, requestCode);
        }
    }

    // TODO: apply transition Animation
//	@SuppressLint("NewApi")
//	public void openView(ViewName viewName, Bundle param, View... views) {
//		Intent localIntent = null;
//		ActivityOptions options = null;
//		switch (viewName) {
//
//		case DEAL_PAGE:
//			localIntent = new Intent(this, DealPage.class);
//			options = ActivityOptions.makeSceneTransitionAnimation(this,
//					views[0], "productImageTransition");
//			break;
//
//		default:
//			break;
//
//		}
//
//		if (localIntent != null) {
//			if (param != null)
//				localIntent.putExtras(param);
//			startActivityForResult(localIntent, 1, options.toBundle());
//		}
//	}

    /**
     * Open Dialog
     *
     * @param dialogName
     * @param listener
     * @param dialogInfo
     */
    public void openDialog(ViewName dialogName, DialogCallback listener,
                           String[] dialogInfo) {

        switch (dialogName) {
            case RESET_DIALOG:
                ResetPasswordDialog resetDialog = new ResetPasswordDialog(this);
                resetDialog.init(dialogName, listener);
                break;

            case THANKYOU_ONEBTN_DIALOG:
                new ThankYouDialog().show(this, listener, dialogInfo);
                break;

            default:
                break;
        }
    }

    Handler handler = new Handler() {
        public void handleMessage(Message msg) {

            if (!isFinishing()) {

                switch (msg.what) {
                    case SHOW_EXCEPTION:
                        Toast.makeText(AbstractActivity.this,
                                getString(R.string.exceptionMsg),
                                Toast.LENGTH_SHORT).show();
                    case LOADING_CLOSE:
                        closeLoading();
                        break;
                    case LOADING_OPEN:// open progress dialog
                        openLoading();
                        break;

                    case OFF_PASSCODE:// off Passcode message
                        Toast.makeText(AbstractActivity.this,
                                "Passcode has been disabled.", Toast.LENGTH_SHORT)
                                .show();
                        break;

                    case UPDATE_CART:// update cart number
                        onRefreshCart(MakanDealApplication.getInstance()
                                .getCartItemNumber());
                        break;
                }
            }
            handlerMessage(msg);
        }
    };

    /**
     * Refresh cart Item Number display
     */
    @Override
    public void onRefreshCart(int cartItemCount) {
        setCartItemNum(cartItemCount);
    }

    /**
     * Set Cart total item
     *
     * @param num
     */
    public void setCartItemNum(final int num) {
        if (cartItemNum != null)
            if (num > 0) {
                cartItemNum.setText(String.valueOf(num));
                cartItemNum.setVisibility(View.VISIBLE);
            } else {
                cartItemNum.setVisibility(View.GONE);
            }
    }

    /**
     * Open Loading
     */
    private void openLoading() {
        loadingOpen = true;
        if (mProgressDialog != null)
            mProgressDialog = null;
        mProgressDialog = new MakanDealProgressDialog(this);
        mProgressDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    /**
     * Cloase Loading
     */
    private void closeLoading() {
        // if (loadingListener != null)
        // loadingListener
        // .onLoadingStateChange(LoadingListener.LoadingState.CLOSE);
        loadingOpen = false;
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    /**
     * Get service Data
     *
     * @param contentID
     * @param position
     * @param params
     * @param loadingFlag
     */

    protected void getServiceData(int contentID, int position, Bundle params,
                                  boolean loadingFlag) {
        if (loadingFlag && !loadingOpen) {
            handler.sendEmptyMessage(LOADING_OPEN);
        }

        switch (contentID) {
            case 1042:
            case 1020:
                new ServiceGateway().getData(this, contentID,
                        MakanDealApplication.getInstance(), position, params);
                break;

            default:
                new ServiceGateway().getData(this, contentID, this, position,
                        params);
                break;
        }
    }

    protected void checkoutDeal(int contentID, Bundle params,
                                boolean loadingFlag, BaseEntity checkoutEntity) {
        if (loadingFlag && !loadingOpen) {
            handler.sendEmptyMessage(LOADING_OPEN);
        }

        if (params == null)
            params = new Bundle();
        params.putInt("CONTENT_ID", contentID);
        new ServiceGateway().checkOut(this, this, params, checkoutEntity);
    }

    protected Intent handleEventAndPromoLogic2(BaseEntity entity) {
        boolean hasLogic = ((String) entity.get("HAS_LOGIC")).equals("1");
        if (hasLogic) {
            Bundle localBundle = new Bundle();
            localBundle.putString("CAMPAIGN_NAME",
                    (String) entity.get("CAMPAIGN_NAME"));
            localBundle.putInt("CAMPAIGN_VIEW",
                    Integer.valueOf((String) entity.get("CAMPAIGN_VIEW")));
            localBundle
                    .putString("CAMPAIGN_EVENT", (String) entity.get("TYPE"));
            localBundle.putString("CAMPAIGN_IMGURL",
                    (String) entity.get("IMAGE_URL"));

            Intent mIntent = new Intent();
            localBundle.putBoolean("ISCAMPAIGN", true);
            mIntent.putExtras(localBundle);
            return mIntent;
        } else {
            String campaignView = (String) entity.get("CAMPAIGN_VIEW");
            if (campaignView.equalsIgnoreCase("sign-up-campaign")) {
                if (OAuthAuthorization.getInstance().isUserLogin()) {
                    openDrawer(true);
                } else {
                    openView(ViewName.SIGN_UP, null);
                }
            }
            return null;
        }
        // this.finish();
    }

    protected void handleEventAndPromoLogic(BaseEntity entity) {
        boolean hasLogic = ((String) entity.get("HAS_LOGIC")).equals("1");
        if (hasLogic) {
            Bundle localBundle = new Bundle();
            localBundle.putString("CAMPAIGN_NAME",
                    (String) entity.get("CAMPAIGN_NAME"));
            localBundle.putInt("CAMPAIGN_VIEW",
                    Integer.valueOf((String) entity.get("CAMPAIGN_VIEW")));
            localBundle
                    .putString("CAMPAIGN_EVENT", (String) entity.get("TYPE"));
            localBundle.putString("CAMPAIGN_IMGURL",
                    (String) entity.get("IMAGE_URL"));
            // openView(ViewName.CAMPAIGN_VIEW, localBundle);

            Intent mIntent = new Intent();
            localBundle.putBoolean("ISCAMPAIGN", true);
            mIntent.putExtras(localBundle);
            setResult(Activity.RESULT_OK, mIntent);
            // openView(ViewName.HOMEPAGE,localBundle);
        } else {
            String campaignView = (String) entity.get("CAMPAIGN_VIEW");
            if (campaignView.equalsIgnoreCase("sign-up-campaign")) {
                if (OAuthAuthorization.getInstance().isUserLogin()) {
                    openDrawer(true);
                } else {
                    openView(ViewName.SIGN_UP, null);
                }
            }
        }
        // this.finish();
    }

    /**
     * show connection error toast
     */

    @Override
    public void onConnectionError(Exception ex) {
        handler.sendEmptyMessage(SHOW_EXCEPTION);

    }

    /**
     * To close loading
     *
     * @param entity
     * @param position
     * @param params
     * @param flagCloseLoading
     */

    public void onAquireResult(BaseEntity entity, int position, Bundle params,
                               boolean flagToCloseLoading) {
        if (flagToCloseLoading)
            handler.sendEmptyMessage(LOADING_CLOSE);
    }

    enum ActionBarMethod {
        ALL, NO_SEARCH, HIDE_ALL, EDIT_HEADER, IS_SEARCHVIEW
    }

    /**
     * Enum for View Name
     *
     * @author TOO
     */
    public enum ViewName {
        HOMEPAGE, MAP, CHECKOUT, MERCHANT_VIEW, CREDIT, CART_VIEW, REDEEM_VIEW, SELECT_DEAL, CAMPAIGN_VIEW, TERM_CONDITION, SIGN_IN, SIGN_UP, RESET_DIALOG, EARN_RM2, CONTACT_US, PASSCODE, FINEPRINT, WISHLIST, VOUCHER, FILTER, CATEGORY_PAGE, DEAL_PAGE, THANKYOU_ONEBTN_DIALOG;
    }
}
