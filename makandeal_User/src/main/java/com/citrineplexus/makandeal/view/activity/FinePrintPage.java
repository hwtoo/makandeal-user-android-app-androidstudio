package com.citrineplexus.makandeal.view.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.webkit.WebView;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.util.GlobalService;

public class FinePrintPage extends AbstractActivity {

	private WebView mWebView = null;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setTitle("Fine Print");
		GoogleAnalyticsLib.getInstance(this).trackView("Universal Fine Print");
		setContentView(R.layout.activity_webview);
		mWebView = (WebView) findViewById(R.id.mWebView);
		mWebView.getSettings().setJavaScriptEnabled(true);
//		mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.loadUrl(GlobalService.getApiUrl()
				+ getResources().getString(R.string.UniversalFinePrintLink));

		// TextView fineprint = (TextView) findViewById(R.id.fineprintText);
		// String[] stringArray = getResources().getStringArray(
		// R.array.fineprint_ary);
		// fineprint.setText(GlobalService.convertToBulletText(Arrays
		// .asList(stringArray)));
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {

	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.NO_SEARCH;
	}

	@Override
	int drawerID() {
		return -1;
	}

	@Override
	void handlerMessage(Message msg) {

	}

}
