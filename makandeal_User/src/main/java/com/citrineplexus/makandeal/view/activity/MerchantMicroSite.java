package com.citrineplexus.makandeal.view.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ScrollView;
import android.widget.TextView;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.library.slidingTab.SlidingTabDividerLayout;
import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.MerchantMicroSiteAdapter;
import com.citrineplexus.makandeal.entity.BaseEntity;

import java.util.ArrayList;
import java.util.HashMap;
//import com.google.android.gms.internal.ms;

public class MerchantMicroSite extends AbstractActivity {
	private SlidingTabDividerLayout slidingTabLayout = null;
	private ViewPager viewPager = null;
	private MerchantMicroSiteAdapter pagerAdapter = null;
	private String vendorId = "", entityId = "", vendorName = "";
	private ImageView vendorImg = null;
	private TextView vendorNameTV = null;

	private ScrollView scrollView = null;

	private ArrayList<OnDataRequestListener> fragmentDataRequestListenerAry = new ArrayList<OnDataRequestListener>();
	private HashMap<String, BaseEntity> allDataHashMap = new HashMap<String, BaseEntity>();

	@SuppressLint("ResourceAsColor")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_merchant_site);
		if (getIntent() != null && getIntent().getExtras() != null) {
			Bundle bundle = getIntent().getExtras();
			vendorId = bundle.getString("VENDOR_ID");
			entityId = bundle.getString("ENTITY_ID");
		}
		scrollView = (ScrollView) findViewById(R.id.scrollViewId);

		slidingTabLayout = (SlidingTabDividerLayout) findViewById(R.id.slidingTabLayout);
		slidingTabLayout.setDistributeEvenly(true);
		slidingTabLayout.setCustomTabView(R.layout.custom_tab_layout,
				R.id.tabTitle);
		slidingTabLayout
				.setCustomTabColorizer(new SlidingTabDividerLayout.TabColorizer() {

					@Override
					public int getIndicatorColor(int position) {
						return getResources().getColor(R.color.colorPrimary);
					}
				});

		pagerAdapter = new MerchantMicroSiteAdapter(
				getSupportFragmentManager(), getResources(), vendorId, entityId);
		viewPager = (ViewPager) findViewById(R.id.viewPagerID);
		viewPager.setAdapter(pagerAdapter);

		slidingTabLayout.setViewPager(viewPager);

		vendorImg = (ImageView) findViewById(R.id.merchantSiteImg);
		vendorNameTV = (TextView) findViewById(R.id.merchantName);
	}

	// public ViewPager getViewPager(){
	// return this.viewPager;
	// }
	int viwePagerDefaultHight;

	public void setViewPagerSize(int height) {
		if (viwePagerDefaultHight < height)
			viwePagerDefaultHight = height;
		ViewGroup.LayoutParams params = viewPager.getLayoutParams();
		params.height = viwePagerDefaultHight;
		viewPager.setLayoutParams(params);
		viewPager.setPadding(0, 0, 0, 10);

		scrollView.smoothScrollTo(0, 0);
	}

	public void resetScrollView() {
		if (scrollView != null)
			scrollView.smoothScrollTo(0, 0);
	}

	public void getFragmentData(Bundle bundle, OnDataRequestListener listener) {
		if (!fragmentDataRequestListenerAry.contains(listener))
			fragmentDataRequestListenerAry.add(listener);
		String fragmentClass = bundle.getString("FragmentName");

		if (allDataHashMap.containsKey(fragmentClass)) {
			for (OnDataRequestListener listeners : fragmentDataRequestListenerAry)
				listeners.onAquireResult(allDataHashMap.get(fragmentClass), 0,
						bundle);
		} else {
			getDataFromServer(bundle, bundle.getInt("CONTENT_ID"),
					bundle.getBoolean("LOADING", false));
		}
	}

	public void getDataFromServer(Bundle param, int contentID,
			boolean loadingFlag) {
		getServiceData(contentID, 0, param, loadingFlag);
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		if (params.getBoolean("LOADING"))
			super.onAquireResult(entity, position, params, true);

		String fragmentName = params.getString("FragmentName");
		if (params.getInt("DOWNLOAD_STAGE") == 20) {
			vendorName = (String) entity.get("VENDOR_NAME");
			vendorNameTV.setText((String) entity.get("VENDOR_NAME"));
			String imageUrl = (String) entity.get("VENDOR_BANNER");
			if (imageUrl == null || imageUrl.equals(""))
				vendorImg.setVisibility(View.GONE);
			else {
				vendorImg.setVisibility(View.VISIBLE);
				ImageLoader.getInstance(this).DisplayImage(
						(String) entity.get("VENDOR_BANNER"), vendorImg,
						ScaleType.CENTER_CROP, false);
			}

		}

		if (!allDataHashMap.containsKey(fragmentName)) {
			allDataHashMap.put(fragmentName, entity);
		}
		BaseEntity en = new BaseEntity();
		en = en.merge(entity);
		for (OnDataRequestListener fragementListner : fragmentDataRequestListenerAry)
			fragementListner.onAquireResult(en, position, params);
	}

	@Override
	int setDrawerLockMode() {
		return 0;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return null;
	}

	@Override
	int drawerID() {
		return 0;
	}

	@Override
	void handlerMessage(Message msg) {

	}

	public void openMap(String address, String shopName) {
		Bundle param = new Bundle();
		param.putString("ADDRESS", address);
		param.putString("SHOPNAME", vendorName);
		openView(ViewName.MAP, param);
	}

}
