package com.citrineplexus.makandeal.view.fragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.ExpandableAdapter;
import com.citrineplexus.makandeal.callback.OnExpandableListRender;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.util.Calculator;
import com.citrineplexus.makandeal.util.GlobalService;
import com.citrineplexus.makandeal.view.activity.AbstractActivity.ViewName;
import com.citrineplexus.makandeal.view.activity.MerchantMicroSite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CategoriesFragment extends Fragment implements
		OnDataRequestListener, OnExpandableListRender, OnChildClickListener {

	private ExpandableListView expandableListView = null;
	private ExpandableAdapter adapter = null;
	private String vendorId = "";

	private List<String> listDataHeader = new ArrayList<String>();
	private HashMap<String, List<BaseEntity>> listDataChild = new HashMap<String, List<BaseEntity>>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			vendorId = getArguments().getString("VENDOR_ID");
		}
		listDataHeader.add("PROMOTION");
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser&& context!=null) {
			GoogleAnalyticsLib.getInstance(getActivity()).trackView(
					"Categories");
			((MerchantMicroSite) getActivity()).resetScrollView();

		}
	}

	private Context context;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(this.context==null)
			this.context = context;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_merchat_categories, null);
		expandableListView = (ExpandableListView) v
				.findViewById(R.id.expandListView);
		expandableListView.setDividerHeight(1);
		adapter = new ExpandableAdapter(this);
		adapter.set_listDataHeader(listDataHeader);
		adapter.set_listDataChild(listDataChild);
		expandableListView.setAdapter(adapter);
		expandableListView.setOnChildClickListener(this);
		loadData(1);
		return v;
	}

	private void loadData(int stage) {
		Bundle bundle = new Bundle();
		MerchantMicroSite activity = (MerchantMicroSite) getActivity();
		switch (stage) {
		case 1:// Load Merchant Detail
			bundle.putString("FragmentName", this.getClass().getName());
			bundle.putString("VENDOR_ID", vendorId);
			bundle.putInt("CONTENT_ID", 1030);
			break;

		default:
			break;
		}
		activity.getFragmentData(bundle, this);
	}

	public static Fragment newInstance(String vendorId) {
		CategoriesFragment categories = new CategoriesFragment();
		Bundle bundle = new Bundle();
		bundle.putString("VENDOR_ID", vendorId);
		categories.setArguments(bundle);
		return categories;
	}


	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		if (params.getInt("CONTENT_ID") == 1030
				&& ((BaseEntity) entity).get("JSONARRAY") != null) {
			ArrayList<BaseEntity> templist = (ArrayList<BaseEntity>) ((BaseEntity) entity)
					.get("JSONARRAY");
			listDataChild.put(listDataHeader.get(0), templist);

			int listViewHeight = GlobalService
					.setExpandableListViewHeightBasedOnChildren(expandableListView);
			((MerchantMicroSite) getActivity())
					.setViewPagerSize(listViewHeight);
			adapter.notifyDataSetChanged();
			expandableListView.expandGroup(0);
			// int listViewHeight = GlobalService
			// .setExpandableListViewHeightBasedOnChildren(expandListview)
			// + vendorName.getHeight()
			// + vendorAbout.getHeight()
			// + galleryViewPager.getHeight();
			//
			// ((MerchantMicroSite)
			// getActivity()).setViewPagerSize(listViewHeight);
		}
	}

	@Override
	public void onConnectionError(Exception ex) {

	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) adapter.getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) getActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.child_location_header,
					null);
		}
		LinearLayout backgroud = (LinearLayout) convertView
				.findViewById(R.id.headerBg);
		backgroud.setBackgroundColor(getResources().getColor(R.color.white));
		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.locationHeader);
		Drawable groupIndicator = isExpanded ? getResources().getDrawable(
				R.drawable.arrow_down) : getResources().getDrawable(
				R.drawable.arrow_up);
		lblListHeader.setCompoundDrawablesWithIntrinsicBounds(groupIndicator,
				null, null, null);
		lblListHeader.setText(headerTitle);
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		final BaseEntity entity = (BaseEntity) adapter.getChild(groupPosition,
				childPosition);
		MerchantCategoriesHandle handle = null;
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) getActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(
					R.layout.child_merchant_categories_item, null);
			handle = new MerchantCategoriesHandle();

			handle.mImageView = (ImageView) convertView
					.findViewById(R.id.productImage);
			handle.productTitle = (TextView) convertView
					.findViewById(R.id.productTitle);
			handle.productPrice = (TextView) convertView
					.findViewById(R.id.productPrice);
			handle.originalPrice = (TextView) convertView
					.findViewById(R.id.originalPrice);
			convertView.setTag(handle);
		} else
			handle = (MerchantCategoriesHandle) convertView.getTag();

		if (entity.get("IMAGE_URL") != null)
			ImageLoader.getInstance(getActivity()).DisplayImage(
					(String) entity.get("IMAGE_URL"), handle.mImageView,
					ScaleType.CENTER_CROP, false);

		if (entity.get("NAME") != null)
			handle.productTitle.setText((String) entity.get("NAME"));

		double price = Double.parseDouble(entity.get("PRICE", "0.00")
				.toString());
		if (GlobalService.checkDiscout(entity)) {
			double specialPrice = Double.parseDouble(entity.get(
					"SPECIAL_PRICE", "0.00").toString());
			handle.productPrice.setText("RM" + Calculator.round(specialPrice));
			handle.originalPrice.setText("RM" + Calculator.round(price));
			handle.originalPrice.setVisibility(View.VISIBLE);
		} else
			handle.productPrice.setText("RM" + Calculator.round(price));

		return convertView;
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		BaseEntity entity = (BaseEntity) adapter.getChild(groupPosition,
				childPosition);
		if (entity.get("ENTITY_ID") != null) {
			Bundle param = new Bundle();
			param.putString("ENTITY_ID", entity.get("ENTITY_ID").toString());
			param.putString("VENDOR_ID", entity.get("UDROPSHIP_VENDOR")
					.toString());
			((MerchantMicroSite) getActivity()).openView(ViewName.DEAL_PAGE,
					param);
		}
		return true;
	}

	private class MerchantCategoriesHandle {
		ImageView mImageView;
		TextView productPrice, originalPrice, productTitle;
	}
}
