package com.citrineplexus.makandeal.view.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.AdapterBinder;
import com.citrineplexus.makandeal.adapter.ListAdapter;
import com.citrineplexus.makandeal.entity.BaseEntity;

public abstract class DefaultRecyclerListView extends AbstractActivity
		implements OnRefreshListener {

	private ListAdapter listAdapter = null;
	private int CONTENT_ID = 0;
	// private LinearLayout emptyView = null;

	protected SwipeRefreshLayout swipeLayout = null;
	protected RecyclerView recyclerView = null;
	protected boolean requestingNewData = false;
	protected LinearLayoutManager layoutManager = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getContentView());
		swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeLayout.setOnRefreshListener(this);
		recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
		layoutManager = new LinearLayoutManager(this);
		recyclerView.setHasFixedSize(true);
		recyclerView.setLayoutManager(layoutManager);
		listAdapter = initListAdapter();
		listAdapter.setBinder(getBinder());
		listAdapter.setDataSet(new ArrayList<BaseEntity>());
		// listAdapter.setDataSet(MakanDealApplication.getInstance()
		// .getDealArrayList());
		recyclerView.setAdapter(listAdapter);

		onInit();
		CONTENT_ID = getContentId();
		getSupportActionBar().setTitle(getPageTitle());
		if (CONTENT_ID != -1)
			getServiceData(CONTENT_ID);
	}

	public ListAdapter getListAdapter() {
		return listAdapter;
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params,
			boolean flagCloseLoading) {
		super.onAquireResult(entity, position, params, flagCloseLoading);
		if (params.getString("REQUEST_TYPE") != null) {
			Message msg = new Message();
			msg.obj = entity.get("JSONARRAY");
			if (params.getString("REQUEST_TYPE").equals("INITIAL")) {
				msg.what = 2001;
			} else if (params.getString("REQUEST_TYPE").equals("ADDITIONAL")) {
				msg.what = 2002;
			} else if (params.getString("REQUEST_TYPE").equals("REMOVE")) {
				msg.what = 2003;
			}
			handler.sendMessage(msg);
		}

		if (swipeLayout.isRefreshing())
			swipeLayout.setRefreshing(false);
	}

	@Override
	void handlerMessage(Message msg) {
		switch (msg.what) {
		case 2001:
			listAdapter.clearData();
		case 2002:
			if (msg.obj instanceof BaseEntity) {
				if (((BaseEntity) msg.obj).get("JSONARRAY") != null) {
					for (BaseEntity en : (ArrayList<BaseEntity>) ((BaseEntity) msg.obj)
							.get("JSONARRAY")) {
						listAdapter.addData(en);
					}
				}
			} else if (msg.obj instanceof ArrayList<?>)
				listAdapter.addData((ArrayList<BaseEntity>) msg.obj,
						getUniqueKey());
			listAdapter.notifyDataSetChanged();

			// if (listAdapter.isEmpty()) {
			// showEmptyListBtn();
			// recyclerView.setEmptyView(emptyView);
			// }
		case 2003:
			adapterEmptyHandler(listAdapter.isEmpty(), recyclerView);
			break;

		}

		// if (listView.getMode().equals(Mode.PULL_FROM_START)
		// && listView.isRefreshing())
		// listView.onRefreshComplete();

		requestingNewData = false;
	}

	abstract int getContentView();

	abstract ListAdapter initListAdapter();

	abstract String getPageTitle();

	abstract AdapterBinder getBinder();

	abstract int getContentId();

	abstract void onInit();

	abstract void getServiceData(int contentID);

	abstract String getUniqueKey();

	abstract void adapterEmptyHandler(boolean adapterIsEmpty,
			RecyclerView recyclerView);

}
