package com.citrineplexus.makandeal.view.activity;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.AbstractRecyclerAdapter.OnListAdapterItemClickListener;
import com.citrineplexus.makandeal.adapter.AdapterBinder;
import com.citrineplexus.makandeal.adapter.ListAdapter;
import com.citrineplexus.makandeal.binder.CreditBinder;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;

public class CreditPage extends DefaultRecyclerListView implements
		OnListAdapterItemClickListener, OnClickListener {
	private RelativeLayout creditPageEmptyView = null;
	private final int CONTENT_ID = 1013;
	private final String title = "My Credit";

	@Override
	public void onRefresh() {
		getData(CONTENT_ID, false);
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		super.onAquireResult(entity, position, params,
				params.getBoolean("LOADING", false));

	}

	@Override
	int getContentView() {
		return R.layout.activity_credit_page;
	}

	@Override
	ListAdapter initListAdapter() {
		return new ListAdapter(this);
	}

	@Override
	String getPageTitle() {
		if (!fromCartView) {
			return title;
		} else {
			return "Select Coupon Code";
		}
	}

	@Override
	AdapterBinder getBinder() {
		return new CreditBinder(this);
	}

	@Override
	int getContentId() {
		return CONTENT_ID;
	}

	@Override
	void onInit() {
		creditPageEmptyView = (RelativeLayout) findViewById(R.id.creditPageEmptyView);
		findViewById(R.id.buyNowBtn).setOnClickListener(this);
		GoogleAnalyticsLib.getInstance(this).trackView(title);
	}

	@Override
	void getServiceData(int contentID) {
		getData(contentID, true);
	}

	private void getData(int contentID, boolean loadingFlag) {
		Bundle localBundle = new Bundle();
		localBundle.putString("REQUEST_TYPE", "INITIAL");
		localBundle.putBoolean("LOADING", loadingFlag);
		getServiceData(contentID, 0, localBundle, loadingFlag);
	}

	@Override
	String getUniqueKey() {
		return "CODE";
	}

	@Override
	void adapterEmptyHandler(boolean adapterIsEmpty, RecyclerView recyclerView) {
		if (adapterIsEmpty) {
			recyclerView.setVisibility(View.GONE);
			creditPageEmptyView.setVisibility(View.VISIBLE);
		}
	}

	@Override
	int setDrawerLockMode() {
		if (fromCartView)
			return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
		return DrawerLayout.LOCK_MODE_LOCKED_OPEN;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		if (fromCartView)
			return ActionBarMethod.HIDE_ALL;
		return ActionBarMethod.ALL;
	}

	@Override
	int drawerID() {
		return 106;
	}

	@Override
	public void onItemClickListener(View view, int position) {

	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.buyNowBtn) {

		}
	}

}
