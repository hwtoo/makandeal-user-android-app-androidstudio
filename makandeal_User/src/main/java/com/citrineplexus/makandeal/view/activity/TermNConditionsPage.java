package com.citrineplexus.makandeal.view.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.util.GlobalService;

@SuppressLint("SetJavaScriptEnabled")
public class TermNConditionsPage extends AbstractActivity {

	// private TextView title = null, message = null;
	private int titleText = -1, pageId = -1, msg = -1;

	private WebView mWebView = null;

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_webview);
		mWebView = (WebView) findViewById(R.id.mWebView);
		mWebView.getSettings().setJavaScriptEnabled(true);
		// title = (TextView) findViewById(R.id.TermAndConditionTitle);
		// message = (TextView) findViewById(R.id.TermAndContidtionMsg);
		setData(getIntent());
		
		mWebView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				return false;
			}
		});
	}

	@Override
	public void onBackPressed() {
		if (mWebView.canGoBack()) {
			mWebView.goBack();
		} else {
			super.onBackPressed();
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setMenuChecked(pageId);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setData(intent);
		setMenuChecked(pageId);
	}

	private void setData(Intent intent) {
		if (intent != null && intent.getExtras() != null
				&& intent.getExtras().get("TITLE_ID") != null
				&& intent.getExtras().get("MSG_ID") != null
				&& intent.getExtras().get("PAGE_ID") != null) {
			titleText = intent.getExtras().getInt("TITLE_ID");
			msg = intent.getExtras().getInt("MSG_ID");
			pageId = intent.getExtras().getInt("PAGE_ID");
		}
		handler.sendEmptyMessageDelayed(100, 300);
	}

	@Override
	void handlerMessage(Message msg) {
		if (msg.what == 100) {
			String title = getResources().getString(titleText);
			getSupportActionBar().setTitle(title);
			GoogleAnalyticsLib.getInstance(this).trackView(title);

			if (pageId == 116) {
				mWebView.loadUrl(getResources().getString(this.msg));
			} else {
				mWebView.loadUrl(GlobalService.getWebViewURL()
						+ getResources().getString(this.msg));
				mWebView.setPadding(10, 10, 10, 10);
			}
		}
	}

	
	
	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {

	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.HIDE_ALL;
	}

	@Override
	int drawerID() {
		return pageId;
	}

}
