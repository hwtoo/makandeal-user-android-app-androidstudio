package com.citrineplexus.makandeal.view.activity;

import java.util.ArrayList;
import java.util.Set;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.citrineplexus.library.gcm.GCMRegisterHandler;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.callback.DialogCallback;
import com.citrineplexus.makandeal.callback.OAuthListener;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.service.OAuthAuthorization;
import com.citrineplexus.makandeal.util.GlobalService;
import com.citrineplexus.makandeal.view.MakanDealApplication;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

public class SignInPage extends AbstractActivity implements OnClickListener,
/* StatusCallback, */OAuthListener, DialogCallback {

	private EditText userEmailEditText = null, userPassEditText = null;
	private final String title = "Sign In";

	// TODO: new Facebook SDK
	private CallbackManager callbackManager;

	// private UiLifecycleHelper uiHelper = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signin_page);
		getSupportActionBar().setTitle(title);
		GoogleAnalyticsLib.getInstance(this).trackView(title);

		findViewById(R.id.signInBtn).setOnClickListener(this);
		findViewById(R.id.signUpBtn).setOnClickListener(this);
		findViewById(R.id.forgetPasswordTextView).setOnClickListener(this);

		userEmailEditText = (EditText) findViewById(R.id.emailEditText);
		userPassEditText = (EditText) findViewById(R.id.passwordEditText);

		// uiHelper = new UiLifecycleHelper(this, this);
		// uiHelper.onCreate(savedInstanceState);
		// TODO: new Facebook SDK
		LoginButton fbLogin = (LoginButton) findViewById(R.id.fbLoginButton);
		fbLogin.setReadPermissions("email");
		callbackManager = CallbackManager.Factory.create();
		fbLogin.registerCallback(callbackManager,
				new FacebookCallback<LoginResult>() {
					@Override
					public void onSuccess(LoginResult loginResult) {
						if (!loadingOpen) {
							handler.sendEmptyMessage(LOADING_OPEN);
							GoogleAnalyticsLib.getInstance(SignInPage.this)
									.trackEvent("Sign In",
											"Sign In with Facebook");
							OAuthAuthorization.getInstance().loginWithFacebook(
									SignInPage.this,
									SignInPage.this,
									loginResult.getAccessToken().getToken(),
									loginResult.getAccessToken().getExpires()
											.toString());
						}
					}

					@Override
					public void onCancel() {
					}

					@Override
					public void onError(FacebookException exception) {
					}
				});
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// TODO: Facebook SDK
		callbackManager.onActivityResult(requestCode, resultCode, data);
		// uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.signInBtn:
			String userEmail = userEmailEditText.getText().toString();
			String userPassword = userPassEditText.getText().toString();
			if (!GlobalService.isEmailValid(userEmail)) {
				Toast.makeText(this, "Please enter a valid email address.",
						Toast.LENGTH_SHORT).show();
				return;
			}
			if (userPassword == null || userPassword.equals("")
					|| userPassword.length() <= 0) {
				Toast.makeText(this, "Please enter your password.",
						Toast.LENGTH_SHORT).show();
				return;
			}
			if (!MakanDealApplication.getInstance().isUserLogin()) {
				if (!loadingOpen) {
					handler.sendEmptyMessage(LOADING_OPEN);
					GoogleAnalyticsLib.getInstance(this).trackEvent("Sign In",
							null);
					OAuthAuthorization.getInstance().Login(this, this,
							userEmail, userPassword);
				}
			}
			break;

		case R.id.signUpBtn:
			openView(ViewName.SIGN_UP, null);
			break;

		case R.id.forgetPasswordTextView:
			openDialog(ViewName.RESET_DIALOG, this, null);
			break;

		default:
			break;
		}
	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
	}

	@Override
	void handlerMessage(Message msg) {
	}

	// TODO: Facebook SDK
	// @Override
	// public void call(Session session, SessionState state, Exception
	// exception) {
	// if (exception != null)
	// exception.printStackTrace();
	// if (session.isOpened()) {
	// // accessToken = session.getAccessToken();
	// GoogleAnalyticsLib.getInstance(this).trackEvent("Sign In",
	// "Sign In with Facebook");
	// OAuthAuthorization.getInstance().loginWithFacebook(this, this,
	// session.getAccessToken(),
	// session.getExpirationDate().toString());
	// }
	// }

	@Override
	public void returnMessage(int id, Object obj) {
		if (id == -9999 && obj instanceof BaseEntity) {
			handler.sendEmptyMessage(LOADING_CLOSE);
			BaseEntity msgEntity = (BaseEntity) obj;
			if (msgEntity.get("ERROR") != null) {
				ArrayList<BaseEntity> errorAry = (ArrayList<BaseEntity>) msgEntity
						.get("ERROR");
				BaseEntity errorEntity = (BaseEntity) errorAry.get(0);
				if (errorEntity.get("MESSAGE") != null) {
					Toast.makeText(this, errorEntity.get("MESSAGE").toString(),
							Toast.LENGTH_SHORT).show();

					// TODO: Facebook SDK
					LoginManager.getInstance().logOut();
				}
			}
		}
	}

	@Override
	public void oauthReady(boolean status, Bundle extra) {
	}

	@Override
	public void onAcquireProfile() {
		handler.sendEmptyMessage(LOADING_CLOSE);
		try {
			// Register/update user profile to Notification Database
			new GCMRegisterHandler().registerApp(this);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		// Bundle extra = new Bundle();
		// extra.putBoolean("CHECK_CART_NUM", true);
		// getServiceData(1042, 0, extra, false);
		// new ServiceGateway().mergeCart(this,
		// PersistentData.getInstance(),
		// extra);

		setResult(RESULT_OK, new Intent());
		finish();
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {

	}

	@Override
	public void onDialogClick(int id, ViewName whichDialog, Object result) {
		if (id == DialogInterface.BUTTON_POSITIVE)
			if (result instanceof String) {
				String resetEmail = (String) result;
				if (GlobalService.isEmailValid(resetEmail)) {
					try {
						Bundle params = new Bundle();
						params.putBoolean("SIGN_ADMIN", true);
						params.putString("EMAIL", (String) result);
						getServiceData(1038, 0, params, false);
						openDialog(
								ViewName.THANKYOU_ONEBTN_DIALOG,
								this,
								new String[] {
										getResources().getString(
												R.string.resetPasswordMsg),
										"COMPLETE" });
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else
					Toast.makeText(this, "Please enter a valid email address.",
							Toast.LENGTH_SHORT).show();
			}

	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.ALL;
	}

	@Override
	int drawerID() {
		return 100;
	}

}
