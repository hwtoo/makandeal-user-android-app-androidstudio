package com.citrineplexus.makandeal.view.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.ExpandableAdapter;
import com.citrineplexus.makandeal.callback.OnExpandableListRender;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.view.MakanDealApplication;

public class FilterPage extends AbstractActivity implements
		OnExpandableListRender, OnChildClickListener {

	private ExpandableListView expandListview = null;
	private ExpandableAdapter adapter = null;
	private TextView headerText = null;

	private List<String> listDataHeader = new ArrayList<String>();
	private HashMap<String, List<BaseEntity>> listDataChild = new HashMap<String, List<BaseEntity>>();
	private HashMap<String, List<BaseEntity>> tempHashMap = new HashMap<String, List<BaseEntity>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_filter_page);
		expandListview = (ExpandableListView) findViewById(R.id.expandListView);
		expandListview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

		listDataHeader.add("Price");
		listDataHeader.add("Location");

		adapter = new ExpandableAdapter(this);
		expandListview.setOnChildClickListener(this);
		adapter.set_listDataChild(listDataChild);
		adapter.set_listDataHeader(listDataHeader);

		expandListview.setAdapter(adapter);

		prepareListData();
		expandListview.expandGroup(0);
		expandListview.expandGroup(1);

	}

	private void prepareListData() {

		List<BaseEntity> priceChildData = new ArrayList<>();
		List<BaseEntity> regionChildData = new ArrayList<>();
		for (BaseEntity en : MakanDealApplication.getInstance()
				.getFilterPriceList())
			if ((boolean) en.get("ISSHOWING", false))
				priceChildData.add(en);
		listDataChild.put(listDataHeader.get(0), priceChildData); // Header,
																	// Child
																	// data

		for (BaseEntity en : MakanDealApplication.getInstance()
				.getFilterRegionList())
			if (!(boolean) en.get("ISHIDE", false))
				regionChildData.add(en);
		listDataChild.put(listDataHeader.get(1), regionChildData);
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {

	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.HIDE_ALL;
	}

	@Override
	int drawerID() {
		return -1;
	}

	@Override
	void handlerMessage(Message msg) {

	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		View v = LayoutInflater.from(this).inflate(
				R.layout.child_filter_header, null);
		v.setFocusable(false);
		TextView header = (TextView) v.findViewById(R.id.groupHeader);
		header.setText(listDataHeader.get(groupPosition));
		Drawable groupIndicator = isExpanded ? getResources().getDrawable(
				R.drawable.arrow_down) : getResources().getDrawable(
				R.drawable.icon_arrow_red);
		header.setCompoundDrawablesWithIntrinsicBounds(null, null,
				groupIndicator, null);

		return v;

	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		final BaseEntity entity = (BaseEntity) adapter.getChild(groupPosition,
				childPosition);
		FilterRowClassHolder holder;
		if (convertView == null) {
			holder = new FilterRowClassHolder();
			convertView = LayoutInflater.from(this).inflate(
					R.layout.child_filter_row, null);
			holder.rowLayout = (LinearLayout) convertView
					.findViewById(R.id.filterRowId);
			holder.title = (TextView) convertView.findViewById(R.id.rowTitle);
			holder.checkbox = (CheckBox) convertView
					.findViewById(R.id.checkbox);
			convertView.setTag(holder);
		} else
			holder = (FilterRowClassHolder) convertView.getTag();

		holder.title.setText((String) entity.get("TEXT", ""));
		holder.checkbox.setChecked(entity.getCheckBoxFlag());
		holder.checkbox.setFocusable(false);
		convertView.setFocusable(false);
		return convertView;
	}

	private class FilterRowClassHolder {
		TextView title;
		CheckBox checkbox;
		LinearLayout rowLayout;
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		final BaseEntity entity = (BaseEntity) adapter.getChild(groupPosition,
				childPosition);
		entity.couponCheckBoxFlag();
		adapter.notifyDataSetChanged();

		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				Bundle localBundle = new Bundle();
				localBundle.putBoolean("FROM_FILTER", true);
				Intent mIntent = new Intent();
				mIntent.putExtras(localBundle);
				setResult(Activity.RESULT_OK, mIntent);
				finish();
			}
		}, 100);

		return true;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

}
