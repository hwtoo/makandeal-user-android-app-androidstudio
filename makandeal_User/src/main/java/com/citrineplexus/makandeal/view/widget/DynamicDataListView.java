package com.citrineplexus.makandeal.view.widget;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Filter.FilterListener;
import android.widget.Filterable;
import android.widget.Gallery;
import android.widget.ListView;

import com.citrineplexus.bello2.AdapterBaseBinder;
import com.citrineplexus.bello2.BaseEntityAdpater;
import com.citrineplexus.bello2.FilterAdapter;
import com.citrineplexus.library.gallery.GalleryLooping;
import com.citrineplexus.library.slideshow.SlideShowViewPager;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.callback.OnDynamicListViewRefreshListener;
import com.citrineplexus.makandeal.callback.OnDynamicListViewRefreshListener.LoadResponseFlag;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.util.GlobalService;
import com.citrineplexus.makandeal.view.MakanDealApplication;

public class DynamicDataListView implements OnScrollListener {

	private BaseEntityAdpater adapter = null;
	private ListView listView = null;
	private View footer = null;
	private OnDynamicListViewRefreshListener listener = null;
	private OnItemClickListener clickListener = null;
	private SwipeRefreshLayout swipeLayout = null;
	private GalleryLooping headerGallery = null;

	private boolean requestionData = false;
	private int filter = -1002;

	public void setFilter(int filter) {
		this.filter = filter;
	}

	// TODO: backup for stage 1
	public DynamicDataListView(Context context,
			OnDynamicListViewRefreshListener listener,
			ListView pullToRefresshListView, SwipeRefreshLayout swipeLayout,
			BaseEntityAdpater adapter, AdapterBaseBinder binder,
			OnItemClickListener clickListener, int filter) {
		this.listView = pullToRefresshListView;
		this.adapter = adapter;
		this.listener = listener;
		this.clickListener = clickListener;
		this.filter = filter;
		this.swipeLayout = swipeLayout;
		this.swipeLayout.setOnRefreshListener(this.listener);
		adapter.setBinder(binder);
		initLoadingFooter(context);

		// if (screenWidth != -1) {
		// View v = LayoutInflater.from(context).inflate(
		// R.layout.header_product_listing, null);
		// headerGallery = (GalleryLooping) v
		// .findViewById(R.id.galleryViewPager);
		// float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
		// 130, context.getResources().getDisplayMetrics());
		// AbsListView.LayoutParams headerViewParams = new
		// AbsListView.LayoutParams(
		// screenWidth, (int) px);
		// headerGallery.setLayoutParams(headerViewParams);
		// listView.addHeaderView(v, null, true);
		// }
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(clickListener);
	}

	public DynamicDataListView(Context context,
			OnDynamicListViewRefreshListener listener,
			ListView pullToRefresshListView, SwipeRefreshLayout swipeLayout,
			BaseEntityAdpater adapter, AdapterBaseBinder binder,
			OnItemClickListener clickListener, int filter, int screenWidth) {
		this.listView = pullToRefresshListView;
		this.adapter = adapter;
		this.listener = listener;
		this.clickListener = clickListener;
		this.filter = filter;
		this.swipeLayout = swipeLayout;
		this.swipeLayout.setOnRefreshListener(this.listener);
		adapter.setBinder(binder);
		initLoadingFooter(context);

		if (screenWidth != -1) {
			View v = LayoutInflater.from(context).inflate(
					R.layout.header_product_listing, null);
			headerGallery = (GalleryLooping) v
					.findViewById(R.id.galleryViewPager);
			float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
					130, context.getResources().getDisplayMetrics());
			AbsListView.LayoutParams headerViewParams = new AbsListView.LayoutParams(
					screenWidth, (int) px);
			headerGallery.setLayoutParams(headerViewParams);
			listView.addHeaderView(v, null, true);
		}
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(clickListener);
	}

	public GalleryLooping getHeader() {
		return headerGallery;
	}

	private void initLoadingFooter(Context context) {
		LayoutInflater layoutInflater = LayoutInflater.from(context);
		footer = layoutInflater.inflate(R.layout.listview_loading_footer, null);
		listView.addFooterView(footer);
	}

	public ListView getListView() {
		return this.listView;
	}

	public void setSwipeEnable(boolean flag) {
		swipeLayout.setEnabled(flag);
	}

	public void resetFeatureCount() {
		if (adapter instanceof FilterAdapter)
			((FilterAdapter) adapter).resetFeatureCount();
	}

	public boolean isEmpty() {
		return adapter.isEmpty();
	}

	public void initScorllLIstener() {
		listView.setOnScrollListener(this);
	}

	public int getCount() {
		if (adapter instanceof FilterAdapter) {
			return ((FilterAdapter) adapter).getEntityDataCount();
		} else
			return adapter.getCount();
	}

	public void clearData() {
		if (adapter.getCount() > 0)
			adapter.clearData();
	}

	public void addFooter() {
		// if (listView.getFooterViewsCount() > 0)
		// removeFooter();
		// listView.addFooterView(footer);
		footer.setVisibility(View.VISIBLE);
	}

	public void removeFooter() {
		footer.setVisibility(View.GONE);
	}

	public void addData(BaseEntity entity,boolean isCampignDeal) {
		adapter.addData(entity);
		if(!isCampignDeal){
			GlobalService.setPriceCategory(entity);
			MakanDealApplication.getInstance().addDeal(entity);
		}
			
	}

	public void addData(ArrayList<BaseEntity> entities, String uniqueKey,boolean isCampignDeal) {
		adapter.addData(entities, uniqueKey);
		if(!isCampignDeal){
			MakanDealApplication.getInstance().addDeal(entities);}
	}

	public void notifyDataSetChanged() {
		if (swipeLayout.isRefreshing())
			swipeLayout.setRefreshing(false);
		adapter.notifyDataSetChanged();
		filter(filter, false);
	}
	

	public void setRequestData(boolean flag) {
		requestionData = flag;
	}

	public boolean isRequestingData() {
		return this.requestionData;
	}

	public void setVisible(int visibility) {
		listView.setVisibility(visibility);
	}

	public BaseEntityAdpater getAdatper() {
		return this.adapter;
	}

	public void filter(int filter, final boolean scrollToTop) {
		// if (this.filter == filter)
		// return;
		this.filter = filter;
		if (adapter instanceof Filterable)
			((Filterable) adapter).getFilter().filter(String.valueOf(filter),
					new FilterListener() {

						@Override
						public void onFilterComplete(int count) {
							setRequestData(false);
							if (count > 0) {
								if (!swipeLayout.isEnabled()) {
									setSwipeEnable(true);
									addFooter();
								}

								if (listener != null) {
									listener.onLoadResponse(LoadResponseFlag.HAVE_CONTENT);
								}
								if (scrollToTop)
									listView.post(new Runnable() {
										@Override
										public void run() {
											// progressViewlinearLayout.requestFocusFromTouch();
											listView.setSelection(0);
										}
									});
							}
						}
					});
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		final int lastItem = firstVisibleItem + visibleItemCount;
		if (lastItem == totalItemCount) {
			if (!requestionData)
				loadMoreData();
			// }
		}
	}

	private void loadMoreData() {
		if (listener != null)
			listener.onLoadMore();
	}

	// @Override
	// public void onItemClick(AdapterView<?> parent, View view, int position,
	// long id) {
	// adapter.getBinder().onItemClick(position - 1,
	// (BaseEntity) adapter.getItem(position - 1), null);
	// }
}
