package com.citrineplexus.makandeal.view.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.callback.DialogCallback;
import com.citrineplexus.makandeal.util.GlobalService;
import com.citrineplexus.makandeal.view.activity.AbstractActivity.ViewName;

public class ResetPasswordDialog implements OnClickListener,
		android.view.View.OnClickListener {

	private Context context = null;
	private EditText mEditText = null;
	private AlertDialog alertDialog = null;
	private DialogCallback listener = null;
	private ViewName dialogName = null;

	public ResetPasswordDialog(Context context) {
		this.context = context;
	}

	public void init(ViewName dialogName, DialogCallback listener) {
		this.listener = listener;
		this.dialogName = dialogName;
		AlertDialog.Builder mbuidler = new AlertDialog.Builder(context,
				AlertDialog.THEME_HOLO_DARK);
		mbuidler.setTitle("Reset Password");
		View convertView = LayoutInflater.from(context).inflate(
				R.layout.dialog_reset_password, null);
		mEditText = (EditText) convertView
				.findViewById(R.id.resetPasswordEditText);
		mbuidler.setPositiveButton("OK", null);
		mbuidler.setNegativeButton("Cancel", null);
		mbuidler.setView(convertView);
		alertDialog = mbuidler.create();
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.setCancelable(false);
		// alertDialog.setOnShowListener(new OnShowListener() {
		//
		// @Override
		// public void onShow(DialogInterface dialog) {
		// AlertDialog alertDialog = (AlertDialog) dialog;
		// Button button = alertDialog
		// .getButton(DialogInterface.BUTTON_POSITIVE);
		// button.setTypeface(ViewController.getInstance(null)
		// .getTypeface(ViewController.ARIAL));
		//
		// button = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
		// button.setTypeface(ViewController.getInstance(null)
		// .getTypeface(ViewController.ARIAL));
		//
		// }
		// });
		alertDialog.show();

		alertDialog.getButton(DialogInterface.BUTTON_POSITIVE)
				.setOnClickListener(this);
		alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE)
				.setOnClickListener(this);

		int textViewId = alertDialog.getContext().getResources()
				.getIdentifier("android:id/alertTitle", null, null);
		if (textViewId != 0) {
			TextView tv = (TextView) alertDialog.findViewById(textViewId);
			tv.setTextColor(Color.WHITE);
		}

		int dividerId = alertDialog.getContext().getResources()
				.getIdentifier("android:id/titleDivider", null, null);
		if (dividerId != 0) {
			View divider = alertDialog.findViewById(dividerId);
			divider.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
					2));
			divider.setBackgroundColor(context.getResources().getColor(
					R.color.grey_03));
		}
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		if (mEditText.getText() != null
				&& mEditText.getText().toString().length() > 0
				&& !mEditText.getText().toString().equals("")) {
			if (GlobalService.isEmailValid(mEditText.getText().toString())) {
				if (listener != null)
					listener.onDialogClick(which, dialogName, mEditText
							.getText().toString());
				alertDialog.dismiss();
			} else
				Toast.makeText(context, "Please enter a valid email address.",
						Toast.LENGTH_SHORT).show();
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case android.R.id.button1:// Button Position
			if (mEditText.getText() != null
					&& mEditText.getText().toString().length() > 0
					&& !mEditText.getText().toString().equals("")) {
				if (GlobalService.isEmailValid(mEditText.getText().toString())) {
					if (listener != null)
						listener.onDialogClick(-1, dialogName, mEditText
								.getText().toString());
					alertDialog.dismiss();
				} else
					Toast.makeText(context,
							"Please enter a valid email address.",
							Toast.LENGTH_SHORT).show();
			}
			break;

		case android.R.id.button2:// Button Negatif
			alertDialog.dismiss();
			break;

		default:
			break;
		}

	}

}
