package com.citrineplexus.makandeal.view.fragment;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citrineplexus.library.gallery.GalleryLooping;
import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.util.Calculator;
import com.citrineplexus.makandeal.util.GlobalService;
import com.citrineplexus.makandeal.view.activity.DealPage;

import java.util.ArrayList;

public class AboutFragment extends Fragment implements OnDataRequestListener,
        OnClickListener {

    private String entityID = "";
    private TextView productTitle = null, productDescription = null,
            currentPrice = null, discount = null, originalPrice = null,
            totalSold = null;

    private ImageView addWishlistBtn = null;
    // TODO: Stage 2
    // private Button merchantStoreBtn = null;
    private LinearLayout merchantStoreBtn = null;
    private BaseEntity entity = null;

    private LinearLayout finePrintLayout = null, hightlightLayout = null,
            packageIncludeLayout = null;
    private TextView finePrintText = null, hightlightText = null,
            finePrintLink = null, packageInclude = null;

    private GalleryLooping gallery = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null
                && getArguments().getString("ENTITY_ID") != null)
            entityID = getArguments().getString("ENTITY_ID");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        System.out.println("%%%%% setUserVisibleHint context = " + getActivity());
        if (isVisibleToUser && context != null)
            GoogleAnalyticsLib.getInstance(getActivity()).trackView("About");
    }

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (this.context == null)
            this.context = context;
    }

    private void initDetail(int downloadState, int contentId) {
        boolean loadingFlag = false;
        Bundle bundle = new Bundle();
        bundle.putString("FragmentName", this.getClass().getName() + "|"
                + downloadState);
        bundle.putString("ENTITY_ID", entityID);
        bundle.putInt("CONTENT_ID", contentId);
        bundle.putInt("DOWNLOAD_STAGE", downloadState);
        if (downloadState == 1)
            loadingFlag = true;
        bundle.putBoolean("LOADING", loadingFlag);
        // ((DealPage) getActivity()).getDataFromServer(bundle, contentId,
        // loadingFlag);
        ((DealPage) getActivity()).getFragmentData(bundle, this);
    }

    // private void getPackageInclude() {
    // Bundle bundle = new Bundle();
    // bundle.putString("FragmentName", "AboutFragment|2");
    // bundle.putString("ENTITY_ID", entityID);
    // bundle.putInt("CONTENT_ID", 1031);1031
    // bundle.putInt("DOWNLOAD_STAGE", 2);
    // ((DealPage) getActivity()).getFragmentData(bundle, this);
    // }

    private synchronized void parseEntityObject(BaseEntity entity, int stage) {
        if (entity == null)
            return;
        switch (stage) {
            case 1:// BASIC INFO
                parseEntityStage1(entity);
                return;

            case 2:// Product Description
                if (entity.get("DESCRIPTION") != null) {
                    productDescription.setText(entity.get("DESCRIPTION", "")
                            .toString().trim());
                    productDescription.setVisibility(View.VISIBLE);
                }

                if (entity.get("BENEFITS") != null
                        && entity.get("BENEFITS") instanceof ArrayList<?>) {
                    ArrayList<String> benefitsAry = (ArrayList<String>) entity
                            .get("BENEFITS");
                    if (!benefitsAry.isEmpty()) {
                        hightlightText.setText(GlobalService
                                .convertToBulletText(benefitsAry));
                        hightlightLayout.setVisibility(View.VISIBLE);
                    }
                }

                return;

            case 3:// fine print
                if (entity.get("JSONARRAY") != null)
                    if (entity.get("JSONARRAY") instanceof ArrayList<?>) {
                        for (BaseEntity en : (ArrayList<BaseEntity>) entity
                                .get("JSONARRAY")) {

                            ArrayList<String> detailAry = (ArrayList<String>) en
                                    .get("DETAIL");
                            if (detailAry != null && !detailAry.isEmpty()) {
                                finePrintText.setText(GlobalService
                                        .convertToBulletText(detailAry));
                                finePrintLayout.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                break;

            case 4:// Product Image
                if (entity.get("JSONARRAY") != null)
                    if (entity.get("JSONARRAY") instanceof ArrayList<?>) {
                        gallery.cleanUp();
                        for (BaseEntity en : (ArrayList<BaseEntity>) entity
                                .get("JSONARRAY")) {
                            // gallery.addImage((String) en.get("IMAGE_URL"));
                            gallery.addData((String) en.get("IMAGE_URL"));
                        }
                        gallery.notifyDataChanged();
                    }
                break;

            case 5:// Package Included
                if (entity.get("PACKAGE_INCLUDES") != null
                        && entity.get("PACKAGE_INCLUDES") instanceof ArrayList<?>) {
                    ArrayList<String> benefitsAry = (ArrayList<String>) entity
                            .get("PACKAGE_INCLUDES");
                    if (!benefitsAry.isEmpty()) {
                        packageInclude.setText(GlobalService
                                .convertToBulletText(benefitsAry));
                        packageIncludeLayout.setVisibility(View.VISIBLE);
                    }
                }
                break;
        }
    }

    private void parseEntityStage1(BaseEntity entity) {
        if (this.entity == null) {
            ((DealPage) getActivity()).setPageEntity(entity);
        }
        productTitle.setText(entity.get("NAME", "").toString().trim());

        if (getActivity() instanceof DealPage) {
            if (entity.get("IS_SALEABLE") != null
                    && entity.get("IS_SALEABLE", "").toString().equals("1"))
                ((DealPage) getActivity()).setSaleAble(true);
            else
                ((DealPage) getActivity()).setSaleAble(false);
        }

        if (GlobalService.checkDiscout(entity)) {
            double specialPrice = Double.parseDouble(entity
                    .get("SPECIAL_PRICE", "").toString().trim());
            double price = Double.parseDouble(entity.get("PRICE", "")
                    .toString());

            currentPrice.setText("RM " + Calculator.round(specialPrice));
            originalPrice.setText("RM " + Calculator.round(price));

            originalPrice.setPaintFlags(originalPrice.getPaintFlags()
                    | Paint.STRIKE_THRU_TEXT_FLAG);
            originalPrice.setVisibility(View.VISIBLE);
            discount.setVisibility(View.VISIBLE);
            int discountValue = ((int) (100.0D - ((specialPrice / price) * 100)));
            discount.setText(discountValue + "% off");

            totalSold.setText(" / " + entity.get("FSF", "0") + " Sold");
        } else {
            try {
                currentPrice.setText("RM "
                        + Calculator.round(Double.parseDouble(entity.get(
                        "PRICE", "").toString())));
            } catch (Exception e) {
                currentPrice.setVisibility(View.GONE);
            }
            originalPrice.setVisibility(View.GONE);
            discount.setVisibility(View.GONE);
            totalSold.setText(entity.get("FSF", "0") + " Sold");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        productTitle = (TextView) view.findViewById(R.id.productTitle);
        productDescription = (TextView) view
                .findViewById(R.id.productDescription);
        currentPrice = (TextView) view.findViewById(R.id.currentPrice);
        currentPrice = (TextView) view.findViewById(R.id.currentPrice);
        originalPrice = (TextView) view.findViewById(R.id.originalPrice);
        discount = (TextView) view.findViewById(R.id.discount);
        totalSold = (TextView) view.findViewById(R.id.totalSold);
        // highlightsTxt = (TextView) view.findViewById(R.id.highlightsTxt);

        finePrintLayout = (LinearLayout) view
                .findViewById(R.id.finePrintLayout);
        // finePrintTxt = (TextView) view.findViewById(R.id.finePrintTxt);
        TextView finePrintTitle = (TextView) (view
                .findViewById(R.id.finePrintLayout)
                .findViewById(R.id.packageTitle));
        finePrintTitle.setText("Fine Print");
        finePrintText = (TextView) (TextView) (view
                .findViewById(R.id.finePrintLayout)
                .findViewById(R.id.packageInfor));

        // TODO:Stage 2
        packageIncludeLayout = (LinearLayout) view
                .findViewById(R.id.packageIncludedLayout);
        // finePrintTxt = (TextView) view.findViewById(R.id.finePrintTxt);
        TextView packageIncludedTitle = (TextView) (view
                .findViewById(R.id.packageIncludedLayout)
                .findViewById(R.id.packageTitle));
        packageIncludedTitle.setText("Package Included");
        packageInclude = (TextView) (TextView) (view
                .findViewById(R.id.packageIncludedLayout)
                .findViewById(R.id.packageInfor));

        hightlightLayout = (LinearLayout) view
                .findViewById(R.id.hightlightLayout);
        // finePrintTxt = (TextView) view.findViewById(R.id.finePrintTxt);
        TextView hightlightTitle = (TextView) (view
                .findViewById(R.id.hightlightLayout)
                .findViewById(R.id.packageTitle));
        hightlightTitle.setText("Highlights");
        hightlightText = (TextView) (TextView) (view
                .findViewById(R.id.hightlightLayout)
                .findViewById(R.id.packageInfor));

        finePrintLink = (TextView) (TextView) (view
                .findViewById(R.id.finePrintLayout)
                .findViewById(R.id.finePrintLink));
        finePrintLink.setVisibility(View.VISIBLE);
        finePrintLink.setText(getResources().getString(
                R.string.UniversalFinePrintTitle));
        finePrintLink.setOnClickListener(this);
        finePrintLink.setTextColor(getResources().getColor(
                R.color.hyperlinkColor));

        addWishlistBtn = (ImageView) view.findViewById(R.id.addWishlistBtn);
        addWishlistBtn.setOnClickListener(this);

        merchantStoreBtn = (LinearLayout) view
                .findViewById(R.id.merchantStoreBtn);
        merchantStoreBtn.setOnClickListener(this);

        view.findViewById(R.id.addthis_FacebookBtn).setOnClickListener(this);
        view.findViewById(R.id.addthis_TwitterBtn).setOnClickListener(this);
        view.findViewById(R.id.addthis_WhatsAppBtn).setOnClickListener(this);

        gallery = (GalleryLooping) view.findViewById(R.id.galleryLoopingID);
        if (getActivity() instanceof DealPage) {
            DealPage mDealPage = (DealPage) getActivity();
            if (!mDealPage.getEntityImageUrl().equals("")) {
                gallery.addData(mDealPage.getEntityImageUrl());
                gallery.notifyDataChanged();
            }
        }

        //======================================
        if (getActivity() instanceof DealPage) {
            // ((DealPage) getActivity()).addFragmentDataRequestListener(this);
            entity = ((DealPage) getActivity()).getPageEntity();

            // ((DealPage) getActivity()).showAddWishList(true);
        }

        if (entity != null)
            parseEntityObject(entity, 1);
        else
            initDetail(1, 1012);

        initDetail(4, 1003);// Get Product Image
        // get Service Voucher Detail
        initDetail(2, 1031);
        initDetail(3, 1027);
        initDetail(5, 1031);// Get PackageIncluded
        //======================================
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // if (getActivity() instanceof DealPage) {
        // ((DealPage) getActivity()).removeFragmentDataRequestListener(this);
        // }
    }

    public static Fragment newInstance(String entityId) {
        AboutFragment about = new AboutFragment();
        Bundle bundle = new Bundle();
        bundle.putString("ENTITY_ID", entityId);
        about.setArguments(bundle);
        return about;
    }

    @Override
    public void onAquireResult(BaseEntity entity, int position, Bundle params) {
        int downloadState = params.getInt("DOWNLOAD_STAGE", 0);
        if (params != null
                && params.getString("FragmentName").equalsIgnoreCase(
                getClass().getName() + "|" + downloadState)) {
            Message msg = new Message();
            // if (params.getBoolean("SHARE", false)) {
            // shareEnable = true;
            // if (entity != null && entity.get("SHARE_URL") != null) {
            // GlobalService
            // .ShareMessage(this,
            // "Hey, check out this awesome voucher I found at Bello2 "
            // + entity.get("SHARE_URL")
            // .toString().trim());
            // }
            // return;
            // }
            msg.what = downloadState;
            msg.obj = entity;

            handler.sendMessage(msg);
        }
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            parseEntityObject((BaseEntity) msg.obj, msg.what);
            super.handleMessage(msg);
        }
    };

    @Override
    public void onConnectionError(Exception ex) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.addWishlistBtn:
                GlobalService.AddToWishlist(getActivity(), entity);
                break;

            case R.id.merchantStoreBtn:
                if (getActivity() instanceof DealPage) {
                    ((DealPage) getActivity()).openMerchantStore();
                    GoogleAnalyticsLib.getInstance(getActivity()).trackEvent(
                            "Other Deals", null);
                }
                break;

            case R.id.finePrintLink:
                if (getActivity() instanceof DealPage) {
                    ((DealPage) getActivity()).openFinePrint();
                }
                break;

            case R.id.addthis_FacebookBtn:
                ((DealPage) getActivity()).shareToFacebook();
                break;

            case R.id.addthis_TwitterBtn:
                ((DealPage) getActivity()).shareTwitter();
                break;

            case R.id.addthis_WhatsAppBtn:
                ((DealPage) getActivity()).shareToWhatsApp();
                break;

            default:
                break;
        }

    }
}
