package com.citrineplexus.makandeal.view.activity;

import java.util.ArrayList;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.citrineplexus.bello2.BaseEntityAdpater;
import com.citrineplexus.bello2.ItemDisplayBinder;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;

public class SearchResultPage extends AbstractActivity implements
		OnRefreshListener, OnItemClickListener {

	private SwipeRefreshLayout mSwipeLayout = null;
	private ListView mListView = null;
	private TextView noContentMsg = null;
	private ProgressBar loadingProgressBar = null;
	private ArrayList<BaseEntity> searchArray = new ArrayList<BaseEntity>();
	private String query;
	private BaseEntityAdpater adapter = null;
	private final String title = "Search Page";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		handleIntent(getIntent());
		setContentView(R.layout.activity_search_page);
		mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		mSwipeLayout.setOnRefreshListener(this);
		noContentMsg = (TextView) findViewById(R.id.noContentMsg);
		loadingProgressBar = (ProgressBar) findViewById(R.id.loadingProgressBar);
		loadingProgressBar.setVisibility(View.GONE);
		mListView = (ListView) findViewById(R.id.pullToRefreshID);
		adapter = new BaseEntityAdpater(this, null);
		adapter.setDataSet(searchArray);
		adapter.setBinder(new ItemDisplayBinder());

		mListView.setAdapter(adapter);
		mListView.setOnItemClickListener(this);
		searchProduct();
	}

	private void searchProduct() {
		if (query.length() <= 0 || query.equals(""))
			return;
		Bundle localBundle = new Bundle();
		localBundle.putString("SEARCH_TEXT", query.replace(" ", "%20"));
		getServiceData(1026, 0, localBundle, true);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		handleIntent(intent);
		searchProduct();
	}

	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			query = intent.getStringExtra(SearchManager.QUERY);
			getSupportActionBar().setTitle(query);
			GoogleAnalyticsLib.getInstance(this).trackView(title);
			GoogleAnalyticsLib.getInstance(this).trackEvent("Search", query);
		}
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		super.onAquireResult(entity, position, params, true);
		if (!adapter.isEmpty())
			adapter.clearData();

		if (entity != null && entity.get("JSONARRAY") != null) {
			for (BaseEntity en : (ArrayList<BaseEntity>) entity
					.get("JSONARRAY"))
				adapter.addData(en);
			adapter.notifyDataSetChanged();
		}

		if (adapter.isEmpty())
			mListView.setEmptyView(noContentMsg);
	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.IS_SEARCHVIEW;
	}

	@Override
	int drawerID() {
		return 0;
	}

	@Override
	void handlerMessage(Message msg) {

	}

	@Override
	public void onRefresh() {
		searchProduct();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		BaseEntity entity = (BaseEntity) adapter.getItem(position);
		if (entity.get("ENTITY_ID") != null) {
			Bundle param = new Bundle();
			param.putString("ENTITY_ID", entity.get("ENTITY_ID").toString());
			openView(ViewName.DEAL_PAGE, param);
		}
	}

}
