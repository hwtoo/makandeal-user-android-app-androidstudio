package com.citrineplexus.makandeal.view.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.TextView;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapPage extends AbstractActivity implements OnMapReadyCallback,
		InfoWindowAdapter {

	private String shopName = "";
	private String shopAddress = "";

	MarkerOptions markerOptions;
	LatLng latLng;
	SupportMapFragment mapFragment;
	double lat = 0;
	double lng = 0;

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map_page);
		Intent mIntent = getIntent();
		if (mIntent != null && mIntent.getExtras() != null
				&& mIntent.getExtras().get("ADDRESS") != null)
			shopAddress = getIntent().getExtras().getString("ADDRESS");

		if (mIntent != null && mIntent.getExtras() != null
				&& mIntent.getExtras().get("SHOPNAME") != null)
			shopName = getIntent().getExtras().getString("SHOPNAME");

		setTitle(shopName);

		initGoogleMap(shopAddress);
		Bundle bundle = new Bundle();
		bundle.putString("ADDRESS", shopAddress);
		getServiceData(1000, 0, bundle, true);
	}

	private void initGoogleMap(String address) {
		mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
	}

	@Override
	public void onMapReady(GoogleMap map) {
		LatLng shopLocation = new LatLng(lat, lng);

		map.setMyLocationEnabled(true);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(shopLocation, 14));
		map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		map.setInfoWindowAdapter(this);

		Marker marker = map.addMarker(new MarkerOptions().title(shopName)
				.snippet(shopAddress).position(shopLocation));
		marker.showInfoWindow();
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		super.onAquireResult(entity, position, params, true);
		if (entity != null && entity.get("RESULTS") != null) {
			ArrayList<BaseEntity> ary = (ArrayList<BaseEntity>) entity
					.get("RESULTS");
			for (BaseEntity en : ary) {
				if (en != null && en.get("GEOMETRY") != null) {
					BaseEntity geometryEntity = (BaseEntity) en.get("GEOMETRY");
					BaseEntity location = (BaseEntity) geometryEntity
							.get("LOCATION");
					lat = (Double) location.get("LAT");
					lng = (Double) location.get("LNG");
					break;
				}
			}
			// BaseEntity location = (BaseEntity) ((BaseEntity) ary.get(0).get(
			// "GEOMETRY")).get("LOCATION");
			// lat = (Double) location.get("LAT");
			// lng = (Double) location.get("LNG");
		}
		mapFragment.getMapAsync(this);
	}

	@Override
	void handlerMessage(Message msg) {

	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.HIDE_ALL;
	}

	@Override
	int drawerID() {
		return 0;
	}

	@Override
	public View getInfoContents(Marker marker) {
		View v = getLayoutInflater().inflate(R.layout.map_custome_view, null);

		TextView vendorName = (TextView) v.findViewById(R.id.vendorName);
		TextView vendorAddress = (TextView) v.findViewById(R.id.vendorAddress);

		vendorName.setText(marker.getTitle());
		vendorAddress.setText(marker.getSnippet());
		return v;
	}

	@Override
	public View getInfoWindow(Marker marker) {
		return null;
	}

}
