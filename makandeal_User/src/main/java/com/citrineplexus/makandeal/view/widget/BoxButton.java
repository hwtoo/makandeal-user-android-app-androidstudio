package com.citrineplexus.makandeal.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.Button;

import com.citrineplexus.makandeal.R;

public class BoxButton extends Button {

	public BoxButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		if (!isInEditMode())
			init(context, attrs);
	}

	public BoxButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (!isInEditMode())
			init(context, attrs);
	}

	public BoxButton(Context context) {
		super(context);
	}

	private void init(Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.BoxButton, 0, 0);
		try {
			int mColor = a.getColor(R.styleable.BoxButton_BorderColor,
					getResources().getColor(R.color.colorPrimary));
			setBackgroundResource(R.drawable.box_button_bg);
			GradientDrawable drawable = (GradientDrawable) getBackground();
			drawable.setStroke(1, mColor);
		} finally {
			a.recycle();
		}

	}

	private static final int LEFT = 0, TOP = 1, RIGHT = 2, BOTTOM = 3;

	// Pre-allocate objects for layout measuring
	private Rect textBounds = new Rect();
	private Rect drawableBounds = new Rect();

	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		super.onLayout(changed, left, top, right, bottom);

		if (!changed)
			return;

		final CharSequence text = getText();
		if (!TextUtils.isEmpty(text)) {
			TextPaint textPaint = getPaint();
			textPaint.getTextBounds(text.toString(), 0, text.length(),
					textBounds);
		} else {
			textBounds.setEmpty();
		}

		final int width = getWidth() - (getPaddingLeft() + getPaddingRight());

		final Drawable[] drawables = getCompoundDrawables();

		if (drawables[LEFT] != null) {
			drawables[LEFT].copyBounds(drawableBounds);
			int leftOffset = (width
					- (textBounds.width() + drawableBounds.width()) + getRightPaddingOffset())
					/ 2 - getCompoundDrawablePadding();
			drawableBounds.offset(leftOffset, 0);
			drawables[LEFT].setBounds(drawableBounds);
		}

		if (drawables[RIGHT] != null) {
			drawables[RIGHT].copyBounds(drawableBounds);
			int rightOffset = ((textBounds.width() + drawableBounds.width())
					- width + getLeftPaddingOffset())
					/ 2 + getCompoundDrawablePadding();
			drawableBounds.offset(rightOffset, 0);
			drawables[RIGHT].setBounds(drawableBounds);
		}
	}
}
