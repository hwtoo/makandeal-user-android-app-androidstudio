package com.citrineplexus.makandeal.view.fragment;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.service.OAuthAuthorization;
import com.citrineplexus.makandeal.view.activity.CheckoutPage;

public class BillingFragment extends Fragment implements OnClickListener {

	private EditText firstName, lastName, eMail, contactNum, city, zip,
			address1, address2;
	private TextView country;
	private Spinner state;
	private Button next = null;
	private BaseEntity en = null;
	private CheckoutPage checkoutView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		GoogleAnalyticsLib.getInstance(getActivity()).trackView("Billing Info");
		if (getActivity() instanceof CheckoutPage) {
			checkoutView = (CheckoutPage) getActivity();
			en = checkoutView.getCheckoutEntity();
		}
		checkoutView.setTitle("Billing Information");
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_billing_infor, container,
				false);

		next = (Button) v.findViewById(R.id.next);
		next.setOnClickListener(this);

		firstName = (EditText) v.findViewById(R.id.firstName);
		lastName = (EditText) v.findViewById(R.id.lastName);
		eMail = (EditText) v.findViewById(R.id.email);
		contactNum = (EditText) v.findViewById(R.id.contactNum);
		address1 = (EditText) v.findViewById(R.id.address);
		address2 = (EditText) v.findViewById(R.id.address2);
		city = (EditText) v.findViewById(R.id.city);
		zip = (EditText) v.findViewById(R.id.zipcode);

		state = (Spinner) v.findViewById(R.id.state);

		country = (TextView) v.findViewById(R.id.country);
		country.setClickable(false);
		country.setFocusable(false);
		country.setText("Malaysia");

		if (en != null) {
			firstName.setText(en.get("FIRSTNAME", "").toString());
			lastName.setText(en.get("LASTNAME", "").toString());
			eMail.setText(en.get("EMAIL", "").toString());
			contactNum.setText(en.get("TELEPHONE", "").toString());
			if (en.get("STREET") != null)
				if (en.get("STREET") instanceof ArrayList<?>) {

					ArrayList<String> streetAry = ((ArrayList<String>) en
							.get("STREET"));
					if (streetAry.size() >= 1)
						address1.setText(streetAry.get(0));

					if (streetAry.size() >= 2)
						address2.setText(streetAry.get(1));

					// for (String addr : ((ArrayList<String>)
					// en.get("STREET")))
					// address.append(addr);
				} else if (en.get("STREET") instanceof String) {
					address1.setText(en.get("STREET").toString());
				}

			city.setText(en.get("CITY", "").toString());
			zip.setText(en.get("POSTCODE", "").toString());

			String[] ary = getResources().getStringArray(R.array.region);

			for (int i = 0; i < ary.length; i++) {
				if (ary[i].equalsIgnoreCase(en.get("REGION", "").toString())) {
					state.setSelection(i);
					break;
				}
			}

		}

		return v;
	}

	private boolean checkBillingInfoField(boolean checkingPut) {

		if (firstName.getText().toString().length() > 0
				&& lastName.getText().toString().length() > 0
				&& eMail.getText().toString().length() > 0
				&& contactNum.getText().toString().length() > 0
				&& address1.getText().toString().length() > 0
				&& city.getText().toString().length() > 0
				&& zip.getText().toString().length() > 0
				&& country.getText().length() > 0) {
			if (checkingPut) {
				en.put("FIRSTNAME", firstName.getText().toString());
				en.put("LASTNAME", lastName.getText().toString());
				en.put("EMAIL", eMail.getText().toString());
				en.put("TELEPHONE", contactNum.getText().toString());
				// for (int i = 0; i < addressAry.length; i++) {
				// ArrayList<String> mStreet = new ArrayList<String>();
				// mStreet.add(address1.getText().toString());
				// mStreet.add(address2.getText().toString());
				en.put("STREET", address1.getText().toString());
				en.put("STREET2", address2.getText().toString());
				// }
				en.put("CITY", city.getText().toString());
				en.put("POSTCODE", zip.getText().toString());
				String selectedState = state.getSelectedItem().toString();
				
				en.put("REGION", selectedState);
				for (int i = 0; i < getResources().getStringArray(
						R.array.region).length; i++) {
					if (getResources().getStringArray(R.array.region)[i]
							.equalsIgnoreCase(selectedState)) {
						en.put("REGION_ID",
								getResources().getIntArray(R.array.region_id)[i]);
					}
				}
				en.put("COUNTRY_ID", "MY");
			}
			return true;
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.next:
			if (checkBillingInfoField(true)) {
				OAuthAuthorization.getInstance().getUserProfileData().merge(en);
				checkoutView.checkFragment();
			} else
				Toast.makeText(getActivity(), "All fields are compulsory.",
						Toast.LENGTH_SHORT).show();
			break;

		default:
			break;
		}

	}

}
