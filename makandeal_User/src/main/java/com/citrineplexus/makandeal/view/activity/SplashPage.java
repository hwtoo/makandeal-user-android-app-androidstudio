package com.citrineplexus.makandeal.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.view.MakanDealApplication;
import com.citrineplexus.makandeal.view.activity.AbstractActivity.ViewName;

public class SplashPage extends Activity {
	private ImageView logoImg = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_page);

		logoImg = (ImageView) findViewById(R.id.logo);
		// logoImg.post(show);
		logoImg.postDelayed(Action, 2500L);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		logoImg = null;

	}

	/**
	 * Alpha Splash Image
	 */
	// private Runnable show = new Runnable() {
	// public void run() {
	// logoImg.setVisibility(View.VISIBLE);
	// AlphaAnimation localAlphaAnimation = new AlphaAnimation(0.0F, 1.0F);
	// localAlphaAnimation.setDuration(1500);
	// localAlphaAnimation.setInterpolator(new AccelerateInterpolator());
	// logoImg.setAnimation(localAlphaAnimation);
	// }
	// };

	/**
	 * delay 2.5 second, then open next page
	 */
	private Runnable Action = new Runnable() {
		public void run() {
			Bundle localBundle = getIntent().getExtras();
			if (localBundle == null)
				localBundle = new Bundle();
			checkPassCode(localBundle.getBoolean("GCM_MESSAGE", false),
					localBundle);
		}
	};

	/**
	 * Open next view
	 * 
	 * @param viewName
	 *            page to open
	 * @param localBundle
	 *            parameters pass to next activity
	 */
	private void openView(ViewName viewName, Bundle localBundle) {
		Intent mIntent = null;
		switch (viewName) {
		case PASSCODE:
			mIntent = new Intent(this, PasscodePage.class);
			break;

		case DEAL_PAGE:
			mIntent = new Intent(this, DealPage.class);
			break;

		case HOMEPAGE:
//			mIntent = new Intent(this, HomePage_BK.class);
			mIntent = new Intent(this, HomePage.class);
			break;

		default:
			break;
		}
		if (mIntent != null) {
			if (localBundle != null)
				mIntent.putExtras(localBundle);
			startActivity(mIntent);
		}
		finish();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK)
			return true;
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * Check user had set the passcode
	 * 
	 * @param flagGCM
	 *            if true, GoogleCloudMessaging received. open service Detail
	 *            page
	 * @param localBundle
	 *            parameter from GoogleCloudMessaging
	 */
	private void checkPassCode(boolean flagGCM, Bundle localBundle) {
		if (MakanDealApplication.getInstance().getPreferences()
				.getString("PASSCODE", null) != null) {
			if (localBundle == null)
				localBundle = new Bundle();
			localBundle.putInt("PASS_OPTION", 1);
			openView(ViewName.PASSCODE, localBundle);
		} else {
			if (flagGCM) {
				openView(ViewName.DEAL_PAGE, localBundle);
			} else {
				localBundle.putBoolean("FROM_SPALSH", true);
				openView(ViewName.HOMEPAGE, localBundle);
			}
		}
	}

}
