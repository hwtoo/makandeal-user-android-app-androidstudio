package com.citrineplexus.makandeal.view.activity;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.view.MakanDealApplication;
import com.citrineplexus.makandeal.view.widget.PasscodeButton;

public class PasscodePage extends AbstractActivity implements OnClickListener {

	private ImageView[] resultBox = new ImageView[4];
	private PasscodeButton[] numberBtn = null;
	private TextView passcodeTitle = null, incorrectTxt = null;
	private String passcode = "";
	private int[] btnID = { R.id.number1, R.id.number2, R.id.number3,
			R.id.number4, R.id.number5, R.id.number6, R.id.number7,
			R.id.number8, R.id.number9, R.id.number0 };
	private Bundle localBundle;
	private boolean setPasscode = false;
	private String confirmPasscode = "";
	private String tempPasscode = "";
	private int setPasscodeStep = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setTitle("Passcode");
		setContentView(R.layout.activity_passcode_page);
		passcodeTitle = (TextView) findViewById(R.id.passcodeTitle);
		incorrectTxt = (TextView) findViewById(R.id.incorrectPass);

		resultBox[0] = (ImageView) findViewById(R.id.passcode1);
		resultBox[1] = (ImageView) findViewById(R.id.passcode2);
		resultBox[2] = (ImageView) findViewById(R.id.passcode3);
		resultBox[3] = (ImageView) findViewById(R.id.passcode4);

		findViewById(R.id.deleteBtn).setOnClickListener(this);

		numberBtn = new PasscodeButton[btnID.length];
		for (int i = 0; i < btnID.length; i++) {
			numberBtn[i] = (PasscodeButton) findViewById(btnID[i]);
			numberBtn[i].setOnClickListener(this);
			if (i == 9)
				numberBtn[i].setTag(0);
			else
				numberBtn[i].setTag((i + 1));
		}

		localBundle = getIntent().getExtras();

		if (localBundle != null && localBundle.getInt("PASS_OPTION", -1) != -1)
			switch (localBundle.getInt("PASS_OPTION")) {
			case 1:// ENTER PASSCODE
				setPasscode = false;
				confirmPasscode = MakanDealApplication.getInstance()
						.getPreferences().getString("PASSCODE", "");
				break;
			case 2:// SET PASSCODE
				setPasscode = true;
				setPasscodeStep = 1;
				break;
			}

	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {

	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.HIDE_ALL;
	}

	@Override
	int drawerID() {
		return 108;
	}

	@Override
	void handlerMessage(Message msg) {

	}

	private void checkPasscode() {
		if (passcode.length() < 4)
			return;
		if (setPasscode) {
			handleSetPasscode();
		} else {
			if (passcode.equals(confirmPasscode)) {
				if (localBundle != null
						&& localBundle.getBoolean("GCM_MESSAGE", false))
					 openView(ViewName.DEAL_PAGE, localBundle);
				else {
					Bundle newBundle = new Bundle();
					newBundle.putBoolean("FROM_SPALSH", true);
					openView(ViewName.HOMEPAGE, newBundle);
				}
				finish();
			} else {
				incorrectTxt.setText("Incorrect passcode");
				incorrectTxt.setVisibility(View.VISIBLE);
				passcode = "";
			}
		}
	}

	private void handleSetPasscode() {
		if (setPasscodeStep == 1) {
			tempPasscode = passcode;
			passcode = "";
			setPasscodeStep = 2;
			passcodeTitle.setText("Re-Enter Passcode");
		} else if (setPasscodeStep == 2) {
			if (tempPasscode.equals(passcode)) {
				MakanDealApplication.getInstance().getPreferences().edit()
						.putString("PASSCODE", passcode).commit();
				Toast.makeText(this, "Successful set passcode.",
						Toast.LENGTH_SHORT).show();
				finish();
			} else {
				incorrectTxt.setText("Passcode does not match");
				incorrectTxt.setVisibility(View.VISIBLE);
				passcode = "";
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.number0:
		case R.id.number1:
		case R.id.number2:
		case R.id.number3:
		case R.id.number4:
		case R.id.number5:
		case R.id.number6:
		case R.id.number7:
		case R.id.number8:
		case R.id.number9:
			if (passcode.length() < 4)
				passcode += String.valueOf((Integer) v.getTag());
			checkPasscode();
			break;

		case R.id.deleteBtn:
			passcode = "";
			break;

		default:
			break;
		}
		for (int i = 0; i < 4; i++)
			resultBox[i]
					.setImageResource(i < passcode.length() ? R.drawable.passcode_icon2
							: R.drawable.passcode_icon1);
	}

}
