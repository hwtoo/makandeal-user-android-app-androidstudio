package com.citrineplexus.makandeal.view.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.citrineplexus.library.gcm.GCMRegisterHandler;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.callback.DialogCallback;
import com.citrineplexus.makandeal.callback.OAuthListener;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.service.OAuthAuthorization;
import com.citrineplexus.makandeal.util.GlobalService;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

public class SignUpPage extends AbstractActivity implements OnClickListener,
/* StatusCallback, */DialogCallback, OAuthListener {

	private EditText emailEditText, firstNameEditText = null,
			lastNameEditText = null, passwordEditText = null,
			confirmPassEditText = null;

	private final String title = "Sign Up";
	// TODO: Facebook SDK
	private CallbackManager callbackManager;

	// private UiLifecycleHelper uiHelper = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// uiHelper = new UiLifecycleHelper(this, this);
		// uiHelper.onCreate(savedInstanceState);

		setContentView(R.layout.activity_signup_page);
		getSupportActionBar().setTitle(title);
		GoogleAnalyticsLib.getInstance(this).trackView(title);

		firstNameEditText = (EditText) findViewById(R.id.firstNameEditText);
		emailEditText = (EditText) findViewById(R.id.emailEditText);
		lastNameEditText = (EditText) findViewById(R.id.lastNameEditText);
		passwordEditText = (EditText) findViewById(R.id.passwordEditText);
		confirmPassEditText = (EditText) findViewById(R.id.confirmPassowrdEditText);

		findViewById(R.id.signUpBtn).setOnClickListener(this);
		findViewById(R.id.signInBtn).setOnClickListener(this);

		// TODO: Facebook SDK
		LoginButton fbLogin = (LoginButton) findViewById(R.id.fbLoginButton);

		callbackManager = CallbackManager.Factory.create();
		fbLogin.registerCallback(callbackManager,
				new FacebookCallback<LoginResult>() {
					@Override
					public void onSuccess(LoginResult loginResult) {
						if (!loadingOpen) {
							handler.sendEmptyMessage(LOADING_OPEN);
							GoogleAnalyticsLib.getInstance(SignUpPage.this)
									.trackEvent("Sign Up",
											"Sign In with Facebook");
							OAuthAuthorization.getInstance().loginWithFacebook(
									SignUpPage.this,
									SignUpPage.this,
									loginResult.getAccessToken().getToken(),
									loginResult.getAccessToken().getExpires()
											.toString());
						}
					}

					@Override
					public void onCancel() {
					}

					@Override
					public void onError(FacebookException exception) {
					}
				});

		// fbLogin.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_fb,
		// 0,
		// 0, 0);
		// fbLogin.setCompoundDrawablePadding(20);
		// fbLogin.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
	}

	@Override
	protected void onResume() {
		super.onResume();
		// TODO: Facebook SDK
		// uiHelper.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		// TODO: Facebook SDK
		callbackManager.onActivityResult(requestCode, resultCode, data);
		// uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
	}

	@Override
	void handlerMessage(Message msg) {
		// if (loadingOpen)
		// handler.sendEmptyMessage(LOADING_CLOSE);
		if (msg.what == 1000) {
			openView(ViewName.SIGN_IN, null);
			setResult(RESULT_OK, new Intent());
			finish();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.signUpBtn:
			String email = emailEditText.getText().toString();
			String password = passwordEditText.getText().toString();
			String confirmPassword = confirmPassEditText.getText().toString();
			String firstName = firstNameEditText.getText().toString();
			String lastName = lastNameEditText.getText().toString();

			if (email.equals("") || email.length() <= 0 || password.equals("")
					|| confirmPassword.equals("") || firstName.equals("")
					|| firstName.length() <= 0 || lastName.equals("")
					|| lastName.length() <= 0)
				Toast.makeText(this, "All fields are compulsory.",
						Toast.LENGTH_SHORT).show();
			else if (password.length() < 6)
				Toast.makeText(this,
						"Passwords must be at least six (6) characters long.",
						Toast.LENGTH_SHORT).show();
			else if (!password.equals(confirmPassword))
				Toast.makeText(this, "These passwords don't match.",
						Toast.LENGTH_SHORT).show();
			else if (!GlobalService.isEmailValid(email))
				Toast.makeText(this, "Please enter a valid email address.",
						Toast.LENGTH_SHORT).show();
			else
				try {
					Bundle params = new Bundle();
					params.putString("EMAIL", email);
					params.putString("PASSWORD", password);
					params.putString("FIRSTNAME", firstName);
					params.putString("LASTNAME", lastName);
					params.putBoolean("SIGN_ADMIN", true);
					params.putBoolean("LOADING", true);
					getServiceData(1039, 0, params, true);
					GoogleAnalyticsLib.getInstance(this).trackEvent("Sign Up",
							null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			break;

		case R.id.signInBtn:
			handler.sendEmptyMessage(1000);
			break;

		default:
			break;
		}
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		super.onAquireResult(entity, position, params, true);
		if (entity != null)
			if (entity.get("MESSAGES") != null) {
				BaseEntity msgEntity = (BaseEntity) entity.get("MESSAGES");
				ArrayList<BaseEntity> errorAry = (ArrayList<BaseEntity>) msgEntity
						.get("ERROR");
				BaseEntity errorEntity = errorAry.get(0);
				openDialog(ViewName.THANKYOU_ONEBTN_DIALOG, this, new String[] {
						(String) errorEntity.get("MESSAGE"), "ERROR" });
			} else
				openDialog(ViewName.THANKYOU_ONEBTN_DIALOG, this, new String[] {
						"Please check your email inbox.", "COMPLETE" });

	}

	// TODO: Facebook SDK
	// @Override
	// public void call(Session session, SessionState state, Exception
	// exception) {
	// if (exception != null)
	// exception.printStackTrace();
	// if (session.isOpened()) {
	// // accessToken = session.getAccessToken();
	// GoogleAnalyticsLib.getInstance(this).trackEvent("Sign Up",
	// "Sign In with Facebook");
	// OAuthAuthorization.getInstance().loginWithFacebook(this, this,
	// session.getAccessToken(),
	// session.getExpirationDate().toString());
	// }
	// }

	@Override
	public void returnMessage(int id, Object obj) {
		if (id == -9999 && obj instanceof BaseEntity) {
			handler.sendEmptyMessage(LOADING_CLOSE);
			BaseEntity msgEntity = (BaseEntity) obj;
			if (msgEntity.get("ERROR") != null) {
				ArrayList<BaseEntity> errorAry = (ArrayList<BaseEntity>) msgEntity
						.get("ERROR");
				BaseEntity errorEntity = (BaseEntity) errorAry.get(0);
				if (errorEntity.get("MESSAGE") != null) {
					Toast.makeText(this, errorEntity.get("MESSAGE").toString(),
							Toast.LENGTH_SHORT).show();

					// TODO: Facebook SDK
					LoginManager.getInstance().logOut();
				}
			}
		}
	}

	@Override
	public void oauthReady(boolean status, Bundle extra) {

	}

	@Override
	public void onAcquireProfile() {
		// handler.sendEmptyMessage(1001);
		handler.sendEmptyMessage(LOADING_CLOSE);
		try {
			// Register/update user profile to Notification Database
			new GCMRegisterHandler().registerApp(this);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		setResult(RESULT_OK, new Intent());
		finish();
	}

	@Override
	public void onDialogClick(int id, ViewName whichDialog, Object result) {
		if (result instanceof String) {
			if (((String) result).equals("COMPLETE")) {
				// handler.sendEmptyMessage(1000);
				openView(ViewName.HOMEPAGE, null);
				setResult(RESULT_OK, new Intent());
				finish();
			}
		}
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.ALL;
	}

	@Override
	int drawerID() {
		return 101;
	}

}
