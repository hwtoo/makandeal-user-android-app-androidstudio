//package com.citrineplexus.makandeal.view.activity;
//


//TODO: remove CampaignPage for stage 2

//import java.util.ArrayList;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.os.Message;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v4.widget.SwipeRefreshLayout;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import com.citrineplexus.bello2.FilterAdapter;
//import com.citrineplexus.bello2.ItemDisplayBinder;
//import com.citrineplexus.makandeal.R;
//import com.citrineplexus.makandeal.callback.OnDynamicListViewRefreshListener;
//import com.citrineplexus.makandeal.entity.BaseEntity;
//import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
//import com.citrineplexus.makandeal.view.widget.DynamicDataListView;
//
//public class CampaignPage extends AbstractActivity implements
//		OnDynamicListViewRefreshListener, OnItemClickListener {
//	private DynamicDataListView mListview = null;
//	private ProgressBar mLoadingProgressBar = null;
//	private int filterInt =-1;
//	private String campaignType = "";
//	private TextView mNoContentMsg = null;
//	private ImageView noContentImg = null;
//	private int pageCount = 1;
//
//	private ArrayList<BaseEntity> campaignArray = new ArrayList<BaseEntity>();
//
//	@Override
//	protected void onNewIntent(Intent intent) {
//		super.onNewIntent(intent);
//		if (intent != null && intent != null) {
//			initIntent(intent.getExtras());
//		}
//		if (mListview != null && !mListview.isEmpty())
//			mListview.clearData();
//
//		initCampaignProductExt();
//	}
//
//	private void initIntent(Bundle bundle) {
//		String campaign_name = bundle.getString("CAMPAIGN_NAME");
//		GoogleAnalyticsLib.getInstance(this).trackView(campaign_name);
//		getSupportActionBar().setTitle(campaign_name);
//		
//		campaignType = bundle.getString("CAMPAIGN_EVENT");
//		filterInt = bundle.getInt("CAMPAIGN_VIEW");
////		ImageLoader.getInstance(this).DisplayImage(
////				bundle.getString("CAMPAIGN_IMGURL"), campaignImage,
////				ScaleType.FIT_CENTER, false);
//	}
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_category_page);
//		mNoContentMsg = (TextView) findViewById(R.id.noContentMsg);
//		noContentImg = (ImageView) findViewById(R.id.noConteImage);
//		mLoadingProgressBar = (ProgressBar) findViewById(R.id.loadingProgressBar);
////		campaignImage = (ImageView) findViewById(R.id.CampaignImage);
////		campaignImage.setVisibility(View.VISIBLE);
//
//		if (getIntent() != null && getIntent().getExtras() != null) {
//			Bundle bundle = getIntent().getExtras();
//			initIntent(bundle);
//		}
//
//		ListView listview = (ListView) findViewById(R.id.pullToRefreshID);
//		SwipeRefreshLayout swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
//
//		FilterAdapter adapter = new FilterAdapter(this);
//		adapter.setDataSet(campaignArray);
//
//		mListview = new DynamicDataListView(this, this, listview, swipeLayout,
//				adapter, new ItemDisplayBinder(), this, -111,-1);
//		if (adapter.isEmpty()) {
//			initCampaignProductExt();
//		}
//
//	}
//
//	private void initCampaignProductExt() {
//		mListview.setRequestData(true);
//		mListview.setSwipeEnable(false);
//		mListview.initScorllLIstener();
//		Bundle localBundle = new Bundle();
//		localBundle.putString("REQUEST_TYPE", "INITIAL");
//		localBundle.putString("Campaign_Event", campaignType);
//		localBundle.putString("Campaign_View", String.valueOf(filterInt));
//		localBundle.putString("PAGE", "1");
//		getServiceData(1052, 0, localBundle, false);
//	}
//
//	@Override
//	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
//		if (entity == null || params == null)
//			return;
//		Message msg = new Message();
//
//		if (params.getString("REQUEST_TYPE").equals("INITIAL"))
//			msg.what = 1001;
//		else if (params.getString("REQUEST_TYPE").equals("ADDITIONAL"))
//			msg.what = 1002;
//		msg.obj = entity;
//		handler.sendMessage(msg);
//	}
//
//	@Override
//	int setDrawerLockMode() {
//		return DrawerLayout.LOCK_MODE_UNLOCKED;
//	}
//
//	@Override
//	ActionBarMethod setActionBarMethod() {
//		return ActionBarMethod.ALL;
//	}
//
//	@Override
//	int drawerID() {
//		return -1;
//	}
//
//	@Override
//	void handlerMessage(Message msg) {
//		switch (msg.what) {
//		case 1001:// Refresh All Data
//			mListview.clearData();
//
//		case 1002:// Additional Data
//			if (msg.obj != null)
//				if (msg.obj instanceof BaseEntity) {
//					if (((BaseEntity) msg.obj).get("JSONARRAY") != null) {
//						ArrayList<BaseEntity> dataAry = (ArrayList<BaseEntity>) ((BaseEntity) msg.obj)
//								.get("JSONARRAY");
//						// if (!(isLastPage =
//						// mListview.checklastTime(dataAry
//						// .get(dataAry.size() - 1))))
//						for (BaseEntity en : dataAry)
//							mListview.addData(en,false);
//					}
//				} else if (msg.obj instanceof ArrayList<?>) {
//					mListview.addData((ArrayList<BaseEntity>) msg.obj, null,false);
//				}
//			mListview.notifyDataSetChanged();
//			break;
//
//		case 1003:
//			if (msg.obj instanceof LoadResponseFlag)
//				onLoadResponse((LoadResponseFlag) msg.obj);
//			break;
//
//		}
//	}
//
//	@Override
//	public void onRefresh() {
//		if (mListview != null && !mListview.isRequestingData()) {
//			pageCount = 1;
//			initCampaignProductExt();
//		}
//	}
//
//	@Override
//	public void onLoadMore() {
//		int count = mListview.getCount();
//		if (!mListview.isRequestingData() && pageCount != (1 + count / 50)) {
//			mListview.setRequestData(true);
//			Bundle localBundle = new Bundle();
//			localBundle.putString("REQUEST_TYPE", "ADDITIONAL");
//
//			pageCount = (1 + count / 50);
//			localBundle.putString("PAGE", String.valueOf((1 + count / 50)));
//			getServiceData(1052, 0, localBundle, false);
//		} else {
//			if (mListview.isEmpty()) {
//				Message msg = new Message();
//				msg.what = 1003;
//				msg.obj = LoadResponseFlag.NO_CONTENT;
//				handler.sendMessage(msg);
//			}
//			mListview.removeFooter();
//		}
//	}
//
//	@Override
//	public void onLoadResponse(LoadResponseFlag flag) {
//		mLoadingProgressBar.setVisibility(View.GONE);
//		switch (flag) {
//		case HAVE_CONTENT:
//			mNoContentMsg.setVisibility(View.GONE);
//			noContentImg.setVisibility(View.GONE);
//			mListview.setVisible(View.VISIBLE);
//			break;
//
//		case NO_CONNECTION:
//			mNoContentMsg.setText(R.string.noConnectionMsg);
//			noContentImg.setVisibility(View.GONE);
//
//		case NO_CONTENT:
//			mNoContentMsg.setVisibility(View.VISIBLE);
//			noContentImg.setVisibility(View.VISIBLE);
//			mListview.setVisible(View.GONE);
//			break;
//
//		}
//	}
//
//	@Override
//	public void onItemClick(AdapterView<?> parent, View view, int position,
//			long id) {
//		BaseEntity entity = (BaseEntity) mListview.getAdatper().getItem(
//				position);
//
//		if (entity.get("CAMPAIGN_NAME") != null) {
//			handleEventAndPromoLogic(entity);
//		} else if (entity.get("ENTITY_ID") != null) {
//			Bundle param = new Bundle();
//			param.putString("ENTITY_ID", entity.get("ENTITY_ID").toString());
//			openView(ViewName.DEAL_PAGE, param);
//		}
//	}
//
//}
