package com.citrineplexus.makandeal.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.ExpandableAdapter;
import com.citrineplexus.makandeal.binder.LocationChildViewHolder;
import com.citrineplexus.makandeal.callback.OnExpandableListRender;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.util.GlobalService;
import com.citrineplexus.makandeal.view.activity.DealPage;
import com.citrineplexus.makandeal.view.widget.BoxButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class LocationFragment extends Fragment implements
		OnDataRequestListener, OnExpandableListRender {

	private ExpandableListView expandListview = null;
	private ExpandableAdapter adapter = null;
	private ProgressBar loadingBar = null;

	private List<String> listDataHeader = new ArrayList<String>();
	private HashMap<String, List<BaseEntity>> listDataChild = new HashMap<String, List<BaseEntity>>();
	private HashMap<String, List<BaseEntity>> tempHashMap = new HashMap<String, List<BaseEntity>>();
	private String vendorId = "";// , vendorName = "";

	public static Fragment newInstance(String vendorId) {
		LocationFragment about = new LocationFragment();
		Bundle bundle = new Bundle();
		bundle.putString("VENDOR_ID", vendorId);
		about.setArguments(bundle);
		return about;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null && getArguments().get("VENDOR_ID") != null) {
			vendorId = getArguments().getString("VENDOR_ID");
		}
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser&&context!=null)
			GoogleAnalyticsLib.getInstance(getActivity())
					.trackView("Locations");
	}
	private Context context;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(this.context==null)
			this.context = context;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_expandable_listview,
				container, false);
		// View footer = getActivity().getLayoutInflater().inflate(
		// R.layout.deal_fragment_footer, null);
		// TODO: Stage 2
		View footer = getActivity().getLayoutInflater().inflate(
				R.layout.more_merchant_layout, null);
		footer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (getActivity() instanceof DealPage) {
					((DealPage) getActivity()).openMerchantStore();
					GoogleAnalyticsLib.getInstance(getActivity()).trackEvent(
							"Other Deals", null);
				}
			}
		});

		SwipeRefreshLayout swipe_container = (SwipeRefreshLayout) view
				.findViewById(R.id.swipe_container);
		swipe_container.setEnabled(false);

		expandListview = (ExpandableListView) view
				.findViewById(R.id.expandListView);
		expandListview.setDividerHeight(1);

		loadingBar = (ProgressBar) view.findViewById(R.id.loadingBar);

		// prepareListData();
		adapter = new ExpandableAdapter(this);
		adapter.set_listDataChild(listDataChild);
		adapter.set_listDataHeader(listDataHeader);

		expandListview.addFooterView(footer);

		expandListview.setAdapter(adapter);

		Bundle param = new Bundle();
		if (vendorId != null && !vendorId.equals("")) {
			param.putString("FragmentName", this.getClass().getName());
			param.putString("UDROPSHIP_VENDOR", vendorId);
			param.putBoolean("LOADING", false);
			param.putInt("CONTENT_ID", 1018);
			((DealPage) getActivity()).getFragmentData(param, this);
		}
		return view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	private void prepareListData() {
		SparseArray<String> myStringArray = parseStringArray(R.array.region);
		ArrayList<String> headerKey = new ArrayList<String>();
		// Adding Header data
		for (String key : tempHashMap.keySet()) {
			headerKey.add(key);
			listDataHeader.add(myStringArray.get(Integer.valueOf(key)));
		}

		for (int i = 0; i < listDataHeader.size(); i++) {
			// List<BaseEntity> tempData = tempHashMap.get(headerKey.get(i));
			listDataChild.put(listDataHeader.get(i),
					tempHashMap.get(headerKey.get(i))); // Header, Child
														// data
		}
	}

	private SparseArray<String> parseStringArray(int stringArrayResourceId) {
		String[] stringArray = getResources().getStringArray(
				stringArrayResourceId);
		int[] intArray = getResources().getIntArray(R.array.region_id);

		SparseArray<String> outputArray = new SparseArray<String>(
				stringArray.length);
		for (int i = 0; i < intArray.length; i++) {
			outputArray.put(intArray[i], stringArray[i]);
		}
		// for (String entry : stringArray) {
		// outputArray.put(Integer.valueOf(splitResult[0]), intArray[1]);
		// }
		return outputArray;
	}

	@Override
	public void onPause() {
		super.onPause();
		((DealPage) getActivity()).removeFragmentDataRequestListener(this);
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		if (params.getString("FragmentName").equalsIgnoreCase(
				getClass().getName())) {
			Message msg = new Message();
			msg.what = 0;
			msg.obj = entity;
			handler.sendMessage(msg);
		}
	}

	Handler handler = new Handler() {
		public void handleMessage(Message msg) {

			switch (msg.what) {
			case 0:
				loadingBar.setVisibility(View.GONE);
				if (msg.obj instanceof BaseEntity) {
					BaseEntity entity = (BaseEntity) msg.obj;
					if (((BaseEntity) entity).get("JSONARRAY") != null) {

						boolean finishLoop = false;

						ArrayList<BaseEntity> templist = (ArrayList<BaseEntity>) ((BaseEntity) entity)
								.get("JSONARRAY");
						ArrayList<BaseEntity> allBranchList = new ArrayList<BaseEntity>();

						for (BaseEntity abc : templist)
							allBranchList.add(abc);

						Collections.sort(allBranchList,
								BaseEntity.COMPARE_BY_REGION);

						if (!tempHashMap.isEmpty())
							tempHashMap.clear();

						if (!listDataHeader.isEmpty())
							listDataHeader.clear();

						if (!listDataChild.isEmpty())
							listDataChild.clear();

						while (!finishLoop) {

							// boolean startLooping = true;
							ArrayList<BaseEntity> groupArrayList = new ArrayList<BaseEntity>();
							String regionId = (String) allBranchList.get(0)
									.get("REGION_ID");

							int allItemSize = allBranchList.size();
							for (int i = 0; i < allItemSize;) {

								BaseEntity en = allBranchList.get(i);
								if (en.get("REGION_ID") != null
										&& en.get("REGION_ID").toString()
												.equals(regionId)) {
									groupArrayList.add(en);
									allBranchList.remove(en);
								} else
									i++;
								allItemSize = allBranchList.size();
							}
							tempHashMap.put(regionId, groupArrayList);

							if (allBranchList.isEmpty())
								finishLoop = true;
						}
						prepareListData();
					}
				}
				adapter.notifyDataSetChanged();

				break;

			default:
				break;
			}
		}
	};

	@Override
	public void onConnectionError(Exception ex) {

	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) adapter.getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) getActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.child_location_header,
					null);
		}
		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.locationHeader);
		lblListHeader.setText(headerTitle);
		expandListview.expandGroup(groupPosition);
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final BaseEntity entity = (BaseEntity) adapter.getChild(groupPosition,
				childPosition);

		// final String childText = (String) getChild(groupPosition,
		// childPosition);

		LocationChildViewHolder holder = null;
		if (convertView == null) {

			convertView = ((LayoutInflater) getActivity().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE)).inflate(
					R.layout.child_location_row, parent, false);
			holder = new LocationChildViewHolder();
			holder.address = (TextView) convertView.findViewById(R.id.address);
			holder.contactNumber = (TextView) convertView
					.findViewById(R.id.contactNumber);
			holder.operationHour = (TextView) convertView
					.findViewById(R.id.operationHour);
			holder.contactUs_Phone = (BoxButton) convertView
					.findViewById(R.id.contactUs_Phone);
			holder.map = (BoxButton) convertView.findViewById(R.id.map);
			convertView.setTag(holder);
		} else
			holder = (LocationChildViewHolder) convertView.getTag();

		if (holder.address != null)
			holder.address.setText(Html.fromHtml(entity.get("STREET", "")
					.toString().trim()
					+ " "
					+ entity.get("ZIP", "").toString().trim()
					+ " "
					+ entity.get("CITY", "").toString().trim()));

		if (holder.contactNumber != null) {
			holder.contactNumber.setText((String) entity.get("TELEPHONE", ""));
		}

		if (entity.get("BUSINESS_HOUR") != null) {
			String businessHour = entity.get("BUSINESS_HOUR", "").toString()
					.trim();

			if (holder.operationHour != null) {
				holder.operationHour.setText(Html.fromHtml(businessHour));
			}
		}

		// if (holder.address != null)
		// holder.address.setText(childText);

		holder.contactUs_Phone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (entity.get("TELEPHONE") != null
						&& !entity.get("TELEPHONE", "").equals("")) {
					GoogleAnalyticsLib.getInstance(getActivity()).trackEvent(
							"Call", "From DealPage");
					GlobalService.callSupportIntent(getActivity(),
							(String) entity.get("TELEPHONE"));
				}

			}
		});

		holder.map.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (getActivity() instanceof DealPage) {
					String address = entity.get("STREET", "").toString().trim()
							+ " " + entity.get("ZIP", "").toString().trim()
							+ " " + entity.get("CITY", "").toString().trim();
					address = address.replaceAll("\r", "").replaceAll("\n", "");
					((DealPage) getActivity()).openMap(address, "");
					GoogleAnalyticsLib.getInstance(getActivity()).trackEvent(
							"Map", "From Deal Page");
				}
			}
		});

		return convertView;
	}

}
