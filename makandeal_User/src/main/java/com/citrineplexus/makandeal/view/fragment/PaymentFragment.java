package com.citrineplexus.makandeal.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.view.activity.AbstractActivity.ViewName;
import com.citrineplexus.makandeal.view.activity.CheckoutPage;

public class PaymentFragment extends Fragment implements OnClickListener {

	private CheckoutPage checkoutView;
	private BaseEntity checkoutEntity;
	private Button next = null;
	private TextView terms = null;
	private RadioGroup paymentMethod = null;
	private final String title = "Payment Method";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		GoogleAnalyticsLib.getInstance(getActivity()).trackView(title);
		if (getActivity() instanceof CheckoutPage) {
			checkoutView = (CheckoutPage) getActivity();
			checkoutView.setTitle(title);
			checkoutEntity = checkoutView.getCheckoutEntity();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View holder = inflater.inflate(R.layout.fragment_payment, container,
				false);

		next = (Button) holder.findViewById(R.id.checkoutNow);
		next.setOnClickListener(this);
		paymentMethod = (RadioGroup) holder.findViewById(R.id.paymentGroup);
		terms = (TextView) holder.findViewById(R.id.terms);
		terms.setOnClickListener(this);
		return holder;
	}

	@Override
	public void onClick(View v) {
		String paymentmethod = "";
		switch (v.getId()) {

		case R.id.checkoutNow:
			int selectedId = 0;
			switch (paymentMethod.getCheckedRadioButtonId()) {
			case R.id.onlineBanking:
				selectedId = 0;
				paymentmethod = "Online Banking";
				break;

			case R.id.creditCard:
				selectedId = 1;
				paymentmethod = "Credit Card";
				break;

			case R.id.payPal:
				selectedId = 2;
				paymentmethod = "Paypal";
				break;

			default:
				break;
			}

			if (!paymentmethod.equals("")) {
				GoogleAnalyticsLib.getInstance(getActivity()).trackEvent(
						"Confirm Purchase", paymentmethod);
				Bundle extra = new Bundle();
				extra.putInt("PAYMENT_METHOD_ID", selectedId);
				makePayment(extra);
			}else{
				Toast.makeText(getActivity(),
						"Please select your payment method.",
						Toast.LENGTH_SHORT).show();
			}
			return;

		case R.id.terms:
			Bundle params = new Bundle();
			params.putInt("TITLE_ID", R.string.TermNCondtionTitle);
			params.putInt("MSG_ID", R.string.tncLink);
			params.putInt("PAGE_ID", 110);
			checkoutView.openView(ViewName.TERM_CONDITION, params);
			break;
		}
	}

	private void makePayment(Bundle extra) {
		// if (checkoutEntity.get("QUOTE_ID") == null)
		// throw new Exception("No Quote id");
		//
		// if (extra.getInt("PAYMENT_METHOD_ID", -1) == -1)
		// throw new Exception("No payment method");

		checkoutEntity.put("PAYMENT_METHOD_ID",
				extra.getInt("PAYMENT_METHOD_ID", -1));

		// credit/debit card
		// online banking/direct debit
		// paypal - paypal_standard
		// molpay - molpay
		checkoutEntity
				.put("PAYMENT_METHOD", new String[] { "ipay88_16", "ipay88_2",
						"paypal_standard" }[(Integer) checkoutEntity.get(
						"PAYMENT_METHOD_ID", -1)]);

		Bundle param = new Bundle();
		param.putBoolean("CHECKOUT", true);
		param.putBoolean("LOADING", true);

		// if ((Double) checkoutEntity.get("GRAND_TOTAL", -1) < 0)
		// try {
		// throw new Exception("Grand total less than 0");
		// } catch (Exception e) {
		// e.printStackTrace();
		// }

		checkoutView.checkout(param, checkoutEntity);
	}

}
