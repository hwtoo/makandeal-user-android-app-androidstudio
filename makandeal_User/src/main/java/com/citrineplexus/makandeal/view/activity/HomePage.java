package com.citrineplexus.makandeal.view.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.citrineplexus.bello2.FilterAdapter;
import com.citrineplexus.bello2.ItemDisplayBinder;
import com.citrineplexus.library.gallery.GalleryLooping;
import com.citrineplexus.library.gcm.GCMRegisterHandler;
import com.citrineplexus.library.slideshow.OnSlideShowIitemClickListener;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.callback.OAuthListener;
import com.citrineplexus.makandeal.callback.OnDynamicListViewRefreshListener;
import com.citrineplexus.makandeal.callback.OnDynamicListViewRefreshListener.LoadResponseFlag;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.service.OAuthAuthorization;
import com.citrineplexus.makandeal.view.MakanDealApplication;
import com.citrineplexus.makandeal.view.activity.AbstractActivity.ViewName;
import com.citrineplexus.makandeal.view.widget.DynamicDataListView;

public class HomePage extends AbstractActivity implements OAuthListener,
		OnSlideShowIitemClickListener, OnClickListener,
		OnDynamicListViewRefreshListener, OnItemClickListener {

	private GalleryLooping viewPager;
	private FragmentManager fragementManager = null;

	private DynamicDataListView mListview = null;
	private ProgressBar mLoadingProgressBar = null;
	private int filterInt = -1000;
	private int CampaignView = -1;
	private String filterName = "All Deals";
	private TextView mNoContentMsg = null;
	private ImageView noContentImg = null;
	private TextView categoryButton = null;
	private final int homePageRequestCode = 111;
	private boolean isCampaignDeal = false;
	private String campaignType = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_page);
		GoogleAnalyticsLib.getInstance(this).trackView("Open App");

		if (getIntent() != null && getIntent().getExtras() != null) {

			Bundle bundle = getIntent().getExtras();
			if (bundle.getBoolean("GCM_MESSAGE"))
				GoogleAnalyticsLib.getInstance(this).trackNotification(
						"Notification", null);

			if (bundle.getBoolean("FROM_SPALSH")) {
				try {
					OAuthAuthorization.getInstance().setOAuthListener(this);
					OAuthAuthorization.getInstance().Login(
							this,
							MakanDealApplication.getInstance().getPreferences()
									.getString("TOKEN", null),
							MakanDealApplication.getInstance().getPreferences()
									.getString("SERCRET", null));
					if (MakanDealApplication.getInstance()
							.getNotificationFlag())
						new GCMRegisterHandler().registerApp(this);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (bundle.getBoolean("EXITAPP")) {
				this.finish();
				return;
			}
		}

		mNoContentMsg = (TextView) findViewById(R.id.noContentMsg);
		noContentImg = (ImageView) findViewById(R.id.noConteImage);
		mLoadingProgressBar = (ProgressBar) findViewById(R.id.loadingProgressBar);

		View emptyView = getLayoutInflater().inflate(
				R.layout.listview_empty_view, null);
		ListView listview = (ListView) findViewById(R.id.pullToRefreshID);
		// listview.setEmptyView(emptyView);
		SwipeRefreshLayout swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);

		FilterAdapter adapter = new FilterAdapter(this);
		// adapter.setDataSet(MakanDealApplication.getInstance()
		// .getDealArrayList());

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		mListview = new DynamicDataListView(this, this, listview, swipeLayout,
				adapter, new ItemDisplayBinder(), this, filterInt, size.x);

		categoryButton = (TextView) findViewById(R.id.categoryButton);
		categoryButton.setOnClickListener(this);
		categoryButton.setText(filterName);

		findViewById(R.id.filtersButton).setOnClickListener(this);

		fragementManager = getSupportFragmentManager();
		// initCategoryView();

		// TODO: get Subcategory Data, HIDE due to no deal
		// getServiceData(1051, 0, null, true);

		viewPager = mListview.getHeader();
		viewPager.setOnSlideShowIitemClickListener(this);
		Bundle eventBundle = new Bundle();
		eventBundle.putBoolean("Campaign_Event", true);
		getServiceData(1050, 0, eventBundle, false);

		Bundle promoBundle = new Bundle();
		promoBundle.putBoolean("Campaign_Event", false);
		getServiceData(1050, 0, promoBundle, false);

		if (adapter.isEmpty() && !isCampaignDeal) {
			initProductExt();
		} else {
			initEventExt();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		MakanDealApplication.getInstance().cleanUpData();
		// gridLayout.removeAllViews();
	}

	private void initEventExt() {
		mListview.setRequestData(true);
		mListview.setSwipeEnable(false);
		mListview.initScorllLIstener();
		Bundle localBundle = new Bundle();
		localBundle.putString("REQUEST_TYPE", "INITIAL");
		localBundle.putString("Campaign_Event", campaignType);
		localBundle.putString("Campaign_View", String.valueOf(CampaignView));
		localBundle.putString("PAGE", "1");
		getServiceData(1052, 0, localBundle, false);
	}

	private void initProductExt() {
		mListview.setRequestData(true);
		mListview.setSwipeEnable(false);
		mListview.initScorllLIstener();
		Bundle localBundle = new Bundle();
		localBundle.putString("REQUEST_TYPE", "INITIAL");
		localBundle.putString("PAGE", "1");
		getServiceData(1002, 0, localBundle, false);
	}

	private void initEventView(BaseEntity baseEntity) {
		if (baseEntity == null)
			return;
		if (baseEntity.get("JSONARRAY") != null) {
			ArrayList<BaseEntity> eventArray = (ArrayList<BaseEntity>) baseEntity
					.get("JSONARRAY");
			for (BaseEntity en : eventArray) {
				viewPager.addData((String) en.get("IMAGE_URL"));
				MakanDealApplication.getInstance().addEventList(en);
			}
		}
		viewPager.notifyDataChanged();
		// float density = getResources().getDisplayMetrics().densityDpi;
		// if (density > 240)
		// viewPager.setPadding(20, 0, 20, 0);
	}

	// private void initCategoryView() {
	// GridViewScrollable mGrideView = (GridViewScrollable)
	// findViewById(R.id.gridViewId);
	// final FilterPageAdapter adapter = new FilterPageAdapter(this,
	// R.layout.child_grideview_home_page, MakanDealApplication
	// .getInstance().getFilterAry(), false);
	// adapter.setChildType(FilterPageAdapter.GRIDVIEW_TYPE);
	// mGrideView.setAdapter(adapter);
	// mGrideView.setExpanded(true);
	// mGrideView.setOnItemClickListener(new OnItemClickListener() {
	// @Override
	// public void onItemClick(AdapterView<?> parent, View view,
	// int position, long id) {
	// FilterItemEntity entity = adapter.getItem(position);
	// Bundle localBundle = new Bundle();
	// localBundle.putString("FILTERNAME", entity.getTitle());
	// localBundle.putInt("FLITERBY", entity.getId());
	// openCategoryPage(localBundle);
	// }
	// });
	// }

	public void openCategoryPage(Bundle bundle) {
		openView(ViewName.CATEGORY_PAGE, bundle);
	}

	// private LinearLayout generate3ItemLayout() {
	// LinearLayout horizontalLayout = new LinearLayout(this);
	// LayoutParams parentParams = new LayoutParams(LayoutParams.MATCH_PARENT,
	// LayoutParams.WRAP_CONTENT);
	// horizontalLayout.setLayoutParams(parentParams);
	// horizontalLayout.setOrientation(LinearLayout.HORIZONTAL);
	// horizontalLayout.setWeightSum(2);
	//
	// BaseEntity promoEntity;
	//
	// promoEntity = MakanDealApplication.getInstance().getPromo(0);
	// LayoutParams firstParams = new LayoutParams(0,
	// LayoutParams.MATCH_PARENT);
	// firstParams.setMargins(0, 0, 5, 5);
	// firstParams.weight = 1;
	// ImageView m1stPromo = new ImageView(this);
	// m1stPromo.setId((Integer) promoEntity.get("PROMO_ID"));
	// m1stPromo.setLayoutParams(firstParams);
	// m1stPromo.setOnClickListener(this);
	// m1stPromo.setAdjustViewBounds(true);
	// ImageLoader.getInstance(this).DisplayImage(
	// (String) promoEntity.get("IMAGE_URL"), m1stPromo,
	// ScaleType.FIT_CENTER, false);
	// horizontalLayout.addView(m1stPromo);
	//
	// LayoutParams params = new LayoutParams(0, LayoutParams.MATCH_PARENT);
	// params.weight = 1;
	// LinearLayout verticalLayout = new LinearLayout(this);
	// verticalLayout.setOrientation(LinearLayout.VERTICAL);
	// verticalLayout.setLayoutParams(params);
	// verticalLayout.setWeightSum(2);
	//
	// LayoutParams imagParam = new LayoutParams(LayoutParams.MATCH_PARENT, 0);
	// imagParam.setMargins(0, 0, 0, 3);
	// imagParam.weight = 1;
	//
	// promoEntity = MakanDealApplication.getInstance().getPromo(1);
	// ImageView m2ndPromo = new ImageView(this);
	// m2ndPromo.setLayoutParams(imagParam);
	// m2ndPromo.setId((Integer) promoEntity.get("PROMO_ID"));
	// m2ndPromo.setOnClickListener(this);
	// m2ndPromo.setAdjustViewBounds(true);
	// ImageLoader.getInstance(this).DisplayImage(
	// (String) promoEntity.get("IMAGE_URL"), m2ndPromo,
	// ScaleType.FIT_CENTER, false);
	// verticalLayout.addView(m2ndPromo);
	//
	// LayoutParams imagParam2 = new LayoutParams(LayoutParams.MATCH_PARENT, 0);
	// imagParam2.setMargins(0, 2, 0, 5);
	// imagParam2.weight = 1;
	// promoEntity = MakanDealApplication.getInstance().getPromo(2);
	// ImageView m3thPromo = new ImageView(this);
	// m3thPromo.setLayoutParams(imagParam2);
	// m3thPromo.setId((Integer) promoEntity.get("PROMO_ID"));
	// m3thPromo.setOnClickListener(this);
	// m3thPromo.setAdjustViewBounds(true);
	// ImageLoader.getInstance(this).DisplayImage(
	// (String) promoEntity.get("IMAGE_URL"), m3thPromo,
	// ScaleType.FIT_CENTER, false);
	// verticalLayout.addView(m3thPromo);
	//
	// horizontalLayout.addView(verticalLayout);
	// return horizontalLayout;
	// }

	// private LinearLayout generateTwoItemLayout(BaseEntity entityLeft,
	// BaseEntity entityRight) {
	// LinearLayout horizontalLayout = new LinearLayout(this);
	// horizontalLayout.setWeightSum(2);
	// horizontalLayout.setOrientation(LinearLayout.HORIZONTAL);
	//
	// LayoutParams params = new LayoutParams(0, LayoutParams.WRAP_CONTENT);
	// params.setMargins(0, 0, 5, 5);
	// params.weight = 1;
	//
	// ImageView leftImage = new ImageView(this);
	// leftImage.setLayoutParams(params);
	// leftImage.setId((Integer) entityLeft.get("PROMO_ID"));
	// ImageLoader.getInstance(this).DisplayImage(
	// (String) entityLeft.get("IMAGE_URL"), leftImage,
	// ScaleType.FIT_CENTER, false);
	// leftImage.setOnClickListener(this);
	// leftImage.setAdjustViewBounds(true);
	// horizontalLayout.addView(leftImage);
	//
	// LayoutParams paramsRight = new LayoutParams(0,
	// LayoutParams.WRAP_CONTENT);
	// paramsRight.weight = 1;
	//
	// ImageView rightImage = new ImageView(this);
	// rightImage.setLayoutParams(paramsRight);
	// rightImage.setId((Integer) entityRight.get("PROMO_ID"));
	// ImageLoader.getInstance(this).DisplayImage(
	// (String) entityRight.get("IMAGE_URL"), rightImage,
	// ScaleType.FIT_CENTER, false);
	// rightImage.setOnClickListener(this);
	// rightImage.setAdjustViewBounds(true);
	// horizontalLayout.addView(rightImage);
	//
	// return horizontalLayout;
	// }

	// private void initPromoPage(BaseEntity baseEntity) {
	// if (baseEntity == null)
	// return;
	// if (baseEntity.get("JSONARRAY") != null) {
	// ArrayList<BaseEntity> eventArray = (ArrayList<BaseEntity>) baseEntity
	// .get("JSONARRAY");
	// for (int i = 0; i < eventArray.size(); i++) {
	// BaseEntity en = eventArray.get(i);
	// en.put("PROMO_ID", i);
	// MakanDealApplication.getInstance().addPromoList(en);
	// }
	// }
	// // gridLayout.setPadding(10, 10, 10, 10);
	// if (gridLayout.getChildCount() > 0)
	// gridLayout.removeAllViewsInLayout();
	// if (MakanDealApplication.getInstance().getPromoList().size() >= 3)
	// gridLayout.addView(generate3ItemLayout());
	//
	// ArrayList<BaseEntity> promoList = MakanDealApplication.getInstance()
	// .getPromoList();
	// if ((promoList.size() - 3) % 2 == 0) {
	// for (int i = 3; i < promoList.size();) {
	// gridLayout.addView(generateTwoItemLayout(promoList.get(i),
	// promoList.get((i + 1))));
	// i = i + 2;
	// }
	// }
	// }

	// private void initSubCategory(BaseEntity baseEntity) {
	// if (baseEntity == null)
	// return;
	// if (baseEntity.get("JSONARRAY") != null) {
	// ArrayList<BaseEntity> eventArray = (ArrayList<BaseEntity>) baseEntity
	// .get("JSONARRAY");
	// for (BaseEntity en : eventArray) {
	// MakanDealApplication.getInstance().addSubcategroy(en);
	// }
	// }
	//
	// // LinearLayout filteredCategoryLayout = (LinearLayout)
	// // findViewById(R.id.filteredCategoryLayout);
	// FragmentTransaction ft = fragementManager.beginTransaction();
	// ArrayList<BaseEntity> subCategoryList = MakanDealApplication
	// .getInstance().getSubcategoryList();
	// int subCategorySize = subCategoryList.size();
	// for (int i = 0; i < subCategorySize; i++) {
	// ft.add(R.id.filteredCategoryLayout,
	// FilteredCategoryFragment.newInstance(i), null);
	// }
	// ft.commit();
	// }

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_UNLOCKED;
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		Message msg = new Message();
		switch (position) {
		case 1050:
			if (params != null && params.get("Campaign_Event") != null) {
				// if (params.getBoolean("Campaign_Event"))
				initEventView(entity);
				// else
				// initPromoPage(entity);
			}
			break;

		case 1051:
			super.onAquireResult(entity, position, params, true);
			msg.what = 101;
			msg.obj = entity;
			handler.sendMessage(msg);
			break;

		case 1001:// Refresh All Data
			mListview.clearData();

		case 1002:// Additional Data
		case 1052:// Get Campign Deal
			if (entity == null || params == null)
				return;
			if (params.getString("REQUEST_TYPE").equals("INITIAL"))
				msg.what = 1001;
			else if (params.getString("REQUEST_TYPE").equals("ADDITIONAL"))
				msg.what = 1002;
			msg.obj = entity;
			handler.sendMessage(msg);
			break;

		default:
			break;
		}

		// if (position == 1050) {
		// if (params != null && params.get("Campaign_Event") != null) {
		// // if (params.getBoolean("Campaign_Event"))
		// initEventView(entity);
		// // else
		// // initPromoPage(entity);
		// }
		// } else if (position == 1051) {
		// super.onAquireResult(entity, position, params, true);
		// Message msg = new Message();
		// msg.what = 101;
		// msg.obj = entity;
		// handler.sendMessage(msg);
		// } else if (position == 1002) {
		// if (entity instanceof BaseEntity) {
		// if (entity.get("JSONARRAY") != null) {
		// ArrayList<BaseEntity> dataAry = (ArrayList<BaseEntity>) entity
		// .get("JSONARRAY");
		// // if (!(isLastPage =
		// // mListview.checklastTime(dataAry
		// // .get(dataAry.size() - 1))))
		// int count = 0;
		// for (BaseEntity en : dataAry)
		// MakanDealApplication.getInstance().addDeal(en);
		// }
		// }
		// }
	}

	@Override
	void handlerMessage(Message msg) {
		// if (msg.what == 101) {
		// initSubCategory((BaseEntity) msg.obj);
		// }
		switch (msg.what) {
		case 1001:// Refresh All Data
			mListview.clearData();

		case 1002:// Additional Data
			if (msg.obj != null)
				if (msg.obj instanceof BaseEntity) {
					if (((BaseEntity) msg.obj).get("JSONARRAY") != null) {

						ArrayList<BaseEntity> dataAry = (ArrayList<BaseEntity>) ((BaseEntity) msg.obj)
								.get("JSONARRAY");
						// if (!(isLastPage =
						// mListview.checklastTime(dataAry
						// .get(dataAry.size() - 1))))
						for (BaseEntity en : dataAry) {
							mListview.addData(en, isCampaignDeal);
						}

					}
				} else if (msg.obj instanceof ArrayList<?>) {
					mListview.addData((ArrayList<BaseEntity>) msg.obj, null,
							isCampaignDeal);
				}
			mListview.notifyDataSetChanged();
			break;

		case 1003:
			if (msg.obj instanceof LoadResponseFlag)
				onLoadResponse((LoadResponseFlag) msg.obj);
			break;

		}
	}

	@Override
	public void returnMessage(int id, Object obj) {
		if (id == -9999 && obj instanceof BaseEntity) {
			BaseEntity msgEntity = (BaseEntity) obj;
			if (msgEntity.get("ERROR") != null) {
				ArrayList<BaseEntity> errorAry = (ArrayList<BaseEntity>) msgEntity
						.get("ERROR");
				BaseEntity errorEntity = (BaseEntity) errorAry.get(0);
				if (errorEntity.get("MESSAGE") != null) {
					Toast.makeText(this, "Invalid email or password.",
							Toast.LENGTH_SHORT).show();
				}
			}
		}
	}

	@Override
	public void oauthReady(boolean status, Bundle extra) {
		OAuthAuthorization.getInstance().getUserProfile(this, false);
	}

	@Override
	public void onAcquireProfile() {
		updateMenu();
		setMenuChecked(drawerID());
		try {
			new GCMRegisterHandler().updateDeviceIDwithProfile(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.ALL;
	}

	@Override
	int drawerID() {
		return 102;
	}

	@Override
	public void onClick(View v) {
		Bundle bundle = new Bundle();
		switch (v.getId()) {
		case R.id.filtersButton:
			bundle.putInt("REQUEST_CODE", homePageRequestCode);
			openView(ViewName.FILTER, bundle);
			break;

		case R.id.categoryButton:
			bundle.putString("VIEWNAME", this.getClass().getName());
			bundle.putInt("REQUEST_CODE", homePageRequestCode);
			openView(ViewName.CATEGORY_PAGE, bundle);
			break;

		default:
			// int promoId = v.getId();
			// BaseEntity promo = MakanDealApplication.getInstance().getPromo(
			// promoId);
			// handleEventAndPromoLogic(promo);
			break;
		}

	}

	@Override
	public void onSlideItemClick(View v, int position) {
		BaseEntity event = MakanDealApplication.getInstance()
				.getEvent(position);
		Intent mIntent = handleEventAndPromoLogic2(event);
		if (mIntent != null)
			loadCampaign(mIntent.getExtras());
	}

	private void loadCampaign(Bundle bundle) {
		if (bundle.get("CAMPAIGN_NAME") != null) {
			filterName = bundle.getString("CAMPAIGN_NAME");
			campaignType = bundle.getString("CAMPAIGN_EVENT");
			filterInt = -1000;
			CampaignView = bundle.getInt("CAMPAIGN_VIEW");
			isCampaignDeal = true;
		}
		mLoadingProgressBar.setVisibility(View.VISIBLE);
		mNoContentMsg.setVisibility(View.GONE);
		noContentImg.setVisibility(View.GONE);
		mListview.clearData();
		mListview.setFilter(filterInt);
		mListview.getAdatper().notifyDataSetChanged();
		initEventExt();
		categoryButton.setText(filterName);
	}

	@Override
	public void onItemClick(View v, int position) {
		// TODO: Slider Onclick
	}

	@Override
	public void onRefresh() {
		if (mListview != null && !mListview.isRequestingData()
				&& !isCampaignDeal) {
			MakanDealApplication.lastRequestPage = 1;
			initProductExt();
		} else
			initEventExt();
	}

	private void getContent(int count) {
		if (!mListview.isRequestingData() && /* !isLastPage */
		MakanDealApplication.lastRequestPage != (1 + count / 50)) {
			mListview.setRequestData(true);
			Bundle localBundle = new Bundle();
			localBundle.putString("REQUEST_TYPE", "ADDITIONAL");

			MakanDealApplication.lastRequestPage = (1 + count / 50);
			localBundle.putString("PAGE", String.valueOf((1 + count / 50)));

			getServiceData(1002, 0, localBundle, false);
		} else {
			if (mListview.isEmpty()) {
				Message msg = new Message();
				msg.what = 1003;
				msg.obj = LoadResponseFlag.NO_CONTENT;
				handler.sendMessage(msg);
			}
			mListview.removeFooter();
		}
	}

	private int pageCount = 1;

	private void getCampaign(int count) {

		if (!mListview.isRequestingData() && pageCount != (1 + count / 50)) {
			mListview.setRequestData(true);
			Bundle localBundle = new Bundle();
			localBundle.putString("REQUEST_TYPE", "ADDITIONAL");

			pageCount = (1 + count / 50);
			localBundle.putString("PAGE", String.valueOf((1 + count / 50)));
			getServiceData(1052, 0, localBundle, false);
		} else {
			if (mListview.isEmpty()) {
				Message msg = new Message();
				msg.what = 1003;
				msg.obj = LoadResponseFlag.NO_CONTENT;
				handler.sendMessage(msg);
			}
			mListview.removeFooter();
		}
	}

	@Override
	public void onLoadMore() {
		int count = mListview.getCount();

		if (isCampaignDeal)
			getContent(count);
		else
			getCampaign(count);

		// if (!mListview.isRequestingData() && /* !isLastPage */
		// MakanDealApplication.lastRequestPage != (1 + count / 50)) {
		// mListview.setRequestData(true);
		// Bundle localBundle = new Bundle();
		// localBundle.putString("REQUEST_TYPE", "ADDITIONAL");
		//
		// MakanDealApplication.lastRequestPage = (1 + count / 50);
		// localBundle.putString("PAGE", String.valueOf((1 + count / 50)));
		// if (isCampaignDeal)
		// getServiceData(1052, 0, localBundle, false);
		// else
		// getServiceData(1002, 0, localBundle, false);
		// } else {
		// if (mListview.isEmpty()) {
		// Message msg = new Message();
		// msg.what = 1003;
		// msg.obj = LoadResponseFlag.NO_CONTENT;
		// handler.sendMessage(msg);
		// }
		// mListview.removeFooter();
		// }
	}

	@Override
	public void onLoadResponse(LoadResponseFlag flag) {
		mLoadingProgressBar.setVisibility(View.GONE);
		switch (flag) {
		case HAVE_CONTENT:
			mNoContentMsg.setVisibility(View.GONE);
			noContentImg.setVisibility(View.GONE);
			mListview.setVisible(View.VISIBLE);
			break;

		case NO_CONNECTION:
			mNoContentMsg.setText(R.string.noConnectionMsg);
			noContentImg.setVisibility(View.GONE);

		case NO_CONTENT:
			mNoContentMsg.setVisibility(View.VISIBLE);
			noContentImg.setVisibility(View.VISIBLE);
			// mListview.setVisible(View.GONE);
			break;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		BaseEntity entity = (BaseEntity) mListview.getAdatper().getItem(
				position);

		if (entity.get("ENTITY_ID") != null) {
			Bundle param = new Bundle();
			param.putString("ENTITY_ID", entity.get("ENTITY_ID").toString());
			param.putString("VENDOR_ID", entity.get("UDROPSHIP_VENDOR")
					.toString());
			openView(ViewName.DEAL_PAGE, param);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		if (resultCode == RESULT_OK && requestCode == homePageRequestCode
				&& intent != null && intent.getExtras() != null) {
			Bundle bundle = intent.getExtras();

			if (bundle.getBoolean("FROM_FILTER")) {
				mListview.notifyDataSetChanged();
			} else if (!bundle.getBoolean("ISCAMPAIGN")) {
				isCampaignDeal = false;
				filterName = bundle.getString("FILTERNAME");
				GoogleAnalyticsLib.getInstance(this).trackView(filterName);
				filterInt = bundle.getInt("FLITERBY");
				mListview.clearData();
				mListview.addData(MakanDealApplication.getInstance()
						.getDealArrayList(), null, false);
				mListview.filter(filterInt, true);
				mListview.notifyDataSetChanged();
				categoryButton.setText(filterName);
			} else {
				loadCampaign(bundle);
			}
		} else
			super.onActivityResult(resultCode, resultCode, intent);
	}

	// private void initProductExt() {
	// Bundle localBundle = new Bundle();
	// localBundle.putString("REQUEST_TYPE", "INITIAL");
	// localBundle.putString("PAGE", "1");
	// getServiceData(1002, 0, localBundle, false);
	// }

}
