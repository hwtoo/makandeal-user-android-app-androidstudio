package com.citrineplexus.makandeal.view.activity;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.AbstractRecyclerAdapter.OnListAdapterItemClickListener;
import com.citrineplexus.makandeal.adapter.AdapterBinder;
import com.citrineplexus.makandeal.adapter.ListAdapter;
import com.citrineplexus.makandeal.binder.WishBinder;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;

public class WishlistPage extends DefaultRecyclerListView implements
		OnListAdapterItemClickListener {

	private TextView emptyTextView = null;
	private WishBinder wishBinder = null;
	private final int CONTENT_ID = 1010;
	private final String title = "Wish List";

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.edit_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_edit:
			if (!getListAdapter().isEmpty()) {
				wishBinder.openDeleteLayout();
				getListAdapter().notifyDataSetChanged();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	int getContentView() {
		return R.layout.recycler_listview;
	}

	@Override
	ListAdapter initListAdapter() {
		return new ListAdapter(this);
	}

	@Override
	String getPageTitle() {
		return title;
	}

	@Override
	AdapterBinder getBinder() {
		wishBinder = new WishBinder(this, this);
		return wishBinder;
	}

	@Override
	int getContentId() {
		return CONTENT_ID;
	}

	@Override
	void onInit() {
		emptyTextView = (TextView) findViewById(R.id.emptyView);
		GoogleAnalyticsLib.getInstance(this).trackView(title);
	}

	@Override
	void getServiceData(int contentID) {
		Bundle localBundle = new Bundle();
		localBundle.putString("REQUEST_TYPE", "INITIAL");
		getServiceData(contentID, 0, localBundle, true);
	}

	@Override
	String getUniqueKey() {
		return null;
	}

	@Override
	void adapterEmptyHandler(boolean adapterIsEmpty, RecyclerView recyclerView) {
		if (adapterIsEmpty) {
			recyclerView.setVisibility(View.GONE);
			if (emptyTextView != null) {
				emptyTextView
						.setText("You have not added any services into wishlist.");
				emptyTextView.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_UNLOCKED;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.EDIT_HEADER;
	}

	@Override
	int drawerID() {
		return 105;
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		super.onAquireResult(entity, position, params, true);
	}

	@Override
	public void onBackPressed() {
		if (!getListAdapter().isEmpty() && wishBinder.getRemoveFlag()) {
			wishBinder.openDeleteLayout();
			getListAdapter().notifyDataSetChanged();
		} else
			super.onBackPressed();
	}

	@Override
	public void onItemClickListener(View view, int position) {
		switch (view.getId()) {
		case R.id.deleteBtn:
			if (getListAdapter().getData(position) == null)
				return;
			BaseEntity en = (BaseEntity) getListAdapter().getData(position);
			Bundle localBundle = new Bundle();
			localBundle.putString("REQUEST_TYPE", "REMOVE");
			localBundle.putString("ENTITY_ID", en.get("WISHLIST_ITEM_ID", "")
					.toString());
			getServiceData(1011, position, localBundle, true);
			getListAdapter().removeData(position);
			break;

		default:
			BaseEntity entity = getListAdapter().getData(position);
			if (entity.get("ENTITY_ID") != null) {
				Bundle param = new Bundle();
				param.putString("ENTITY_ID", entity.get("ENTITY_ID").toString());
				openView(ViewName.DEAL_PAGE, param);
			}
			break;
		}
	}

	@Override
	public void onRefresh() {
		Bundle localBundle = new Bundle();
		localBundle.putString("REQUEST_TYPE", "INITIAL");
		getServiceData(CONTENT_ID, 0, localBundle, false);
	}
}
