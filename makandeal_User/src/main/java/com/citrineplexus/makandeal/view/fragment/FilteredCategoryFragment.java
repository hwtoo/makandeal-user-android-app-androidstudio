//package com.citrineplexus.makandeal.view.fragment;
//
//import java.util.ArrayList;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.TextView;
//
//import com.citrineplexus.makandeal.R;
//import com.citrineplexus.makandeal.adapter.FilterPageAdapter;
//import com.citrineplexus.makandeal.entity.BaseEntity;
//import com.citrineplexus.makandeal.entity.FilterItemEntity;
//import com.citrineplexus.makandeal.view.MakanDealApplication;
//import com.citrineplexus.makandeal.view.activity.HomePage;
//import com.citrineplexus.makandeal.view.widget.GridViewScrollable;
//
//public class FilteredCategoryFragment extends Fragment {
//
//	private GridViewScrollable gridView = null;
//	private TextView title = null;
//	private FilterItemEntity[] filteredCategoryAry = null;
//	private FilterPageAdapter adapter;
//
//	// String[] titleAry = { "sashimi", "teppanyaki", "shabu shabu", "sushi",
//	// "ramen", "dessert" };
//	// int[] idAry = { 101, 102, 103, 104, 105, 106 };
//	private BaseEntity subEntity = null;
//
//	@Override
//	public View onCreateView(LayoutInflater inflater,
//			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//		View view = inflater.inflate(R.layout.fragment_filtered_category,
//				container, false);
//		gridView = (GridViewScrollable) view
//				.findViewById(R.id.filteredCategoryGridView);
//		title = (TextView) view.findViewById(R.id.filteredCategoryTitle);
//
//		initData();
//
//		return view;
//	}
//
//	private void initData() {
//		if (getArguments() == null)
//			return;
//		Bundle bundle = getArguments();
//		// POSITION
//		subEntity = MakanDealApplication.getInstance().getSubcategory(
//				bundle.getInt("POSITION"));
//		title.setText((String) subEntity.get("TITLE"));
//
//		ArrayList<BaseEntity> categoryAry = (ArrayList<BaseEntity>) subEntity
//				.get("CATEGORY");
//
//		filteredCategoryAry = new FilterItemEntity[categoryAry.size()];
//		for (int i = 0; i < categoryAry.size(); i++) {
//			BaseEntity categoryEntity = categoryAry.get(i);
//			filteredCategoryAry[i] = new FilterItemEntity(
//					(String) categoryEntity.get("IMAGE_URL"),
//					(String) categoryEntity.get("CATEGORY_NAME"),
//					Integer.parseInt((String) categoryEntity.get("FILTER_ID")));
//		}
//
//		adapter = new FilterPageAdapter(getActivity(),
//				R.layout.child_grideview_home_page, filteredCategoryAry, true);
//		adapter.setChildType(FilterPageAdapter.GRIDVIEW_TYPE);
//		gridView.setAdapter(adapter);
//		gridView.setExpanded(true);
//
//		gridView.setOnItemClickListener(new OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view,
//					int position, long id) {
//				FilterItemEntity entity = adapter.getItem(position);
//				Bundle localBundle = new Bundle();
//				localBundle.putString("FILTERNAME", entity.getTitle());
//				localBundle.putInt("FLITERBY", entity.getId());
//				if (getActivity() instanceof HomePage) {
//					((HomePage) getActivity()).openCategoryPage(localBundle);
//				}
//			}
//		});
//	}
//
//	public static FilteredCategoryFragment newInstance(int position) {
//		FilteredCategoryFragment f = new FilteredCategoryFragment();
//		Bundle args = new Bundle();
//		args.putInt("POSITION", position);
//		f.setArguments(args);
//		return f;
//	}
//}
