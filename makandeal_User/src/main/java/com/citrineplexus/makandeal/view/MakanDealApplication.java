package com.citrineplexus.makandeal.view;

import java.util.ArrayList;
import java.util.List;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.TypedArray;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.BaseColumns;
import android.widget.Toast;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.SearchSuggestionAdapter;
import com.citrineplexus.makandeal.callback.OnRefreshCartListener;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.entity.FilterItemEntity;
import com.citrineplexus.makandeal.entity.NavDrawerItem;
import com.citrineplexus.makandeal.entity.NavDrawerItemSeparator;
import com.citrineplexus.makandeal.entity.NavDrawerItemWithIcon;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.service.OAuthAuthorization;
import com.citrineplexus.makandeal.service.ServiceGateway;
import com.citrineplexus.makandeal.util.GlobalService;
import com.facebook.FacebookSdk;

public class MakanDealApplication extends Application implements
		OnDataRequestListener {
	private static MakanDealApplication _instance;
	private String googleApiKey = "";
	private boolean notificationFlag = true;

	private SharedPreferences prefs = null;
	private boolean isLogin = false;
	// private OnRefreshCartListener listener = null;
	private ArrayList<OnRefreshCartListener> listenerAry = new ArrayList<OnRefreshCartListener>();
	// private OnRefreshCartListener selectDealListener = null;
	private int cartItemNumber = 0;

	private List<String> termsList = null;
	private List<BaseEntity> cartItem = new ArrayList<BaseEntity>();
	private FilterItemEntity[] filterAry = null;
	private ArrayList<BaseEntity> dealArrayList = new ArrayList<BaseEntity>();
	private ArrayList<BaseEntity> mEventList = new ArrayList<BaseEntity>();
	private ArrayList<BaseEntity> mPromoList = new ArrayList<BaseEntity>();
	private ArrayList<BaseEntity> mSubCategoryList = new ArrayList<BaseEntity>();
	private ArrayList<BaseEntity> mFilterPriceList = new ArrayList<BaseEntity>();
	private ArrayList<BaseEntity> mFilterRegionList = new ArrayList<BaseEntity>();

	private BaseEntity checkoutEntity = null;

	private SearchSuggestionAdapter mAdapter;
	public static final String cursorTag = "DealName";

	public static int lastRequestPage = 1;
	public static final NavDrawerItem[] offlineMenu = new NavDrawerItem[] {
			new NavDrawerItemWithIcon(100, R.drawable.navitem_signin,
					"Sign In", false, 0),
			new NavDrawerItemWithIcon(101, R.drawable.navitem_signup,
					"Sign Up", false, 0),
			new NavDrawerItemSeparator(-1),
			new NavDrawerItemWithIcon(102, R.drawable.navitem_homepage, "Home",
					false, 0),
			new NavDrawerItemWithIcon(103, R.drawable.navitem_promotion,
					"Promotions", false, 0),
			new NavDrawerItemWithIcon(116, R.drawable.navitem_blog,
					"Makandeal Blog", false, 0),
			new NavDrawerItemSeparator(-1), new NavDrawerItemSeparator(-1),
			new NavDrawerItemWithIcon(114, -1, "Notification", true, 2),
			new NavDrawerItemSeparator(-1),
			new NavDrawerItemWithIcon(109, -1, "FAQ", false, 1),
			new NavDrawerItemWithIcon(110, -1, "Terms & Conditions", false, 1),
			new NavDrawerItemWithIcon(111, -1, "Privacy Policy", false, 1),
			new NavDrawerItemWithIcon(112, -1, "Contact Us", false, 1) };

	public static final NavDrawerItem[] onlineMenu = new NavDrawerItem[] {
			new NavDrawerItemWithIcon(102, R.drawable.navitem_homepage, "Home",
					false, 0),
			new NavDrawerItemWithIcon(103, R.drawable.navitem_promotion,
					"Promotions", false, 0),
			new NavDrawerItemWithIcon(116, R.drawable.navitem_blog,
					"Makandeal Blog", false, 0),
			new NavDrawerItemSeparator(-1),
			new NavDrawerItemWithIcon(104, R.drawable.navitem_voucher,
					"Voucher", false, 0),
			new NavDrawerItemWithIcon(105, R.drawable.navitem_wishlist,
					"Wish List", false, 0),
			new NavDrawerItemWithIcon(106, R.drawable.navitem_credit,
					"My Credit", false, 0),
			new NavDrawerItemWithIcon(107, R.drawable.navitem_earnrm2,
					"Earn RM2", false, 0), new NavDrawerItemSeparator(-1),
			new NavDrawerItemWithIcon(108, -1, "Passcode", true, 2),
			new NavDrawerItemWithIcon(114, -1, "Notification", true, 2),
			new NavDrawerItemSeparator(-1),
			new NavDrawerItemWithIcon(109, -1, "FAQ", false, 1),
			new NavDrawerItemWithIcon(110, -1, "Terms & Conditions", false, 1),
			new NavDrawerItemWithIcon(111, -1, "Privacy Policy", false, 1),
			new NavDrawerItemWithIcon(112, -1, "Contact Us", false, 1),
			new NavDrawerItemSeparator(-1),
			new NavDrawerItemWithIcon(113, -1, "Sign Out", false, 1) };

	public static MakanDealApplication getInstance() {
		return _instance;
	}

	public void setNotificationFlag() {
		this.notificationFlag = !notificationFlag;
		prefs.edit().putBoolean("NotificationFlag", notificationFlag).commit();
	}

	public boolean getNotificationFlag() {
		return notificationFlag;
	}

	public void cleanUpData() {
		if (mAdapter != null && !mAdapter.isEmpty())
			mAdapter.cleanUp();

		if (termsList != null && !termsList.isEmpty()) {
			termsList.clear();
			// termsList = null;
		}
		if (cartItem != null && !cartItem.isEmpty()) {
			cartItem.clear();
			// cartItem = null;
		}
		if (dealArrayList != null && !dealArrayList.isEmpty()) {
			dealArrayList.clear();
			// dealArrayList = null;
		}
		if (mEventList != null && !mEventList.isEmpty()) {
			mEventList.clear();
			// mEventList = null;
		}
		if (mPromoList != null && !mPromoList.isEmpty()) {
			mPromoList.clear();
			// mPromoList = null;
		}
		if (mSubCategoryList != null && !mSubCategoryList.isEmpty()) {
			mSubCategoryList.clear();
			// mSubCategoryList = null;
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();
		initSingleton();
		// TODO: old Facebook SDK
		// AppEventsLogger.activateApp(this,
		// getResources().getString(R.string.facebookAppID));

		try {
			ApplicationInfo ai = getPackageManager().getApplicationInfo(
					getPackageName(), PackageManager.GET_META_DATA);
			Bundle bundle = ai.metaData;
			if (bundle != null
					&& bundle.get("com.google.android.maps.v2.API_KEY") != null)
				googleApiKey = bundle
						.getString("com.google.android.maps.v2.API_KEY");
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		String[] stringArray = getResources().getStringArray(R.array.region);
		int[] intArray = getResources().getIntArray(R.array.region_id);

		for (int i = 0; i < stringArray.length; i++) {
			BaseEntity en = new BaseEntity();
			en.put("TEXT", stringArray[i]);
			en.setFilterID(intArray[i]);
			mFilterRegionList.add(en);
		}

		String[] priceArray = getResources().getStringArray(
				R.array.filter_price_ary);
		int[] priceIdArray = getResources().getIntArray(R.array.price_id);
		for (int i = 0; i < priceArray.length; i++) {
			BaseEntity en = new BaseEntity();
			en.put("TEXT", priceArray[i]);
			en.setFilterID(priceIdArray[i]);
			mFilterPriceList.add(en);
		}
	}

	public void setFilterPriceCategoryIsAvailable(int priceID) {
		for (BaseEntity en : mFilterPriceList) {
			if (!((boolean) en.get("ISSHOWING", false))
					&& en.getFilterID() == priceID) {
				en.put("ISSHOWING", true);
			}
		}
	}

	public List<BaseEntity> getFilterRegionList() {
		return this.mFilterRegionList;
	}

	public List<BaseEntity> getFilterPriceList() {
		return this.mFilterPriceList;
	}

	private void initSingleton() {
		_instance = this;
		getPreferences();
		OAuthAuthorization.getInstance();
		GoogleAnalyticsLib.getInstance(this);
		ImageLoader.getInstance(this);
		GlobalService.checkLive(this);
		initCategoryItem();
		initTermList();
		notificationFlag = MakanDealApplication.getInstance().getPreferences()
				.getBoolean("NotificationFlag", notificationFlag);
		// TODO: new Facebook SDK
		FacebookSdk.sdkInitialize(getApplicationContext());
	}

	public FilterItemEntity[] getFilterAry() {
		if (filterAry != null)
			return filterAry;
		return null;
	}

	public ArrayList<BaseEntity> getDealArrayList() {
		return dealArrayList;
	}

	public void addDeal(BaseEntity entity) {
		dealArrayList.add(entity);
	}

	public void addDeal(List<BaseEntity> entities) {
		dealArrayList.addAll(entities);
	}

	public void addEventList(BaseEntity campaign) {
		this.mEventList.add(campaign);
	}

	public BaseEntity getEvent(int position) {
		int eventPosition = position % mEventList.size();
		return this.mEventList.get(eventPosition);
	}

	public List<BaseEntity> getEventListing() {
		return this.mEventList;
	}

	public void addPromoList(BaseEntity campaign) {
		this.mPromoList.add(campaign);
	}

	public BaseEntity getPromo(int position) {
		return this.mPromoList.get(position);
	}

	public List<BaseEntity> getPromoList() {
		return this.mPromoList;
	}

	public void addSubcategroy(BaseEntity campaign) {
		this.mSubCategoryList.add(campaign);
	}

	public BaseEntity getSubcategory(int position) {
		return this.mSubCategoryList.get(position);
	}

	public ArrayList<BaseEntity> getSubcategoryList() {
		return this.mSubCategoryList;
	}

	private void initCategoryItem() {
		TypedArray typeAryicon = getResources().obtainTypedArray(
				R.array.fliterIcon);
		String[] typeTitle = getResources().getStringArray(R.array.fliterType);
		int[] typeId = getResources().getIntArray(R.array.filterTypeID);
		// int[] typeIcon = getResources().getIntArray(R.array.fliterIcon);

		filterAry = new FilterItemEntity[typeId.length];
		for (int i = 0; i < typeId.length; i++) {
			filterAry[i] = new FilterItemEntity(
					typeAryicon.getResourceId(i, -1), typeTitle[i], typeId[i]);
		}
		typeAryicon.recycle();
	}

	private void initTermList() {
		if (termsList == null) {
			termsList = new ArrayList<String>();
			final String[] from = new String[] { BaseColumns._ID, cursorTag };
			MatrixCursor cursor = new MatrixCursor(from);
			mAdapter = new SearchSuggestionAdapter(this, cursor, false);
		}

		if (termsList.isEmpty()) {
			new ServiceGateway().getData(this, 1032, this, 0, null);
		}
	}

	public SearchSuggestionAdapter getmAdapter() {
		return mAdapter;
	}

	public List<String> getTermsList() {
		return termsList;
	}

	public String getGoogleApiKey() {
		return this.googleApiKey;
	}

	public boolean isUserLogin() {
		return isLogin;
	}

	public void userIsLogin() {
		isLogin = true;
	}

	public SharedPreferences getPreferences() {
		if (prefs == null)
			prefs = getSharedPreferences(
					getApplicationContext().getString(R.string.app_name),
					MODE_PRIVATE);
		return prefs;
	}

	public void storeTokenAndSercret(String token, String sercret) {
		if (prefs == null)
			getPreferences();
		Editor editor = prefs.edit();
		if (prefs.getString("TOKEN", null) != null
				&& prefs.getString("SERCRET", null) != null) {
			editor.remove("TOKEN");
			editor.remove("SERCRET");
		}
		editor.putString("TOKEN", token);
		editor.commit();
		editor.putString("SERCRET", sercret);
		editor.commit();
	}

	public void requestUserCartItems() {
		Bundle param = new Bundle();
		param.putBoolean("CART_ITEMS", true);
		new ServiceGateway().getData(this, 1020, this, 0, param);
		// ViewController.getInstance(context)
		// .getServiceData(1020, this, 0, param);
	}

	public void setCartItemNumber(int num) {
		if (num == 0)
			cartItem = null;
		cartItemNumber = num;

		// if (listener != null)
		// listener.onRefreshCart(cartItemNumber);
		for (OnRefreshCartListener listener : listenerAry)
			listener.onRefreshCart(cartItemNumber);
	}

	// public void setCartListener(OnRefreshCartListener
	// paramRefreshCartListener) {
	// listener = paramRefreshCartListener;
	// }

	public void addCartListener(OnRefreshCartListener paramRefreshCartListener) {
		if (!listenerAry.contains(paramRefreshCartListener))
			listenerAry.add(paramRefreshCartListener);
	}

	// public void setSelectDealListener(OnRefreshCartListener
	// selectDealListener) {
	// this.selectDealListener = selectDealListener;
	// }

	public int getCartItemNumber() {
		return cartItemNumber;
	}

	public List<BaseEntity> getCartItem() {
		return cartItem;
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {

		if (position == 1032) {
			if (entity == null)
				return;
			if (entity.get("JSONARRAY") != null)
				if (entity.get("JSONARRAY") instanceof ArrayList<?>) {
					for (BaseEntity en : (ArrayList<BaseEntity>) entity
							.get("JSONARRAY")) {
						int num_results = Integer.valueOf((String) en
								.get("NUM_RESULTS"));

						if (num_results > 0) {
							// mAdapter.addData((String) en.get("QUERY_TEXT"));
							termsList.add((String) en.get("QUERY_TEXT"));
						}
					}

					if (mAdapter != null)
						mAdapter.notifyDataSetChanged();
				}
		}

		if (OAuthAuthorization.getInstance().getUserProfileData() != null) {
			if (position == 1020 || position == 1042) {
				if (position == 1042
						&& params.getBoolean("CHECK_CART_NUM", false)) {
					Bundle checkItemParam = new Bundle();
					checkItemParam.putBoolean("CART_ITEMS", true);
					new ServiceGateway().getData(this, 1020, this, 0,
							checkItemParam);
					// ViewController.getInstance(fixContext).getServiceData(1020,
					// this, 0, checkItemParam);
					return;
				}

				if (params.getBoolean("CART_ITEMS", false)) {
					if (entity == null || entity.get("JSONARRAY") == null) {
						setCartItemNumber(0);
						return;
					}

					if (cartItem != null && !cartItem.isEmpty())
						cartItem.clear();
					cartItem = (ArrayList<BaseEntity>) entity.get("JSONARRAY");
					int cartItemNum = 0;

					for (BaseEntity en : cartItem) {
						if (OAuthAuthorization.getInstance().isUserLogin())
							if (OAuthAuthorization.getInstance()
									.getUserProfileData() != null) {
								OAuthAuthorization.getInstance()
										.getUserProfileData()
										.put("QUOTE_ID", en.get("QUOTE_ID"));
							}
						cartItemNum += Integer.valueOf(en.get("QTY", 0)
								.toString());
					}
					setCartItemNumber(cartItemNum);
					return;
				} else
					setCartItemNumber(0);

			}
		}
	}

	@Override
	public void onConnectionError(Exception ex) {
		handler.sendEmptyMessage(0);
	}

	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			Toast.makeText(MakanDealApplication.this,
					getString(R.string.exceptionMsg), Toast.LENGTH_SHORT)
					.show();
		}
	};

	public BaseEntity getServiceVoucherByEntityID(String id) {
		if (dealArrayList != null)
			for (BaseEntity en : dealArrayList)
				if (en.get("ENTITY_ID").equals(id))
					return en;
		return null;
	}

	public void setCheckoutEntity(BaseEntity checkoutEntity) {
		this.checkoutEntity = checkoutEntity;
	}

	public BaseEntity getCheckoutEntity() {
		return checkoutEntity;
	}
}
