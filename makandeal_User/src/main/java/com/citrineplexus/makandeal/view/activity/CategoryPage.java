package com.citrineplexus.makandeal.view.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.FilterPageAdapter;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.entity.FilterItemEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.view.MakanDealApplication;
import com.citrineplexus.makandeal.view.widget.GridViewScrollable;

public class CategoryPage extends AbstractActivity implements
		OnItemClickListener, OnClickListener {

	private FilterPageAdapter adapter;
	private GridViewScrollable mGridview;
	private ArrayList<BaseEntity> eventListing = new ArrayList<>();
	private LinearLayout eventLinearLayout;
	private boolean isFromHome = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category_page);
		GoogleAnalyticsLib.getInstance(this).trackView("Filter Page");

		if (getIntent() != null && getIntent().getExtras() != null
				&& getIntent().getExtras().get("VIEWNAME") != null)
			if (getIntent().getExtras().getString("VIEWNAME")
					.equals("HomePage")) {
				isFromHome = true;
			}

		eventLinearLayout = (LinearLayout) findViewById(R.id.filter_eventLinear);
		findViewById(R.id.filter_allDeal).setOnClickListener(this);

		initCategoryView();
		initEventView();
	}

	private void initCategoryView() {
		GridViewScrollable mGrideView = (GridViewScrollable) findViewById(R.id.gridViewId);
		final FilterPageAdapter adapter = new FilterPageAdapter(this,
				R.layout.child_grideview_home_page, MakanDealApplication
						.getInstance().getFilterAry(), false);
		adapter.setChildType(FilterPageAdapter.GRIDVIEW_TYPE);
		mGrideView.setAdapter(adapter);
		mGrideView.setExpanded(true);
		mGrideView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				FilterItemEntity entity = adapter.getItem(position);
				Bundle localBundle = new Bundle();
				localBundle.putString("FILTERNAME", entity.getTitle());
				localBundle.putInt("FLITERBY", entity.getId());
				if (isFromHome) {
					openView(ViewName.HOMEPAGE, localBundle);
				} else {
					Intent mIntent = new Intent();
					localBundle.putBoolean("ISCAMPAIGN", false);
					mIntent.putExtras(localBundle);
					setResult(Activity.RESULT_OK, mIntent);
				}
				finish();
			}
		});
	}

	private void initEventView() {
		if (!eventListing.isEmpty())
			eventListing.clear();

		MakanDealApplication mApplication = MakanDealApplication.getInstance();
		eventListing.addAll(mApplication.getPromoList());
		eventListing.addAll(mApplication.getEventListing());

		for (BaseEntity en : eventListing) {
			TextView tv = new TextView(this);
			tv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT));
			tv.setCompoundDrawablesWithIntrinsicBounds(null, null,
					getResources().getDrawable(R.drawable.icon_arrow), null);
			tv.setTag(en);
			tv.setText(en.get("CAMPAIGN_NAME").toString());
			tv.setBackgroundColor(getResources().getColor(R.color.white));
			int pixels = (int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP, 10, getResources()
							.getDisplayMetrics());
			tv.setPadding(pixels, pixels, pixels, pixels);
			tv.setOnClickListener(this);
			View dividen = new View(this);
			dividen.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
					1));
			dividen.setBackgroundColor(getResources().getColor(R.color.grey_03));
			eventLinearLayout.addView(tv);
			eventLinearLayout.addView(dividen);
		}
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {

	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_UNLOCKED;
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.ALL;
	}

	@Override
	void handlerMessage(Message msg) {

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// FilterItemEntity entity = adapter.getItem(position);
		// Bundle localBundle = new Bundle();
		// localBundle.putString("FILTERNAME", entity.getTitle());
		// localBundle.putInt("FLITERBY", entity.getId());
		// openView(ViewName.HOMEPAGE, localBundle);
		// finish();
	}

	@Override
	int drawerID() {
		return 103;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.filter_allDeal) {
			Bundle localBundle = new Bundle();
			localBundle.putString("FILTERNAME", "All Deals");
			localBundle.putInt("FLITERBY", -1000);
			if (isFromHome) {
				openView(ViewName.HOMEPAGE, localBundle);
			} else {
				Intent mIntent = new Intent();
				localBundle.putBoolean("ISCAMPAIGN", false);
				mIntent.putExtras(localBundle);
				setResult(Activity.RESULT_OK, mIntent);
			}
			finish();
		} else if (v.getTag() != null && v.getTag() instanceof BaseEntity) {
			BaseEntity en = (BaseEntity) v.getTag();

			Intent mIntent = handleEventAndPromoLogic2(en);
			if (mIntent != null)
				setResult(Activity.RESULT_OK, mIntent);
			finish();
		}
	}
}
