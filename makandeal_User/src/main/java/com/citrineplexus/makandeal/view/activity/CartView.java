package com.citrineplexus.makandeal.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.AbstractRecyclerAdapter.OnListAdapterItemClickListener;
import com.citrineplexus.makandeal.adapter.AdapterBinder;
import com.citrineplexus.makandeal.adapter.FooterListAdapter;
import com.citrineplexus.makandeal.adapter.ListAdapter;
import com.citrineplexus.makandeal.binder.CartBinder;
import com.citrineplexus.makandeal.binder.CartFootBinder;
import com.citrineplexus.makandeal.callback.CouponCallBack;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.service.OAuthAuthorization;
import com.citrineplexus.makandeal.util.Calculator;
import com.citrineplexus.makandeal.view.MakanDealApplication;

import java.util.ArrayList;

public class CartView extends DefaultRecyclerListView implements
        OnListAdapterItemClickListener, OnClickListener, CouponCallBack {

    FooterListAdapter mAdapter = null;
    private CartBinder cartBinder = null;
    private CartFootBinder footBinder = null;
    // private boolean updateCart = false;
//	private double grandTotal = 0;
    private Button checkoutBtn = null;
    private boolean checkOut = true;

    private TextView emptyMsg = null;

    public static enum refreshMode {
        INIT_CART, CHANGE_QUANTITY, REMOVE_CART, REFRESH_COUPON
    }

    MenuItem mDynamicMenuItem;

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_menu, menu);
        mDynamicMenuItem = menu.findItem(R.id.action_edit);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        mDynamicMenuItem.setVisible(checkOut);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                if (!getListAdapter().isEmpty()) {
                    GoogleAnalyticsLib.getInstance(this).trackEvent("Add to Cart",
                            "Edit");
                    cartBinder.openDeleteLayout();
                    getListAdapter().notifyDataSetChanged();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onRefresh() {
        updateCart(refreshMode.INIT_CART);
    }

    public void updateCart(refreshMode mode) {
        boolean loading = false;
        // updateCart = true;
        Bundle param = new Bundle();
        param.putBoolean("CART_ITEMS", true);
        param.putString("TYPE", mode.name());
        if (mode == refreshMode.INIT_CART)
            loading = false;
        param.putBoolean("LOADING", loading);
        getServiceData(1020, 0, param, loading);
    }

    @Override
    public void onAquireResult(BaseEntity entity, int position, Bundle params) {
        if (params == null)
            return;
        super.onAquireResult(entity, position, params,
                params.getBoolean("LOADING", false));
        String type = params.getString("TYPE");

        switch (refreshMode.valueOf(type)) {
            case REMOVE_CART:
                for (int i = 0; i < getListAdapter().getTotalSize(); i++) {
                    BaseEntity en = getListAdapter().getData(i);
                    if (en.get("ITEM_ID").toString()
                            .equalsIgnoreCase(params.getString("ITEM_ID"))) {
                        getListAdapter().removeData(i);
                        break;
                    }
                }
            case CHANGE_QUANTITY:
                updateCart(refreshMode.CHANGE_QUANTITY);
//                break;

            case REFRESH_COUPON:
                if (entity.get("MESSAGES") != null) {
                    BaseEntity msgEntity = (BaseEntity) entity.get("MESSAGES");
                    ArrayList<BaseEntity> errorAry = (ArrayList<BaseEntity>) msgEntity
                            .get("ERROR");
                    BaseEntity errorEntity = errorAry.get(0);
                    String errorMessage = (String) errorEntity.get("MESSAGE");
                    int index1 = errorMessage.indexOf("\"");
                    errorMessage = errorMessage.substring(index1 + 1,
                            errorMessage.length());
                    int index2 = errorMessage.indexOf("\"");
                    Toast.makeText(CartView.this,
                            errorMessage, Toast.LENGTH_SHORT)
                            .show();
                } else {
                    footBinder.refreshCoupon(entity);
//				if (position == 1054 || position == 1055)
                    if (position == 1017)
                        break;
                    getCoupon();
                }
                break;

            default:
                break;
        }
    }

    @Override
    int getContentView() {
        return R.layout.activity_cart_page;
    }

    @Override
    ListAdapter initListAdapter() {
        mAdapter = new FooterListAdapter(this);
        return mAdapter;
    }

    @Override
    String getPageTitle() {
        return "Cart";
    }

    @Override
    AdapterBinder getBinder() {
        footBinder = new CartFootBinder(this, this, this);
        mAdapter.setBinder(null, footBinder);
        cartBinder = new CartBinder(this, this);
        return cartBinder;
    }

    @Override
    int getContentId() {
        return -111;
    }

    @Override
    void onInit() {
        checkoutBtn = (Button) findViewById(R.id.checkoutNow);
        checkoutBtn.setOnClickListener(this);
        emptyMsg = (TextView) findViewById(R.id.emptyView);
        onRefreshCart(MakanDealApplication.getInstance().getCartItemNumber());
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCart(refreshMode.INIT_CART);
    }

    @Override
    void getServiceData(int contentID) {
        getCoupon();

    }

    private void getCoupon() {
        Bundle checkCartCoupon = new Bundle();
        checkCartCoupon.putBoolean("CART_COUPON", true);
        if (OAuthAuthorization.getInstance().getUserProfileData() != null
                && OAuthAuthorization.getInstance().getUserProfileData()
                .get("QUOTE_ID") != null) {

            checkCartCoupon.putString("QUOTE_ID", OAuthAuthorization
                    .getInstance().getUserProfileData().get("QUOTE_ID")
                    .toString());
        }

        if (MakanDealApplication.getInstance().getCartItem() != null
                && !MakanDealApplication.getInstance().getCartItem().isEmpty()) {
            BaseEntity en = MakanDealApplication.getInstance().getCartItem()
                    .get(0);
            checkCartCoupon.putString("QUOTE_ID", (String) en.get("QUOTE_ID"));
        }
        checkCartCoupon.putString("TYPE", refreshMode.REFRESH_COUPON.name());
        getServiceData(1017, 0, checkCartCoupon, false);
    }

    @Override
    String getUniqueKey() {
        return null;
    }

    @Override
    void adapterEmptyHandler(boolean adapterIsEmpty, RecyclerView recyclerView) {

    }

    @Override
    int setDrawerLockMode() {
        return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
    }

    @Override
    ActionBarMethod setActionBarMethod() {
        return ActionBarMethod.HIDE_ALL;
    }

    @Override
    int drawerID() {
        return -1;
    }

    @Override
    public void onRefreshCart(int cartItemCount) {
        // handler.sendEmptyMessage(LOADING_CLOSE);
        super.onRefreshCart(cartItemCount);

        // if (updateCart) {
        if (swipeLayout.isRefreshing())
            swipeLayout.setRefreshing(false);

        handler.sendEmptyMessage(LOADING_CLOSE);
        // updateCart = false;
        if (!mAdapter.isEmpty())
            mAdapter.clearData();
        if (MakanDealApplication.getInstance().getCartItem() != null)
            for (BaseEntity en : MakanDealApplication.getInstance()
                    .getCartItem()) {
                mAdapter.addData(en);
            }
        cartBinder.setTotalChild(mAdapter.getTotalSize());
        mAdapter.notifyDataSetChanged();
        calculateGrandTotal();
        // }
        checkEmpty();
    }

    public void checkEmpty() {
        if ((mAdapter.getTotalSize()) <= 0) {
            emptyMsg.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            checkoutBtn.setText("Shop Now");
            checkOut = false;
        } else {
            emptyMsg.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            checkoutBtn.setText("Checkout Now");
            checkOut = true;
        }
        supportInvalidateOptionsMenu();
    }

    private void calculateGrandTotal() {
        double count = 0;
        for (BaseEntity en : mAdapter.getDataList()) {
            double price = Double.parseDouble(en.get("PRICE", "").toString());
            int qty = ((Integer) en.get("QTY"));
            count = count + (price * qty);
        }
        if (footBinder != null)
            footBinder.setCartTotal(Calculator.round(count));
    }

    public void modifyQuantity(int position, String quantity) {
        BaseEntity baseEntity = mAdapter.getData(position);
        if (baseEntity != null) {
            updateQuantity(Integer.valueOf(quantity), baseEntity);
        }
    }

    public void updateQuantity(int qty, BaseEntity baseEntity) {
        if (baseEntity == null || qty <= 0
                || baseEntity.get("ENTITY_ID") == null
                || baseEntity.get("REDEEM_LOCATION") == null)
            return;

        Bundle param = new Bundle();
        param.putBoolean("UPDATE_QUANTITY", true);
        param.putString("QTY", String.valueOf(qty));
        param.putString("ENTITY_ID", baseEntity.get("ENTITY_ID").toString());
        param.putString("REDEEM_LOCATION", baseEntity.get("REDEEM_LOCATION")
                .toString());
        param.putString("TYPE", refreshMode.CHANGE_QUANTITY.name());
        param.putString("ITEM_ID", baseEntity.get("ITEM_ID").toString());
        try {
            getServiceData(1014, 0, param, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClickListener(View view, int position) {
        switch (view.getId()) {
            case R.id.deleteBtn:
                if (getListAdapter().getData(position) == null)
                    return;
                BaseEntity en = (BaseEntity) getListAdapter().getData(position);

                Bundle localBundle = new Bundle();
                localBundle.putString("REQUEST_TYPE", "REMOVE");
                localBundle.putString("TYPE", refreshMode.REMOVE_CART.name());
                localBundle.putString("ITEM_ID", en.get("ITEM_ID", "").toString());
                getServiceData(1016, position, localBundle, true);
                GoogleAnalyticsLib.getInstance(this).trackEvent("Add to Cart",
                        "Delete - " + en.get("NAME").toString());
                // getListAdapter().removeData(position);
                break;

            default:
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (cartBinder != null && cartBinder.getRemoveFlag()) {
            cartBinder.refreshData();
        }
    }

    @Override
    public void onBackPressed() {
        if (!getListAdapter().isEmpty() && cartBinder.getRemoveFlag()) {
            cartBinder.openDeleteLayout();
            getListAdapter().notifyDataSetChanged();
        } else
            super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.checkoutNow) {
            if (checkOut) {
                Bundle checkoutBundle = new Bundle();
                if (OAuthAuthorization.getInstance().isUserLogin()) {
                    if (OAuthAuthorization.getInstance().getUserProfileData()
                            .get("QUOTE_ID") == null)
                        return;

                    checkoutBundle.putString("QUOTE_ID", OAuthAuthorization
                            .getInstance().getUserProfileData().get("QUOTE_ID")
                            .toString());
                    checkoutBundle.putBoolean("GET_ADDRESS", true);
                    checkoutBundle.putDouble("GRAND_TOTAL", footBinder.getGrandTotal());
                    openView(ViewName.CHECKOUT, checkoutBundle);
                }

            } else {
                openView(ViewName.HOMEPAGE, null);
                this.finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            if (data.getBooleanExtra("QUIT_CARTVIEW", false))
                this.finish();
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void couponAction(int contentId, Bundle param) {
        getServiceData(contentId, 0, param, true);
        // new ServiceGateway().getData(this, contentId,
        // this, 0, extra);
    }

}
