package com.citrineplexus.makandeal.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citrineplexus.makandeal.R;

public class PasscodeButton extends LinearLayout {

	TextView numberText = null, alphabetText = null;

	public PasscodeButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs);
	}

	public PasscodeButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}

	public PasscodeButton(Context context) {
		super(context);
	}

	private void init(Context context, AttributeSet attrs) {
		initView(context);

		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.PasscodeButton);
		String numberString = a.getString(R.styleable.PasscodeButton_Number);
		String alphabetString = a
				.getString(R.styleable.PasscodeButton_Alphabet);

		if (numberString != null) {
			if (numberString.equalsIgnoreCase("0")) {
				setWeightSum(0);
				numberText.setGravity(Gravity.CENTER);
				alphabetText.setVisibility(View.GONE);
			}
			numberText.setText(numberString);
		}
		if (alphabetString != null) {
			alphabetText.setText(alphabetString);
			alphabetText.setVisibility(VISIBLE);
		}
		a.recycle();
	}

	private void initView(Context context) {
		setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		setOrientation(VERTICAL);
		setWeightSum(3);
		setPadding(0, 10, 0, 10);

		numberText = new TextView(context);
		numberText.setGravity(Gravity.CENTER_HORIZONTAL);
		numberText.setTextSize(context.getResources().getDimension(
				R.dimen.TextMedium));
		numberText.setTextColor(context.getResources().getColor(
				R.color.textColor));
		numberText.setIncludeFontPadding(false);
		LayoutParams numberLayoutParam = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 2);
		numberText.setLayoutParams(numberLayoutParam);

		alphabetText = new TextView(context);
		alphabetText.setGravity(Gravity.CENTER_HORIZONTAL);
		alphabetText.setVisibility(INVISIBLE);
		alphabetText.setTextSize(12);
		alphabetText.setIncludeFontPadding(false);
		alphabetText.setTextColor(context.getResources().getColor(
				R.color.textColor));
		LayoutParams alphabetLayoutParam = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1);
		alphabetText.setLayoutParams(alphabetLayoutParam);

		addView(numberText);
		addView(alphabetText);

	}

}
