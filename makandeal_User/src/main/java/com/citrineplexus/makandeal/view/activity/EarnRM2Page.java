package com.citrineplexus.makandeal.view.activity;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.util.GlobalService;

public class EarnRM2Page extends AbstractActivity implements OnClickListener {

	private boolean fromPayment = false;
	private final String title = "Earn RM2";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setTitle(title);
		GoogleAnalyticsLib.getInstance(this).trackView(title);

		setContentView(R.layout.activity_earn_rm2);
		findViewById(R.id.earnRM2InviBtn).setOnClickListener(this);

		if (getIntent() != null && getIntent().getExtras() != null
				&& getIntent().getExtras().get("FROM_PAYMENT") != null)
			fromPayment = getIntent().getExtras().getBoolean("FROM_PAYMENT");
		else
			fromPayment = false;
	}

	@Override
	public void onBackPressed() {
		GoogleAnalyticsLib.getInstance(this).trackEvent("Invite Friends",
				"Cancel");
		if (fromPayment)
			openView(ViewName.HOMEPAGE, null);
		else
			super.onBackPressed();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			GoogleAnalyticsLib.getInstance(this).trackEvent("Invite Friends",
					"Cancel");
			finish();
		}
		return true;
	}

	@Override
	int setDrawerLockMode() {
		return DrawerLayout.LOCK_MODE_UNLOCKED;
	}

	@Override
	void handlerMessage(Message msg) {

	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.earnRM2InviBtn) {
			Bundle params = new Bundle();
			GoogleAnalyticsLib.getInstance(this).trackEvent("Invite Friends",
					null);
			getServiceData(1001, 0, params, true);
		}
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {
		super.onAquireResult(entity, position, params, true);
		if (entity != null && entity.get("URL") != null) {
			GlobalService.ShareMessage(this,
					getResources().getString(R.string.earnRM2SuggestText) + " "
							+ entity.get("URL").toString());
		}
	}

	@Override
	ActionBarMethod setActionBarMethod() {
		return ActionBarMethod.ALL;
	}

	@Override
	int drawerID() {
		return 107;
	}

}
