package com.citrineplexus.makandeal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.FilterItemEntity;

public class FilterPageAdapter extends ArrayAdapter<FilterItemEntity> {

	private LayoutInflater inflater;
	private int childType = -1;
	public static final int FILTER_TYPE = 1;
	public static final int GRIDVIEW_TYPE = 2;
	private Context context = null;
	private boolean isImageUrl = false;

	public FilterPageAdapter(Context context, int resource,
			FilterItemEntity[] obj, boolean isImageUrl) {
		super(context, resource, obj);
		this.context = context;
		this.inflater = LayoutInflater.from(context);
		this.isImageUrl = isImageUrl;
	}

	public void setChildType(int childType) {
		this.childType = childType;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (childType == FILTER_TYPE)
			return inflateFilterType(position, convertView, parent, holder);
		else
			return inflateGridViewType(position, convertView, parent, holder);
	}

	private View inflateFilterType(int position, View convertView,
			ViewGroup parent, ViewHolder holder) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.child_fliter_page, parent,
					false);
			holder = new ViewHolder();
			holder.icon = (ImageView) convertView
					.findViewById(R.id.filter_ImageIcon);
			holder.title = (TextView) convertView
					.findViewById(R.id.filter_title);
			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();

		FilterItemEntity entity = getItem(position);
		if (holder.icon != null) {
			holder.icon.setImageResource(entity.getIcon());

		}

		if (holder.title != null)
			holder.title.setText(entity.getTitle());

		return convertView;
	}

	private View inflateGridViewType(int position, View convertView,
			ViewGroup parent, ViewHolder holder) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.child_grideview_home_page,
					parent, false);
			holder = new ViewHolder();
			holder.icon = (ImageView) convertView
					.findViewById(R.id.category_Img);
			holder.title = (TextView) convertView
					.findViewById(R.id.category_title);
			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();
		FilterItemEntity entity = getItem(position);
		if (holder.title != null) {
			if (!isImageUrl) {
				holder.icon.setVisibility(View.GONE);
				holder.title.setCompoundDrawablesWithIntrinsicBounds(0,
						entity.getIcon(), 0, 0);
			} else {
				holder.icon.setVisibility(View.VISIBLE);
				ImageLoader.getInstance(context).DisplayImage(
						entity.getImgURL(), holder.icon, ScaleType.CENTER_CROP,
						false);
				holder.title.setAllCaps(true);
			}
			holder.title.setText(entity.getTitle());
		}
		return convertView;
	}

	private class ViewHolder {
		ImageView icon;
		TextView title;
	}
}
