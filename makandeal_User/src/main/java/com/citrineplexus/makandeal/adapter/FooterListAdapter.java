package com.citrineplexus.makandeal.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.adapter.AdapterBinder.ViewHolder;
import com.citrineplexus.makandeal.binder.CartBinder.CartHolder;
import com.citrineplexus.makandeal.binder.CartFootBinder.CartFootHolder;

public class FooterListAdapter extends ListAdapter {
	private static final int TYPE_HEADER = 0;
	private static final int TYPE_ITEM = 1;

	private AdapterBinder footerBinder = null;

	public FooterListAdapter(Context context) {
		super(context);
		this.context = context;
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, int position) {
		if (viewHolder instanceof CartHolder) {
			viewHolder.binding(position, mData.get(position),
					ImageLoader.getInstance(context), false);
		} else if (viewHolder instanceof CartFootHolder)
			viewHolder.binding(position, null,
					ImageLoader.getInstance(context), true);
	}

	@Override
	public int getItemCount() {
		return super.getItemCount() + 1;
	}

	public void setBinder(AdapterBinder binder, AdapterBinder footerBinder) {
		super.setBinder(binder);
		this.footerBinder = footerBinder;
	}

	@Override
	public int getItemViewType(int position) {
		if (isPositionFooter(position))
			return TYPE_HEADER;
		return TYPE_ITEM;
	}

	private boolean isPositionFooter(int position) {
		return position == (getItemCount() - 1);
	}

	@Override
	public AdapterBinder.ViewHolder onCreateViewHolder(ViewGroup viewGroup,
			int viewType) {
		if (viewType == TYPE_ITEM) {
			View v = mInflater.inflate(binder.getLayoutResourceId(), viewGroup,
					false);
			return binder.newViewHolder(v);
		} else if (viewType == TYPE_HEADER) {
			View v = mInflater.inflate(footerBinder.getLayoutResourceId(),
					viewGroup, false);
			return footerBinder.newViewHolder(v);
		}
		throw new RuntimeException("there is no type that matches the type "
				+ viewType + " + make sure your using types correctly");
	}
}
