package com.citrineplexus.makandeal.adapter;

import java.util.ArrayList;
import java.util.List;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

public abstract class AbstractRecyclerAdapter<V, K extends RecyclerView.ViewHolder>
		extends RecyclerView.Adapter<K> {

	protected List<V> mData = new ArrayList<V>();

	@Override
	public abstract void onBindViewHolder(K viewHolder, int position);

	@Override
	public abstract K onCreateViewHolder(ViewGroup viewGroup, int viewType);

	@Override
	public int getItemCount() {
		return mData.size();
	}

	public int getTotalSize() {
		return mData.size();
	}

	public void addData(V data) {
		mData.add(data);
	}

	public void removeData(int position) {
		mData.remove(position);
		notifyDataSetChanged();
	}

	public V getData(int position) {
		return mData.get(position);
	}

	public boolean isEmpty() {
		return mData.isEmpty();
	}

	public void setDataSet(List<V> dataSet) {
		if (dataSet != null)
			mData = dataSet;
	}

	public void clearData() {
		if (!mData.isEmpty())
			mData.clear();
		notifyDataSetChanged();
	}

	public interface OnListAdapterItemClickListener {
		public void onItemClickListener(View view, int position);
	}

}
