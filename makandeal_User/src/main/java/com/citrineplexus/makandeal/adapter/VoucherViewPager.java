package com.citrineplexus.makandeal.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.citrineplexus.makandeal.view.fragment.VoucherFragment;

public class VoucherViewPager extends FragmentPagerAdapter {

	Fragment unusedFragment, allFragment;

	public VoucherViewPager(FragmentManager fm) {
		super(fm);
		unusedFragment = new VoucherFragment();
		Bundle bundle = new Bundle();
		bundle.putBoolean("SHOW_ALL_FLAG", false);
		unusedFragment.setArguments(bundle);
		
		allFragment = new VoucherFragment();
		Bundle bundle2 = new Bundle();
		bundle2.putBoolean("SHOW_ALL_FLAG", true);
		allFragment.setArguments(bundle2);

	}

	@Override
	public int getCount() {
		return 2;
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case 0:
			return unusedFragment;

		default:
			return allFragment;

		}
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return "Unused";

		case 1:
			return "All";

		}
		return super.getPageTitle(position);
	}

}
