package com.citrineplexus.makandeal.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.citrineplexus.makandeal.view.fragment.AboutFragment;
import com.citrineplexus.makandeal.view.fragment.DetailFragment;
import com.citrineplexus.makandeal.view.fragment.LocationFragment;

public class DealPageViewPager extends FragmentPagerAdapter {

	private String entityId = "";
	private String vendorId = "";

	public DealPageViewPager(FragmentManager fm, String entityId,String vendorId) {
		super(fm);
		this.entityId = entityId;
		this.vendorId = vendorId;
	}

	@Override
	public int getCount() {
		return 3;
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case 0:
			return AboutFragment.newInstance(entityId);

		case 1:
			return DetailFragment.newInstance(entityId);

		default:
			return LocationFragment.newInstance(vendorId);
		}
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return "About";

		case 1:
			return "Details";

		case 2:
			return "Locations";
		}
		return super.getPageTitle(position);
	}

}
