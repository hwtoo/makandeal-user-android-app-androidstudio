package com.citrineplexus.makandeal.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.adapter.AbstractRecyclerAdapter.OnListAdapterItemClickListener;
import com.citrineplexus.makandeal.entity.BaseEntity;

public abstract class AdapterBinder {

	public ViewHolder vh = null;

	static OnListAdapterItemClickListener listener = null;

	public abstract int getLayoutResourceId();

	public abstract ViewHolder getViewHolder(View v);

	public AdapterBinder(OnListAdapterItemClickListener listener) {
		AdapterBinder.listener = listener;
	}

	public void setOnListAdapterItemClickListener(
			OnListAdapterItemClickListener listener) {
		AdapterBinder.listener = listener;
	}

	public ViewHolder newViewHolder(View v) {
		// ViewHolder vh = new ViewHolder(v);
		return getViewHolder(v);
	}

	public static abstract class ViewHolder extends RecyclerView.ViewHolder
			implements OnClickListener {

		public ViewHolder(View v) {
			super(v);
			// initViewHolder(v);
			v.setOnClickListener(this);
		}

		public abstract void binding(int position, BaseEntity entity,
				ImageLoader imageLoader, boolean isFooter);

		@Override
		public void onClick(View v) {
			if (listener != null)
				listener.onItemClickListener(v, getPosition());
		}

		public OnListAdapterItemClickListener getItemClickListener() {
			return listener;
		}

	}
}
