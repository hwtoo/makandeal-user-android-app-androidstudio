package com.citrineplexus.makandeal.adapter;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.view.fragment.CategoriesFragment;
import com.citrineplexus.makandeal.view.fragment.InformationFragment;

public class MerchantMicroSiteAdapter extends FragmentPagerAdapter {

	private Resources res = null;
	private String vendorId="",entityId="";

	public MerchantMicroSiteAdapter(FragmentManager fm, Resources resource,String vendorId,String entityId) {
		super(fm);
		this.res = resource;
		this.vendorId = vendorId;
		this.entityId = entityId;
	}

	@Override
	public int getCount() {
		return 2;
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case 0:
			return CategoriesFragment.newInstance(vendorId);

		case 1:
			return InformationFragment.newInstance(vendorId);

		default:
			return null;
		}
	}
	
	public Drawable getPageIcon(int position){
		Drawable icon = null;
		switch (position) {
		case 0:
			icon = res.getDrawable(R.drawable.microsite_categories);
			break;
			
		case 1:
			icon = res.getDrawable(R.drawable.microsite_information);
			break;

		default:
			break;
		}
		
		return icon;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		Drawable image;
		SpannableString sb;
		ImageSpan imageSpan;
		switch (position) {
		case 0:
//			image = res.getDrawable(R.drawable.navitem_blog);
//			image.setBounds(0, 0, image.getIntrinsicWidth(),
//					image.getIntrinsicHeight());
//			sb = new SpannableString("     Categories");
//			imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
//			sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//			// return "Information";
//			return sb;
			return "Categories";

		case 1:
			image = res.getDrawable(R.drawable.icon_map);
			image.setBounds(0, 0, image.getIntrinsicWidth(),
					image.getIntrinsicHeight());
			sb = new SpannableString("     Information");
			imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
			sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			// return "Information";
//			return sb;
			return "Information";
		}
		return super.getPageTitle(position);
	}

}
