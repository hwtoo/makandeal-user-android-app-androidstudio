package com.citrineplexus.makandeal.adapter;

import java.util.HashMap;
import java.util.List;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.citrineplexus.makandeal.callback.OnExpandableListRender;
import com.citrineplexus.makandeal.entity.BaseEntity;

public class ExpandableAdapter extends BaseExpandableListAdapter {

	private List<String> _listDataHeader = null;
	private HashMap<String, List<BaseEntity>> _listDataChild = null;
	// private Context context = null;
	// private ExpandableListView expandableListView = null;

	private OnExpandableListRender listener;

	public ExpandableAdapter(OnExpandableListRender listener) {
		// this.context = context;
		this.listener = listener;
		// this.expandableListView = expandableListView;
	}

	public void set_listDataHeader(List<String> _listDataHeader) {
		this._listDataHeader = _listDataHeader;
	}

	public void set_listDataChild(
			HashMap<String, List<BaseEntity>> _listDataChild) {
		this._listDataChild = _listDataChild;
	}

	@Override
	public int getGroupCount() {
		return _listDataHeader.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return _listDataChild.get(_listDataHeader.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return _listDataHeader.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		return listener.getGroupView(groupPosition, isExpanded, convertView,
				parent);
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		return listener.getChildView(groupPosition, childPosition, isLastChild,
				convertView, parent);
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
