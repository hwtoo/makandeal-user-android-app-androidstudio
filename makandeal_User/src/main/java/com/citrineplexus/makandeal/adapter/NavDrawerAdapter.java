package com.citrineplexus.makandeal.adapter;

import android.content.Context;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.entity.NavDrawerItem;
import com.citrineplexus.makandeal.entity.NavDrawerItemWithIcon;
import com.citrineplexus.makandeal.view.MakanDealApplication;

public class NavDrawerAdapter extends ArrayAdapter<NavDrawerItem> {

	private LayoutInflater inflater;

	public final static int WITH_ICON = 0;
	public final static int TEXT_ONLY = 1;
	public final static int WITH_NOTIFICATION = 2;
	public final static int SEPARATOR = -1;

	public NavDrawerAdapter(Context context, int resource) {
		super(context, resource);
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public void add(NavDrawerItem object) {
		super.add(object);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		NavDrawerItem item = getItem(position);
		switch (item.getType()) {
		case WITH_ICON:
		case TEXT_ONLY:
		case WITH_NOTIFICATION:
			convertView = getView(convertView, parent, item);
			break;
		case SEPARATOR:
			convertView = getSeparateView(convertView, parent);
			break;

		default:
			break;
		}
		return convertView;
	}

	private View getSeparateView(View convertView, ViewGroup parentView) {
		SeparatorHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.navdrawer_separator,
					parentView, false);
			holder = new SeparatorHolder(convertView);
			convertView.setTag(holder);
		} else
			holder = (SeparatorHolder) convertView.getTag();
		return convertView;
	}

	private View getView(View convertView, ViewGroup parentView,
			NavDrawerItem item) {

		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.navdrawer_item, parentView,
					false);
			holder = new ViewHolder(
					(TextView) convertView.findViewById(R.id.navTitle),
					(ImageView) convertView.findViewById(R.id.navIcon),
					(SwitchCompat) convertView.findViewById(R.id.navSwitch));
			convertView.setTag(holder);
		} else
			holder = (ViewHolder) convertView.getTag();
		if (item.getType() == WITH_ICON || item.getType() == WITH_NOTIFICATION)
			setWithIconValue(holder, item);
		else
			setWithTextValue(holder, item);

		return convertView;
	}

	private void setWithIconValue(final ViewHolder holder, NavDrawerItem item) {
		NavDrawerItemWithIcon iconItem = (NavDrawerItemWithIcon) item;
		if (holder.textHolder != null) {
			holder.textHolder.setText(iconItem.getLabel());
		}

		if (iconItem.isNotification()) {
			if (holder.switchHolder != null) {
				holder.switchHolder.setVisibility(View.VISIBLE);
				if (iconItem.getId() == 108)
					holder.switchHolder.setChecked(MakanDealApplication
							.getInstance().getPreferences()
							.getString("PASSCODE", "").equals("") ? false
							: true);
				else if (iconItem.getId() == 114)
					holder.switchHolder.setChecked(MakanDealApplication
							.getInstance().getPreferences()
							.getBoolean("NotificationFlag", true));
			}
		} else {
			if (holder.imageHolder != null) {
				holder.imageHolder.setVisibility(View.VISIBLE);
				holder.imageHolder.setImageResource(iconItem.getIcon());
			}
		}
	}

	private void setWithTextValue(ViewHolder holder, NavDrawerItem item) {
		if (holder.textHolder != null) {
			holder.textHolder.setText(item.getLabel());
		}
	}

	@Override
	public int getItemViewType(int position) {
		return this.getItem(position).getType();
	}

	@Override
	public int getViewTypeCount() {
		return 3;
	}

	public static class ViewHolder {
		public final TextView textHolder;
		public final ImageView imageHolder;
		public final SwitchCompat switchHolder;

		public ViewHolder(TextView text1, ImageView image1,
				SwitchCompat switchHolder) {
			this.textHolder = text1;
			this.imageHolder = image1;
			this.switchHolder = switchHolder;
		}
	}

	public static class SeparatorHolder {
		public final View view;

		public SeparatorHolder(View v) {
			this.view = v;
		}
	}

}
