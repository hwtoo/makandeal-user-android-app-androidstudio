package com.citrineplexus.makandeal.adapter;

import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.view.MakanDealApplication;

public class SearchSuggestionAdapter extends CursorAdapter {

	// private TextView text;
	private List<String> items = null;

	public void addData(String data) {
		items.add(data);
	}

	@Override
	public boolean isEmpty() {
		return items.isEmpty();
	}

	public void cleanUp() {
		items.clear();
	}

	public SearchSuggestionAdapter(Context context, Cursor c,
			boolean autoRequery) {
		super(context, c, autoRequery);
		items = MakanDealApplication.getInstance().getTermsList();
	}

	public SearchSuggestionAdapter(Context context, Cursor c, int flags) {
		super(context, c, flags);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		// text.setText(items.get(cursor.getPosition()));
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.child_suggestion_item, parent,
				false);
		// text = (TextView) view.findViewById(R.id.suggestionTextView);
		return view;
	}

}
