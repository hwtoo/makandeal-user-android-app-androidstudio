package com.citrineplexus.makandeal.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.adapter.AdapterBinder.ViewHolder;
import com.citrineplexus.makandeal.entity.BaseEntity;

public class ListAdapter extends
		AbstractRecyclerAdapter<BaseEntity, AdapterBinder.ViewHolder> {

	LayoutInflater mInflater = null;
	AdapterBinder binder = null;
	Context context = null;

	public ListAdapter(Context context) {
		this.context = context;
		mInflater = LayoutInflater.from(context);
	}

	public void setBinder(AdapterBinder binder) {
		this.binder = binder;
	}

	public ArrayList<BaseEntity> getDataList() {
		return (ArrayList<BaseEntity>) mData;
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, int position) {
		viewHolder.binding(position, mData.get(position),
				ImageLoader.getInstance(context), false);
	}

	public void addData(ArrayList<BaseEntity> entities, String uniqueKey) {
		for (BaseEntity entity : entities)
			if (entity.get(uniqueKey != null ? uniqueKey : "ENTITY_ID") != null) {
				for (int i = 0; i < mData.size(); i++)
					if (mData
							.get(i)
							.get(uniqueKey != null ? uniqueKey : "ENTITY_ID")
							.equals(entity.get(uniqueKey != null ? uniqueKey
									: "ENTITY_ID")))
						return;
				super.addData(entity);
				// PersistentData.getInstance().dataChange();
				notifyDataSetChanged();
			}
	}

	@Override
	public AdapterBinder.ViewHolder onCreateViewHolder(ViewGroup viewGroup,
			int viewType) {
		View v = mInflater.inflate(binder.getLayoutResourceId(), viewGroup,
				false);
		return binder.newViewHolder(v);
	}

}
