package com.citrineplexus.makandeal.callback;

import android.view.View;
import android.view.ViewGroup;

public interface OnExpandableListRender {
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent);

	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent);
	
}
