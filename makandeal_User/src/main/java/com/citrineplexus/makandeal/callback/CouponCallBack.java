package com.citrineplexus.makandeal.callback;

import android.os.Bundle;

public interface CouponCallBack {

	public void couponAction(int contentId,Bundle param);
}
