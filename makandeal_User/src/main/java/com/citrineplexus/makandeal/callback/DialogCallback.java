package com.citrineplexus.makandeal.callback;

import com.citrineplexus.makandeal.view.activity.AbstractActivity.ViewName;

public interface DialogCallback {

	public void onDialogClick(int id, ViewName whichDialog, Object result);
}
