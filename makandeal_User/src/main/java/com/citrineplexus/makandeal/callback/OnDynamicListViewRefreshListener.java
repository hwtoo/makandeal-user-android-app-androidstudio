package com.citrineplexus.makandeal.callback;

import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;

public interface OnDynamicListViewRefreshListener extends OnRefreshListener {
	public void onLoadMore();

	public void onLoadResponse(LoadResponseFlag flag);

	public enum LoadResponseFlag {
		HAVE_CONTENT, NO_CONTENT, NO_CONNECTION
	}
}
