package com.citrineplexus.makandeal.callback;

public interface OnRefreshCartListener {
	public void onRefreshCart(int cartItemCount);
}
