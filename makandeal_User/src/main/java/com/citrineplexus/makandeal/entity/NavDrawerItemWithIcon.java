package com.citrineplexus.makandeal.entity;

import android.content.res.ColorStateList;

public class NavDrawerItemWithIcon implements NavDrawerItem {

	private int icon = -1, id = -1, type = -1;
	private String label = "";
	private boolean isNotification = false;
	private ColorStateList titleColor;

	public NavDrawerItemWithIcon(int id, int iconResId, String label,
			boolean isNotification, int type) {
		this.id = id;
		this.icon = iconResId;
		this.label = label;
		this.isNotification = isNotification;
		this.type = type;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public int getType() {
		return type;
	}

	public int getIcon() {
		return this.icon;
	}

	public boolean isNotification() {
		return isNotification;
	}

	@Override
	public ColorStateList getTitleSeletor() {
		return titleColor;
	}

	@Override
	public void setTitleSeletor(ColorStateList titleColor) {
		this.titleColor = titleColor;
	}
}
