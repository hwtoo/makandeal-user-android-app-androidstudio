package com.citrineplexus.makandeal.entity;

import android.content.res.ColorStateList;

public interface NavDrawerItem {

	public int getId();

	public String getLabel();

	public int getType();

	public ColorStateList getTitleSeletor();

	public void setTitleSeletor(ColorStateList titleColor);
}
