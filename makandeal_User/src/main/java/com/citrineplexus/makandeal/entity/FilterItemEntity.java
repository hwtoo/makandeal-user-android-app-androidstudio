package com.citrineplexus.makandeal.entity;

public class FilterItemEntity {

	private int icon = -1;
	private String title = "";
	private int id = -1;

	private String imgURL = "";

	public FilterItemEntity(int icon, String title, int id) {
		this.icon = icon;
		this.title = title;
		this.id = id;
	}

	public FilterItemEntity(String imgURL, String title, int id) {
		this.imgURL = imgURL;
		this.title = title;
		this.id = id;
	}

	public int getIcon() {
		return icon;
	}

	public String getTitle() {
		return title;
	}

	public int getId() {
		return id;
	}

	public String getImgURL() {
		return imgURL;
	}
}
