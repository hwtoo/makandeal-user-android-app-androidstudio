package com.citrineplexus.makandeal.entity;

public class SubItemTagEntity {
	private int id;
	private int position;

	public SubItemTagEntity(int position, int tagId) {
		this.position = position;
		this.id = tagId;
	}

	public int getId() {
		return id;
	}

	public int getPosition() {
		return position;
	}
}
