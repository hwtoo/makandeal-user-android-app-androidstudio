package com.citrineplexus.makandeal.entity;

import android.content.res.ColorStateList;

public class NavDrawerItemSeparator implements NavDrawerItem {

	private int type = -1;

	public NavDrawerItemSeparator(int type) {
		this.type = type;
	}

	@Override
	public int getId() {
		return 0;
	}

	@Override
	public String getLabel() {
		return null;
	}

	@Override
	public int getType() {
		return type;
	}

	@Override
	public ColorStateList getTitleSeletor() {
		return null;
	}

	@Override
	public void setTitleSeletor(ColorStateList titleColor) {
	}

}
