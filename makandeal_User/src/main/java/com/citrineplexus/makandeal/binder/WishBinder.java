package com.citrineplexus.makandeal.binder;

import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.AbstractRecyclerAdapter.OnListAdapterItemClickListener;
import com.citrineplexus.makandeal.adapter.AdapterBinder;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.util.Calculator;
import com.citrineplexus.makandeal.util.GlobalService;

public class WishBinder extends AdapterBinder {

	private static boolean removeFlag = false;
	private static float wishlistDeleteWidth = -1;

	public WishBinder(Context context, OnListAdapterItemClickListener listener) {
		super(listener);
		wishlistDeleteWidth = context.getResources().getDimension(
				R.dimen.wishlistDeleteWidth);
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.child_wishlist_item;
	}

	public void openDeleteLayout() {
		removeFlag = !removeFlag;
	}

	public boolean getRemoveFlag() {
		return removeFlag;
	}

	@Override
	public ViewHolder getViewHolder(View v) {
		return new wishListViewHolder(v);
	}

	public static class wishListViewHolder extends AdapterBinder.ViewHolder {

		private TextView productTitle = null, discount = null,
				productPrice = null, originalPrice = null;
		private ImageView productImage = null, deleteBtn = null;
		private LinearLayout contentLayout = null;

		public wishListViewHolder(View v) {
			super(v);
			productImage = (ImageView) v.findViewById(R.id.productImage);
			productTitle = (TextView) v.findViewById(R.id.productTitle);
			productPrice = (TextView) v.findViewById(R.id.productPrice);
			discount = (TextView) v.findViewById(R.id.discount);
			originalPrice = (TextView) v.findViewById(R.id.originalPrice);
			deleteBtn = (ImageView) v.findViewById(R.id.deleteBtn);
			deleteBtn.setOnClickListener(this);
			contentLayout = (LinearLayout) v.findViewById(R.id.contentLayout);
		}

		@Override
		public void binding(int position, BaseEntity entity,
				ImageLoader imageLoader, boolean isFooter) {
			BaseEntity productDetail = (BaseEntity) entity
					.get("PRODUCT_DETAILS");
			if (productImage != null) {
				imageLoader
						.DisplayImage(
								productDetail.get("IMAGE_URL").toString(),
								productImage, ScaleType.CENTER_CROP, false);
			}

			if (productTitle != null)
				productTitle.setText(entity.get("NAME").toString());

			double price = Double.parseDouble(productDetail
					.get("PRICE", "0.00").toString());

			if (GlobalService.checkDiscout(productDetail)) {
				double specialPrice = Double.parseDouble(productDetail.get(
						"SPECIAL_PRICE", "0.00").toString());

				originalPrice.setPaintFlags(originalPrice.getPaintFlags()
						| Paint.STRIKE_THRU_TEXT_FLAG);
				originalPrice.setVisibility(View.VISIBLE);

				int discountValue = ((int) (100.0D - (specialPrice / price) * 100.0D));

				if (productPrice != null)
					productPrice
							.setText("RM " + Calculator.round(specialPrice));

				if (originalPrice != null)
					originalPrice.setText("RM " + Calculator.round(price));

				if (discount != null) {
					discount.setVisibility(View.VISIBLE);
					discount.setText(discountValue + "%OFF");
				}
			} else {
				if (productPrice != null)
					productPrice.setText("RM " + Calculator.round(price));
			}

			if (removeFlag) {
				contentLayout.animate().translationX(wishlistDeleteWidth)
						.start();
			} else {
				if (!removeFlag) {
					contentLayout.animate().translationX(0).start();
				}
			}
		}

	}

}
