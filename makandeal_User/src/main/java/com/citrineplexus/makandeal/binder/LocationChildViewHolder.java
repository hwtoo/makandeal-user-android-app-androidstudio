package com.citrineplexus.makandeal.binder;

import android.widget.TextView;

import com.citrineplexus.makandeal.view.widget.BoxButton;

public class LocationChildViewHolder {
	public TextView address, contactNumber, operationHour;
	public BoxButton contactUs_Phone, map;
}
