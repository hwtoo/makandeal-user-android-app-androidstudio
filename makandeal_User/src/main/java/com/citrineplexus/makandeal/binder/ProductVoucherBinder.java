package com.citrineplexus.makandeal.binder;

import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.AbstractRecyclerAdapter.OnListAdapterItemClickListener;
import com.citrineplexus.makandeal.adapter.AdapterBinder;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.util.Calculator;
import com.citrineplexus.makandeal.util.GlobalService;
import com.citrineplexus.makandeal.view.widget.ProductImageView;

public class ProductVoucherBinder extends AdapterBinder {

	public ProductVoucherBinder(OnListAdapterItemClickListener listener) {
		super(listener);
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.child_category;
	}

	@Override
	public ViewHolder getViewHolder(View v) {
		return new wishListViewHolder(v);
	}

	public static class wishListViewHolder extends AdapterBinder.ViewHolder {

		private ProductImageView productImage;
		private TextView currentPrice, productTitle, location, discount,
				originalPrice, totalSold;

		public wishListViewHolder(View v) {
			super(v);
			productImage = (ProductImageView) v.findViewById(R.id.productImage);
			productTitle = (TextView) v.findViewById(R.id.productTitle);
			currentPrice = (TextView) v.findViewById(R.id.currentPrice);
			location = (TextView) v.findViewById(R.id.location);
			discount = (TextView) v.findViewById(R.id.discount);
			originalPrice = (TextView) v.findViewById(R.id.originalPrice);
			totalSold = (TextView) v.findViewById(R.id.totalSold);

		}

		@Override
		public void binding(int position,BaseEntity entity, ImageLoader imageLoader,
				boolean isFooter) {

			imageLoader.DisplayImage(entity.get("IMAGE_URL", "").toString(),
					productImage, ScaleType.CENTER_CROP, false);

			if (productTitle != null)
				productTitle.setText(entity.get("NAME", "").toString());

			double price = Double.parseDouble(entity.get("PRICE", "")
					.toString());

			if (GlobalService.checkDiscout(entity)) {
				// Deal got discount
				double specialPrice = Double.parseDouble(entity.get(
						"SPECIAL_PRICE", "").toString());

				originalPrice.setPaintFlags(originalPrice.getPaintFlags()
						| Paint.STRIKE_THRU_TEXT_FLAG);
				originalPrice.setVisibility(View.VISIBLE);
				int discountValue = ((int) (100.0D - (specialPrice / price) * 100.0D));
				if (currentPrice != null)
					currentPrice
							.setText("RM " + Calculator.round(specialPrice));

				if (originalPrice != null)
					originalPrice.setText("RM " + Calculator.round(price));

				if (discount != null) {
					discount.setVisibility(View.VISIBLE);
					discount.setText(discountValue + "%OFF");
				}

				if (totalSold != null)
					totalSold.setText("/ " + entity.get("FSF", "0").toString()
							+ " sold");
			} else {
				if (currentPrice != null)
					currentPrice.setText("RM " + Calculator.round(price));

				if (totalSold != null)
					totalSold.setText(entity.get("FSF", "0").toString()
							+ " sold");
			}

			location.setText(entity.get("LOCATION", "0").toString());
		}

	}

}
