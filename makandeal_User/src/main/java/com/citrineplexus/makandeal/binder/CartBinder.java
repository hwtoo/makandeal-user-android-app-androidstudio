package com.citrineplexus.makandeal.binder;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.Spinner;
import android.widget.TextView;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.AbstractRecyclerAdapter.OnListAdapterItemClickListener;
import com.citrineplexus.makandeal.adapter.AdapterBinder;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.util.Calculator;
import com.citrineplexus.makandeal.view.activity.CartView;

public class CartBinder extends AdapterBinder {

	static CartView activity = null;

	private CartHolder cartHolder = null;
	private static boolean removeFlag = false;
	private static float cartDeleteWidth = -1;

	private static int totalChild = 0;

	public CartBinder(OnListAdapterItemClickListener listener, CartView cartView) {
		super(listener);
		activity = cartView;
		cartDeleteWidth = activity.getResources().getDimension(
				R.dimen.cartDeleteWidth);
	}

	public void openDeleteLayout() {
		removeFlag = !removeFlag;
	}

	public void refreshData() {
		removeFlag = false;
	}

	public boolean getRemoveFlag() {
		return removeFlag;
	}

	public void setTotalChild(int totalChild) {
		CartBinder.totalChild = totalChild;
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.child_cart;
	}

	@Override
	public ViewHolder getViewHolder(View v) {
		cartHolder = new CartHolder(v);
		return cartHolder;
	}

	public double getSubTotal() {
		return cartHolder.subtotal;
	}

	public static class CartHolder extends AdapterBinder.ViewHolder implements
			OnItemSelectedListener {
		private TextView productTitle = null, productPrice = null,
				productSubTotal = null;
		private Spinner productQty;
		private double subtotal = 0;
		private int selectedQty = -1;
		private ImageView productImage = null, deleteBtn = null;
		private LinearLayout contentLayout = null;
		private Space space = null;
		private View lastLine = null;

		public double getSubTotal() {
			return this.subtotal;
		}

		public CartHolder(View v) {
			super(v);
			productImage = (ImageView) v.findViewById(R.id.productImage);
			productTitle = (TextView) v.findViewById(R.id.productTitle);
			productPrice = (TextView) v.findViewById(R.id.productPrice);
			productQty = (Spinner) v.findViewById(R.id.productQty);
			productQty.setOnItemSelectedListener(this);
			productSubTotal = (TextView) v.findViewById(R.id.productSubTotal);
			deleteBtn = (ImageView) v.findViewById(R.id.deleteBtn);
			deleteBtn.setOnClickListener(this);
			contentLayout = (LinearLayout) v.findViewById(R.id.contentLayout);
			space = (Space) v.findViewById(R.id.cartSpace);
			lastLine = (View) v.findViewById(R.id.cartLastLine);
		}

		@Override
		public void binding(int position, BaseEntity entity,
				ImageLoader imageLoader, boolean isFooter) {
			if (productImage != null)
				imageLoader.DisplayImage(entity.get("IMAGE_URL").toString(),
						productImage, ScaleType.CENTER_CROP, false);
			if (productTitle != null)
				productTitle.setText(entity.get("NAME").toString());

			double price = Double.parseDouble(entity.get("PRICE", "")
					.toString());
			if (productPrice != null)
				productPrice.setText("RM " + Calculator.round(price));

			int qty = ((Integer) entity.get("QTY"));
			selectedQty = qty - 1;

			if (productQty != null) {
				productQty.setSelection(selectedQty);
				productQty.setTag(selectedQty);
			}

			if (productSubTotal != null)
				productSubTotal.setText("RM "
						+ Calculator.round((subtotal = (price * qty))));

			if (removeFlag) {
				contentLayout.animate().translationX(cartDeleteWidth).start();
			} else {
				if (!removeFlag) {
					contentLayout.animate().translationX(0).start();
				}
			}

			if (position == (totalChild - 1)) {
				if (space != null)
					space.setVisibility(View.GONE);

				if (lastLine != null)
					lastLine.setVisibility(View.GONE);
			}
		}

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			if ((Integer) productQty.getTag() != position) {
				if (CartBinder.activity != null)
					CartBinder.activity.modifyQuantity(getPosition(),
							(String) productQty.getItemAtPosition(position));
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {

		}

	}

}
