package com.citrineplexus.makandeal.binder;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.AbstractRecyclerAdapter.OnListAdapterItemClickListener;
import com.citrineplexus.makandeal.adapter.AdapterBinder;
import com.citrineplexus.makandeal.callback.CouponCallBack;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.service.OAuthAuthorization;
import com.citrineplexus.makandeal.service.ServiceGateway;
import com.citrineplexus.makandeal.util.Calculator;
import com.citrineplexus.makandeal.view.activity.CartView;
import com.citrineplexus.makandeal.view.widget.BoxButton;

public class CartFootBinder extends AdapterBinder {

	private CartFootHolder footerHolder = null;
	private Context context = null;

	private CouponCallBack mCouponCallBack;

	public CartFootBinder(Context context,
			OnListAdapterItemClickListener listener,
			CouponCallBack mCouponCallBack) {
		super(listener);
		this.context = context;
		this.mCouponCallBack = mCouponCallBack;
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.child_cart_footer;
	}

	@Override
	public ViewHolder getViewHolder(View v) {
		footerHolder = new CartFootHolder(context, v, mCouponCallBack);
		return footerHolder;
	}

	public void setCartTotal(String total) {
		if (footerHolder != null)
			footerHolder.setCartTotal(total);
	}

	public double getGrandTotal() {
		if (footerHolder != null)
			return footerHolder.getGrandTotal();
		return -1;
	}

	public void refreshCoupon(BaseEntity entity) {
		if (footerHolder != null)
			footerHolder.refreshCoupon(entity);
	}

	public static class CartFootHolder extends AdapterBinder.ViewHolder
			implements TextWatcher {

		private TextView grandTotal = null, cartSubTotal = null;
		private EditText couponField = null;
		private Button couponBtn = null;
		private boolean isBrowseCoupon = true;
		private Context context = null;
		private LinearLayout couponList = null;

		private CouponCallBack dataRequestlistener;
		private ArrayList<BaseEntity> couponAry = new ArrayList<>();

		public CartFootHolder(Context context, View v,
				CouponCallBack dataRequestlistener) {
			super(v);
			this.context = context;
			grandTotal = (TextView) v.findViewById(R.id.grandTotalText);
			cartSubTotal = (TextView) v.findViewById(R.id.cartSubTotal);
			couponList = (LinearLayout) v.findViewById(R.id.couponList);
			couponField = (EditText) v.findViewById(R.id.couponField);
			couponField.addTextChangedListener(this);
			couponBtn = (Button) v.findViewById(R.id.couponBtn);
			couponBtn.setOnClickListener(this);
			this.dataRequestlistener = dataRequestlistener;
		}

		@Override
		public void binding(int position, BaseEntity entity,
				ImageLoader imageLoader, boolean isFooter) {
		}

		double cartSubTotalAmout = 0;
		double couponDiscountAmount = 0;
		double grandTotalAmount = 0;

		public void setCartTotal(String total) {
			if (cartSubTotal != null)
				cartSubTotal.setText("RM" + total);
			cartSubTotalAmout = Double.parseDouble(total);
			setGrandTotal();
		}

		private void setGrandTotal() {
			grandTotalAmount = cartSubTotalAmout - couponDiscountAmount;
			if (grandTotal != null)
				grandTotal.setText("RM" + Calculator.round(grandTotalAmount));
		}

		public double getGrandTotal() {
			return this.grandTotalAmount;
		}

		public void refreshCoupon(BaseEntity entity) {

			if (couponList != null) {
				if (entity.get("COUPON_CODE") == null) {
					couponList.removeAllViews();
					couponDiscountAmount = 0;
				} else {
					View v = LayoutInflater.from(context).inflate(
							R.layout.child_cart_coupon, null);
					TextView couponCode = (TextView) v
							.findViewById(R.id.couponCode);
					TextView couponAmoutn = (TextView) v
							.findViewById(R.id.couponAmount);
					v.findViewById(R.id.removeCoupon).setOnClickListener(this);
					couponCode.setText("Makandeal Coupon : "
							+ entity.get("COUPON_CODE").toString());
					double discount = Double.parseDouble(entity.get(
							"DISCOUNT_AMOUNT").toString());
					couponAmoutn.setText("-RM" + Calculator.round(discount));
					couponDiscountAmount = discount;
					couponList.addView(v);
				}
				setGrandTotal();
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (couponField.getEditableText().toString().length() > 0) {
				couponBtn.setText("Apply");
				isBrowseCoupon = false;
			} else {
				couponBtn.setText("Browser");
				isBrowseCoupon = true;
			}
		}

		@Override
		public void afterTextChanged(Editable s) {

		}

		@Override
		public void onClick(View v) {
			Bundle extra = new Bundle();
			switch (v.getId()) {
			case R.id.removeCoupon:
				// 1055
				extra.putString("QUOTE_ID", OAuthAuthorization.getInstance()
						.getUserProfileData().get("QUOTE_ID").toString());
				extra.putBoolean("LOADING", true);
				extra.putString("TYPE",
						CartView.refreshMode.REFRESH_COUPON.name());

				if (dataRequestlistener != null)
					dataRequestlistener.couponAction(1055, extra);
				break;
			case R.id.couponBtn:
				if (!isBrowseCoupon) {
					GoogleAnalyticsLib.getInstance(context).trackEvent(
							"Apply Coupon Code", null);

					extra.putString("QUOTE_ID", OAuthAuthorization
							.getInstance().getUserProfileData().get("QUOTE_ID")
							.toString());
					extra.putBoolean("LOADING", true);
					extra.putBoolean("COUPON_ADD", true);
					extra.putString("COUPON_CODE", couponField
							.getEditableText().toString());
					extra.putString("TYPE",
							CartView.refreshMode.REFRESH_COUPON.name());
					if (dataRequestlistener != null)
						dataRequestlistener.couponAction(1054, extra);
				}
				// TODO: tracking for the promo code
				// GoogleAnalyticsLib.getInstance(context).trackEvent(
				// "Apply Promo Code", null);
				break;

			default:
				break;
			}
			super.onClick(v);
		}

	}

}
