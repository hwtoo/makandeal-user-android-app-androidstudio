package com.citrineplexus.makandeal.binder;

import android.view.View;
import android.widget.TextView;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.AbstractRecyclerAdapter.OnListAdapterItemClickListener;
import com.citrineplexus.makandeal.adapter.AdapterBinder;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.util.Calculator;

public class CreditBinder extends AdapterBinder {

	public CreditBinder(OnListAdapterItemClickListener listener) {
		super(listener);
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.child_credit_item;
	}

	@Override
	public ViewHolder getViewHolder(View v) {
		return new CreditViewHolder(v);
	}

	public static class CreditViewHolder extends AdapterBinder.ViewHolder {

		private TextView creditValue = null, creditExpire = null,
				creditType = null, creditCode = null;

		public CreditViewHolder(View v) {
			super(v);
			creditValue = (TextView) v.findViewById(R.id.creditValue);
			creditExpire = (TextView) v.findViewById(R.id.creditExpire);
			creditType = (TextView) v.findViewById(R.id.creditType);
			creditCode = (TextView) v.findViewById(R.id.creditCode);
		}

		@Override
		public void binding(int position,BaseEntity entity, ImageLoader imageLoader,
				boolean isFooter) {

			if (creditValue != null) {
				creditValue.setText("RM"+Calculator.round(Double.valueOf(entity.get(
						"VALUE").toString())));
			}
			if (creditExpire != null) {
				creditExpire.setText(entity.get("EXPIRY").toString());
			}
			if (creditType != null) {
				creditType.setText(entity.get("TYPE").toString());
			}

			if (creditCode != null) {
				creditCode.setText("Coupon Code: "
						+ entity.get("CODE").toString());
			}
		}

	}

}
