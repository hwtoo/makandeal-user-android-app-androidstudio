package com.citrineplexus.makandeal.binder;

import android.graphics.Paint;
import android.view.View;
import android.widget.TextView;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.AbstractRecyclerAdapter.OnListAdapterItemClickListener;
import com.citrineplexus.makandeal.adapter.AdapterBinder;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.util.Calculator;
import com.citrineplexus.makandeal.util.GlobalService;

public class SelectDealBinder extends AdapterBinder {

	public SelectDealBinder(OnListAdapterItemClickListener listener) {
		super(listener);
	}

	public void setOnListAdapterItemClickListener(
			OnListAdapterItemClickListener listener) {
		super.setOnListAdapterItemClickListener(listener);
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.child_select_deal;
	}

	@Override
	public ViewHolder getViewHolder(View v) {
		return new SelectDealViewHolder(v);
	}

	public static class SelectDealViewHolder extends AdapterBinder.ViewHolder {

		private TextView dealTitle = null, dealDiscount = null,
				dealPrice = null, dealOriginalPrice = null;

		public SelectDealViewHolder(View v) {
			super(v);
			dealTitle = (TextView) v.findViewById(R.id.dealTitle);
			dealDiscount = (TextView) v.findViewById(R.id.dealDiscount);
			dealPrice = (TextView) v.findViewById(R.id.dealPrice);
			dealOriginalPrice = (TextView) v
					.findViewById(R.id.dealOriginalPrice);
		}

		@Override
		public void binding(int position, BaseEntity entity,
				ImageLoader imageLoader, boolean isFooter) {

			if (dealTitle != null)
				dealTitle.setText(entity.get("NAME").toString());

			double price = Double.parseDouble(entity.get("PRICE", "0.00")
					.toString());

			if (GlobalService.checkDiscout(entity)) {
				double specialPrice = Double.parseDouble(entity.get(
						"SPECIAL_PRICE", "0.00").toString());

				dealOriginalPrice.setPaintFlags(dealOriginalPrice
						.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
				dealOriginalPrice.setVisibility(View.VISIBLE);

				int discountValue = ((int) (100.0D - (specialPrice / price) * 100.0D));

				if (dealPrice != null)
					dealPrice.setText("RM " + Calculator.round(specialPrice));

				if (dealOriginalPrice != null)
					dealOriginalPrice.setText("RM " + Calculator.round(price));

				if (dealDiscount != null) {
					dealDiscount.setVisibility(View.VISIBLE);
					dealDiscount.setText(discountValue + "%OFF");
				}
			} else {
				if (dealPrice != null)
					dealPrice.setText("RM " + Calculator.round(price));
			}
		}

	}

}
