package com.citrineplexus.makandeal.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.adapter.ExpandableAdapter;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.GoogleAnalyticsLib;
import com.citrineplexus.makandeal.service.OAuthAuthorization;
import com.citrineplexus.makandeal.service.ServiceGateway;
import com.citrineplexus.makandeal.view.MakanDealApplication;

public class GlobalService {

	private static boolean Live_Flag = false;
	private static String apiURL;
	private static String hostName;

	public static void callSupportIntent(Context context, String phoneNumber) {
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setData(Uri.parse("tel:" + Uri.encode(phoneNumber)));
		callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(callIntent);
	}

	public static void AddToWishlist(Context context, BaseEntity entity) {
		if (entity == null)
			return;
		if (!OAuthAuthorization.getInstance().isUserLogin()) {
			Toast.makeText(context, "Please sign in to add to wish list.",
					Toast.LENGTH_SHORT).show();
		} else {
			Bundle param = new Bundle();
			param.putString("ENTITY_ID", entity.get("ENTITY_ID", "").toString());
			param.putString("QTY", "1");
			new ServiceGateway().getData(context, 1041, null, 0, param);
			Toast.makeText(context, "Service voucher added to your wish list.",
					Toast.LENGTH_SHORT).show();
			GoogleAnalyticsLib.getInstance(context).trackEvent(
					"Add to Wish List", null);
		}
	}

	public static void ShareMessage(Context context, String message) {
		Intent localIntent = new Intent();
		localIntent.setAction("android.intent.action.SEND");
		localIntent.putExtra("android.intent.extra.TEXT", message);
		localIntent.setType("text/plain");
		context.startActivity(Intent.createChooser(localIntent, "Bello2"));
	}

	public static boolean checkDiscout(BaseEntity entity) {
		try {
			if (entity.get("SPECIAL_FROM_DATE") == null
					|| entity.get("SPECIAL_TO_DATE") == null
					|| entity.get("SPECIAL_PRICE") == null
					|| entity.get("PRICE") == null)
				return false;

			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			Calendar fromCal = Calendar.getInstance();
			fromCal.setTime(df
					.parse(entity.get("SPECIAL_FROM_DATE").toString()));

			Calendar toCal = Calendar.getInstance();
			toCal.setTime(df.parse(entity.get("SPECIAL_TO_DATE").toString()));

			if (!(Calendar.getInstance().after(fromCal) && Calendar
					.getInstance().before(toCal))) {
				entity.put("DEAL", false);
				return false;
			}
			return true;
		} catch (ParseException e) {
			e.printStackTrace();
			entity.put("DEAL", false);
			return false;
		}
	}

	public static SpannableString convertToBulletText(List<String> ary) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < ary.size(); i++) {
			if (ary.get(i) == null || ary.get(i).equalsIgnoreCase("null")
					|| ary.get(i).trim().equals(""))
				continue;
			String mText = ary.get(i);
			sb.append(mText);
			if ((i + 1) < ary.size())
				sb.append("\n");
		}
		// String[] list = sb.toString().split("\n");
		SpannableString spannable = new SpannableString(sb.toString());
		int spanPosition = 0;
		for (int i = 0; i < ary.size(); i++) {
			// int index = sb.toString().indexOf(text);
			spannable.setSpan(new Bello2BulletSpan(8), spanPosition,
					spanPosition + ary.get(i).length(),
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			spanPosition += ary.get(i).length() + 1;
		}

		return spannable;
	}

	public static boolean isTabletDevice(Context activityContext) {
		// Verifies if the Generalized Size of the device is XLARGE to be
		// considered a Tablet
		boolean xlarge = ((activityContext.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE);

		// If XLarge, checks if the Generalized Density is at least MDPI
		// (160dpi)
		if (xlarge) {
			DisplayMetrics metrics = new DisplayMetrics();
			Activity activity = (Activity) activityContext;
			activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

			// MDPI=160, DEFAULT=160, DENSITY_HIGH=240, DENSITY_MEDIUM=160,
			// DENSITY_TV=213, DENSITY_XHIGH=320
			if (metrics.densityDpi == DisplayMetrics.DENSITY_DEFAULT
					|| metrics.densityDpi == DisplayMetrics.DENSITY_HIGH
					|| metrics.densityDpi == DisplayMetrics.DENSITY_MEDIUM
					|| metrics.densityDpi == DisplayMetrics.DENSITY_TV
					|| metrics.densityDpi == DisplayMetrics.DENSITY_XHIGH) {

				// Yes, this is a tablet!
				return true;
			}
		}
		// No, this is not a tablet!
		return false;
	}

	public static String generateMD5(String s) {
		try {
			MessageDigest digest = java.security.MessageDigest
					.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				byte b = messageDigest[i];
				String hex = Integer.toHexString((int) 0x00FF & b);
				if (hex.length() == 1) {
					sb.append("0");
				}
				sb.append(hex);
			}

			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static boolean isEmailValid(CharSequence email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	public static void emailSupportIntent(Context context) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("message/rfc822");
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { context
				.getResources().getString(R.string.contactUsEmail) });
		context.startActivity(Intent.createChooser(intent, ""));
	}

	public static void checkLive(Context context) {
		apiURL = context.getResources().getString(R.string.apiUrl);
		if (apiURL.contains("http://www"))
			Live_Flag = true;

		int index = apiURL.indexOf(".");
		hostName = apiURL.substring(0, index);
	}

	public static String getHostName() {
		return hostName;
	}

	public static String getNotificationHostNam() {
		if (!Live_Flag)
			return hostName + ".";
		// return "http://staging.";
		return "http://";
	}

	public static boolean isLive() {
		return Live_Flag;
	}

	public static String getApiUrl() {
		return apiURL;
	}

	public static String getWebViewURL() {
		int index = apiURL.indexOf(".");
		if (Live_Flag)
			return "http://m" + apiURL.substring(index, apiURL.length());
		return hostName + ".m" + apiURL.substring(index, apiURL.length());
	}

	public static void setPriceCategory(BaseEntity en) {
		double price = -1;
		if (checkDiscout(en))
			price = Double.parseDouble(en.get("SPECIAL_PRICE", "0.00")
					.toString());
		else
			price = Double.parseDouble(en.get("PRICE", "0.00").toString());
		int priceTag = calculatePrice(1, price);
		en.setFilterID(priceTag);
		MakanDealApplication.getInstance().setFilterPriceCategoryIsAvailable(
				priceTag);
	}

	public static int calculatePrice(int stage, double price) {
		switch (stage) {
		case 1:
			if (price >= 60.00)
				return 110007;

		case 2:
			if (price >= 50.00 && price < 60.00)
				return 110006;

		case 3:
			if (price >= 40.00 && price < 50.00)
				return 110005;

		case 4:
			if (price >= 30.00 && price < 40.00)
				return 110004;

		case 5:
			if (price >= 20.00 && price < 30.00)
				return 110003;

		case 6:
			if (price >= 10.00 && price < 20.00)
				return 110002;

		case 7:
			if (price >= 0.00 && price < 10.00)
				return 110001;

		case 8:
			return -1;
		default:
			return calculatePrice(stage + 1, price);
		}
	}

	public static int setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return 0;
		}

		int totalHeight = 0;
		for (int i = 0, len = listAdapter.getCount(); i < len; i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		// listView.getDividerHeight()获取子项间分隔符占用的高度
		// params.height最后得到整个ListView完整显示需要的高度
		listView.setLayoutParams(params);
		return params.height;
	}

	public static int setExpandableListViewHeightBasedOnChildren(
			ExpandableListView expandableListView) {
		ExpandableAdapter adapter = (ExpandableAdapter) expandableListView
				.getExpandableListAdapter();
		if (adapter == null) {
			return 0;
		}
		int totalHeight = expandableListView.getPaddingTop()
				+ expandableListView.getPaddingBottom();
		for (int i = 0; i < adapter.getGroupCount(); i++) {
			View groupItem = adapter.getGroupView(i, true, null,
					expandableListView);
			groupItem.measure(View.MeasureSpec.UNSPECIFIED,
					View.MeasureSpec.UNSPECIFIED);
			totalHeight += groupItem.getMeasuredHeight();

			for (int j = 0; j < adapter.getChildrenCount(i); j++) {
				View listItem = adapter.getChildView(i, j, false, null,
						expandableListView);
				listItem.setLayoutParams(new LayoutParams(
						View.MeasureSpec.UNSPECIFIED,
						View.MeasureSpec.UNSPECIFIED));
				listItem.measure(View.MeasureSpec.makeMeasureSpec(0,
						View.MeasureSpec.UNSPECIFIED), View.MeasureSpec
						.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
				totalHeight += listItem.getMeasuredHeight();
			}
		}

		ViewGroup.LayoutParams params = expandableListView.getLayoutParams();
		int height = totalHeight + expandableListView.getDividerHeight()
				* (adapter.getGroupCount() - 1);

		if (height < 10)
			height = 100;
		return height;
	}

}
