package com.citrineplexus.makandeal.util;

import java.util.ArrayList;
import java.util.List;

import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.view.MakanDealApplication;

public class CustomFilter {
	public static int featureCount = 1;

	public static boolean filterOption(int contentID, int filterBy,
			BaseEntity entity) {
		// if (contentID != 1003) {
		// featureCount = 1;
		// if (entity.get("FEATURE") != null)
		// entity.remove("FEATURE");
		// }

		if (contentID != 1007 && contentID != 1008 && contentID != 1009) {
			if (!filterPrice(entity))
				return false;
			if (!filterRegion(entity))
				return false;
		}

		try {
			switch (contentID) {
			case -1000:// All Deal
				return true;
			case 1000:// HISTORY
				break;
			case 1001:// GIFT VOUCHER
				break;
			case 1002:// DEAL
				return GlobalService.checkDiscout(entity);
				// case 1003:// FEATURED
				// if (entity.get("FEATURE") != null
				// && (Boolean) entity.get("FEATURE"))
				// break;
				//
				// if (featureCount <= 15) {
				// if (GlobalService.checkDiscout(entity)) {
				// entity.put("FEATURE", true);
				// featureCount++;
				// return true;
				// } else
				// return false;
				// } else
				// return false;
			case 1004:// CATEGORIES
				break;
			case 1005:// 2HER
				if (entity.get("BELLO_SERVICE_2HIM_2HER", "").toString()
						.equals("4")
						|| entity.get("BELLO_SERVICE_2HIM_2HER", "").toString()
								.equals("5"))
					return false;
				break;
			case 1006:// 2HIM
				if (entity.get("BELLO_SERVICE_2HIM_2HER", "").toString()
						.equals("3")
						|| entity.get("BELLO_SERVICE_2HIM_2HER", "").toString()
								.equals("5"))
					return false;
				break;
			case 1007:// EXPIRED SOON
				if (entity.get("STATUS") == null
						|| entity.get("STATUS").equals("R")
						|| entity.get("STATUS").equals("S")
						|| entity.get("STATUS").equals("L")
						|| entity.get("STATUS").equals("E")) {
					return false;
				}
				break;
			case 1008:// AVAILABLE
				return true;
			case 1009:// REDEEM
				if (entity.get("STATUS") == null
						|| (!entity.get("STATUS").equals("R")
								&& !entity.get("STATUS").equals("S") && !entity
								.get("STATUS").equals("L")))
					return false;
				break;

			case 99:
				ArrayList<String> mIds = (ArrayList<String>) entity.get(
						"CATEGORY_IDS", "");
				return checkFilterType(mIds, contentID,
						String.valueOf(filterBy));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	public static boolean checkFilterType(ArrayList<String> mAry,
			int contentID, String filterBy) {
		boolean returnFlag = false;
		for (int i = 0; i < mAry.size(); i++) {
			if (mAry.get(i).equals(filterBy)) {
				returnFlag = true;
				break;
			}
		}
		return returnFlag;
	}

	private static boolean filterPrice(BaseEntity entity) {
		List<BaseEntity> priceList = MakanDealApplication.getInstance()
				.getFilterPriceList();
		boolean isPrice = true;
		for (BaseEntity en : priceList) {
			if (en.getCheckBoxFlag()) {
				if (entity.getFilterID() != en.getFilterID()) {
					isPrice = false;
				} else
					return true;
			}
		}
		return isPrice;
	}

	private static boolean filterRegion(BaseEntity entity) {
		ArrayList<String> mIds = (ArrayList<String>) entity
				.get("REGION_ID", "");

		List<BaseEntity> regionList = MakanDealApplication.getInstance()
				.getFilterRegionList();

		boolean inRegion = true;
		for (String regionID : mIds)
			for (BaseEntity en : regionList) {
				if (en.getCheckBoxFlag())
					if (!regionID.equals(String.valueOf(en.getFilterID()))) {
						inRegion = false;
					} else
						return true;
			}

		return inRegion;
	}

	// private static boolean checkFeature(BaseEntity entity) {
	// if(checkFeature>15)
	// return false;
	// else
	// return Go
	// }
}
