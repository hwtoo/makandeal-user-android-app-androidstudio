package com.citrineplexus.makandeal.util;

import java.util.List;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

import com.citrineplexus.makandeal.view.MakanDealApplication;

public class SearchContentProvider extends ContentProvider {

	private static final String[] COLUMNS = { "_id", // must include this column
			SearchManager.SUGGEST_COLUMN_TEXT_1 };
	public MatrixCursor cursor = new MatrixCursor(COLUMNS);

	@Override
	public boolean onCreate() {
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		String searchWord = uri.getLastPathSegment();
		if (!searchWord.equalsIgnoreCase("search_suggest_query")) {
			List<String> termList = MakanDealApplication.getInstance()
					.getTermsList();
			MatrixCursor matrixCursor = new MatrixCursor(COLUMNS, 1);
			for (String termText : termList) {
				if (termText.toLowerCase().startsWith(searchWord.toLowerCase())) {
					matrixCursor.addRow(new String[] { "1", termText});
				}
			}
			return matrixCursor;
		}
		// MatrixCursor returnMatrix = cursor;
		// cursor = new MatrixCursor(COLUMNS);
		// return returnMatrix;
		return null;
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		return null;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		return 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		return 0;
	}

}
