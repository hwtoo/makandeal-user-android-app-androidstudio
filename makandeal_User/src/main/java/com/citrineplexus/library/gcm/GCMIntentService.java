package com.citrineplexus.library.gcm;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.citrineplexus.makandeal.R;
import com.citrineplexus.makandeal.view.activity.HomePage;
import com.citrineplexus.makandeal.view.activity.SplashPage;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GCMIntentService extends IntentService {

	public GCMIntentService() {
		super("makandeal Daily News");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				Log.d("makandeal", "GCM Error = " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				Log.d("makandeal", "GCM Deleted = " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {
				try {
					sendNotification(extras);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		GCMReceiver.completeWakefulIntent(intent);
	}

	private void sendNotification(Bundle bundle) throws MalformedURLException,
			IOException {
		// ************** RemoteView Notification ********************
		Bitmap remote_picture = BitmapFactory
				.decodeStream((InputStream) new URL(bundle.getString(
						"productImageUrl", "")).getContent());

		Spanned productName = Html.fromHtml(bundle.getString("productName",
				"makandeal Coupon"));

		RemoteViews contentView = new RemoteViews(getPackageName(),
				R.layout.notification_layout);
		contentView.setTextViewText(R.id.notifTitle, productName);
		contentView.setTextViewText(R.id.notifDiscount,
				"RM" + bundle.getString("productSpecialPrice", "0.00"));
		String productPrice = bundle.getString("productPrice", "");
		if (!productPrice.equals(""))
			contentView.setViewVisibility(R.id.notifOriginal, View.GONE);
		contentView.setTextViewText(R.id.notifOriginal, "RM" + productPrice);
		contentView.setImageViewBitmap(R.id.notifImage, remote_picture);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.icon_notofication)
				.setColor(getResources().getColor(R.color.colorPrimary))
				.setAutoCancel(true);

		Intent resultIntent = new Intent(this, SplashPage.class);
		resultIntent.putExtra("GCM_MESSAGE", true);
		resultIntent.putExtra("ENTITY_ID", bundle.getString("productId", "0"));
		resultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Intent.FLAG_ACTIVITY_CLEAR_TOP);

		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		stackBuilder.addParentStack(HomePage.class);
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);

		builder.setPriority(Notification.PRIORITY_MAX);
		builder.setContentIntent(resultPendingIntent);
		// builder.setLargeIcon(BitmapFactory.decodeResource(getResources(),
		// R.drawable.icon_notofication));
		builder.setColor(getResources().getColor(R.color.colorPrimary));

		builder.setContentTitle("makandeal");
		builder.setContentText(productName);

		Notification foregroundNote = builder.build();
		foregroundNote.bigContentView = contentView;
		foregroundNote.defaults |= Notification.DEFAULT_ALL;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		mNotificationManager.notify(9876, foregroundNote);
	}
}
