package com.citrineplexus.library.gcm;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.citrineplexus.library.webservice.OnDataRequestListener;
import com.citrineplexus.makandeal.entity.BaseEntity;
import com.citrineplexus.makandeal.service.OAuthAuthorization;
import com.citrineplexus.makandeal.service.ServiceGateway;
import com.citrineplexus.makandeal.util.GlobalService;
import com.citrineplexus.makandeal.view.MakanDealApplication;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GCMRegisterHandler implements OnDataRequestListener {

	public final String PROPERTY_REG_ID = "registration_id";
	private final String PROPERTY_APP_VERSION = "appVersion";
	private final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	private Context context = null;
	private GoogleCloudMessaging gcm;

	private final String DEV_SENDER_ID = "20025551271";// testing sender id
	private final String LIVE_SENDER_ID = "812408738645";// live sender id
	private final String accessToken = "sjhdaphf32454jhsdjhfasg";
	private String regid;

	public GCMRegisterHandler() {
	}

	public void initAppWithGoogle(Activity activity) {
		this.context = activity.getApplicationContext();

		Thread mthread = null;
		if (checkPlayServices(activity)) {
			gcm = GoogleCloudMessaging.getInstance(activity);
			regid = getRegistrationId(activity);

			// if (regid.isEmpty() || regid.equals("")) {
			mthread = new Thread(getGCMDeviceIDFromGoogle);
			mthread.start();
			// }
		}
	}

	public void registerApp(Activity activity) throws Exception {
		if (context == null)
			this.context = activity.getApplicationContext();
		regid = getRegistrationId(activity);
		initAppWithGoogle(activity);
	}

	public void updateDeviceIDwithProfile(Activity activity) throws Exception {
		if (context == null)
			this.context = activity.getApplicationContext();
		boolean tablet = GlobalService.isTabletDevice(activity);
		postDeviceIDToServer(tablet);
	}

	public void postDeviceIDToServer(boolean tabletFlag) throws Exception {
		Bundle bundle = new Bundle();

		if (regid == null)
			regid = getRegistrationId(context);
		System.out.println("%%%%%% regid ="+regid);
		bundle.putString("DEVICE_ID", regid);
		bundle.putBoolean("REGISTER_ACTION", true);

		if (tabletFlag)
			bundle.putInt("DEVICE_PLATFORM", 4);
		else
			bundle.putInt("DEVICE_PLATFORM", 1);

		if (OAuthAuthorization.getInstance().isUserLogin()
				&& OAuthAuthorization.getInstance().getUserProfileData() != null) {
			bundle.putString("USER_EMAIL", (String) OAuthAuthorization
					.getInstance().getUserProfileData().get("EMAIL"));
			bundle.putString("CUSTOMER_ID", (String) OAuthAuthorization
					.getInstance().getUserProfileData().get("ID"));
		}
		bundle.putString("HEADER_KEYNAME", accessToken);
		new ServiceGateway().getData(context, 1033, this, 0, bundle);
		// ViewController.getInstance(context).getServiceData(1033,
		// GCMRegisterHandler.this, 0, bundle);
	}

	public void unregisterAppToServer(Activity activity) {
		Bundle bundle = new Bundle();
		regid = getRegistrationId(activity);
		bundle.putBoolean("REGISTER_ACTION", false);
		bundle.putString("DEVICE_ID", regid);
		bundle.putString("HEADER_KEYNAME", accessToken);
		new ServiceGateway().getData(activity, 1034, this, 0, bundle);
		// ViewController.getInstance(activity).getServiceData(1034,
		// GCMRegisterHandler.this, 0, bundle);
	}

	private Runnable getGCMDeviceIDFromGoogle = new Runnable() {

		@Override
		public void run() {
			try {
				if (gcm == null) {
					gcm = GoogleCloudMessaging.getInstance(context);
				}
				if (GlobalService.isLive())
					regid = gcm.register(LIVE_SENDER_ID);
				else
					regid = gcm.register(DEV_SENDER_ID);
				boolean tablet = GlobalService.isTabletDevice(context);
				Message msg = new Message();
				msg.what = 0;
				msg.obj = tablet;
				handler.sendMessage(msg);
			} catch (IOException ex) {
				ex.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			try {
				postDeviceIDToServer((Boolean) msg.obj);
			} catch (Exception e) {
				e.printStackTrace();
			}
			super.handleMessage(msg);
		}

	};

	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = MakanDealApplication.getInstance()
				.getPreferences();
		int appVersion = getAppVersion(context);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	private boolean checkPlayServices(Activity activity) {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(activity);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				return false;
			}
		}
		return true;
	}

	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = MakanDealApplication.getInstance()
				.getPreferences();
		String registrationId = prefs.getString(PROPERTY_REG_ID, "");

		if (registrationId.isEmpty() || registrationId.equals("")) {
			// Log.i(TAG, "Registration not found.");
			return "";
		}
		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			// Log.i(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	@Override
	public void onAquireResult(BaseEntity entity, int position, Bundle params) {

		if (params != null && params.getBoolean("REGISTER_ACTION"))
			if (entity != null) {
				BaseEntity mEntity = (BaseEntity) entity.get("ENVELOP");
				if (mEntity != null
						&& ((String) mEntity.get("STATUSMESSAGE"))
								.equalsIgnoreCase("Successful")) {
					storeRegistrationId(context, regid);
				}
			}
	}

	@Override
	public void onConnectionError(Exception ex) {
		// TODO Auto-generated method stub

	}
}
