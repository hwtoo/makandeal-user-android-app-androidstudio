package com.citrineplexus.library.slideshow;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.citrineplexus.library.imageloader.ImageLoader;
import com.citrineplexus.makandeal.R;

public class InfinitePagerAdapter extends PagerAdapter {

	private List<String> mSlideShowURLAry = null;
	private Context mContext;
	private OnSlideShowIitemClickListener onItemClick = null;

	public InfinitePagerAdapter(Context context) {
		mContext = context;
		mSlideShowURLAry = new ArrayList<String>();
	}

	public void setOnSlideShowIitemClickListener(
			OnSlideShowIitemClickListener onItemClick) {
		this.onItemClick = onItemClick;
	}

	public void addData(String data) {
		mSlideShowURLAry.add(data);
	}

	public int getDataSize() {
		return mSlideShowURLAry.size();
	}

	public void cleanUp() {
		mSlideShowURLAry.clear();
	}

	public boolean isEmpty() {
		return mSlideShowURLAry.isEmpty();
	}

	@Override
	public int getCount() {
		if (mSlideShowURLAry.size() > 1)
			return Integer.MAX_VALUE;
		else
			return mSlideShowURLAry.size();
	}

	// protected int getRealCount() {
	// return mSlideShowURLAry.size();
	// }

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		View view = inflater
				.inflate(R.layout.child_slideshow, container, false);

		ImageView mImageView = (ImageView) view
				.findViewById(R.id.slideshowImage);

//		final int itemPosition = position;
		final int virtualPosition = position % getDataSize();

		mImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (onItemClick != null)
					onItemClick.onSlideItemClick(v, virtualPosition);

			}
		});
		if (!mSlideShowURLAry.isEmpty()) {
//			int virtualPosition = position % getDataSize();
			ImageLoader.getInstance(mContext).DisplayImage(
					mSlideShowURLAry.get(virtualPosition), mImageView,
					ScaleType.CENTER_CROP, false);
		}
		((ViewPager) container).addView(mImageView);
		return mImageView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		View view = (View) object;
		((ViewPager) container).removeView(view);
		view = null;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((ImageView) object);
	}

}
