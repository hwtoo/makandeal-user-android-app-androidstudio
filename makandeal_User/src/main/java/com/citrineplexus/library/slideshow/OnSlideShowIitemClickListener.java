package com.citrineplexus.library.slideshow;

import android.view.View;

public interface OnSlideShowIitemClickListener {
	public void onSlideItemClick(View v, int position);
	//TODO: backup for stage 1
	public void onItemClick(View v, int position);
}
