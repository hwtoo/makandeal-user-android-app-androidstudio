package com.citrineplexus.library.slideshow;

import android.content.Context;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class SlideShowViewPager extends ViewPager implements OnTouchListener {

	private InfinitePagerAdapter mAdapter = null;

	private static final long ANIM_VIEWPAGER_DELAY = 5000;
	private static final long ANIM_VIEWPAGER_DELAY_USER_VIEW = 10000;
	private boolean stopSliding = false;
	private float dx1, dx2;

	private Runnable animateViewPager;
	private Handler handler;

	private int dataSize = Integer.MAX_VALUE;

	public SlideShowViewPager(Context context) {
		super(context);
		init(context);
	}

	public SlideShowViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public void onResume() {
		if (mAdapter.getDataSize() > 1) {
			runnable(mAdapter.getDataSize());
			// Re-run callback
			handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
		}
	}
	
	public int getDataSize(){
		return mAdapter.getDataSize();
	}

	public void onPause() {
		if (handler != null) {
			// Remove callback
			handler.removeCallbacks(animateViewPager);
		}
	}

	private void init(Context context) {
		if (mAdapter == null)
			mAdapter = new InfinitePagerAdapter(context);
		setAdapter(mAdapter);
		setClipToPadding(false);
		setOnTouchListener(this);
	}

	public void setOnSlideShowIitemClickListener(
			OnSlideShowIitemClickListener onSlideShowClickListener) {
		mAdapter.setOnSlideShowIitemClickListener(onSlideShowClickListener);
	}

	public void addData(String imageUrl) {
		mAdapter.addData(imageUrl);
	}

	public int getmAdapterSize() {
		return mAdapter.getDataSize();
	}
	
	public void cleanUp(){
		if (!mAdapter.isEmpty())
			mAdapter.cleanUp();
	}
	
	public boolean isEmpty(){
		return mAdapter.isEmpty();
	}

	public void notifyDataSetChanged() {
		runnable(dataSize);
		mAdapter.notifyDataSetChanged();
		if (mAdapter.getDataSize() > 1) {
			setCurrentItem(mAdapter.getDataSize() * 100);
			handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
		} else
			setOnTouchListener(null);
	}

	public void runnable(final int size) {
		handler = new Handler();
		animateViewPager = new Runnable() {
			public void run() {
				if (!stopSliding) {
					changeSlideShow(false);
					// if (mViewPager.getCurrentItem() == size - 1) {
					// mViewPager.setCurrentItem(0);
					// } else {
					// mViewPager.setCurrentItem(
					// mViewPager.getCurrentItem() + 1, true);
					// }
					handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
				}
			}
		};
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		v.getParent().requestDisallowInterceptTouchEvent(true);
		switch (event.getAction()) {

		case MotionEvent.ACTION_CANCEL:
			break;

		case MotionEvent.ACTION_UP:
			v.performClick();
			boolean toRight = false;
			dx2 = event.getX();
			float deltaX = dx2 - dx1;
			if (Math.abs(deltaX) > 100) {
				// Left to Right swipe action
				if (dx2 > dx1) {
					toRight = true;
				}

				// Right to left swipe action
				else {
					toRight = false;
				}

			}
			// calls when touch release on ViewPager
			if (mAdapter != null && dataSize != 0) {
				// if (stopSliding) {
//				changeSlideShow(toRight);
				stopSliding = false;
				// } else {
				// runnable(mSlideShowURLAry.size());
				// }
				// if (mViewPager.getCurrentItem() == mSlideShowURLAry
				// .size() - 1) {
				// mViewPager.setCurrentItem(0);
				// } else {
				// if (toRight)
				// mViewPager.setCurrentItem(
				// mViewPager.getCurrentItem() + 1, true);
				// else
				// mViewPager.setCurrentItem(
				// mViewPager.getCurrentItem() - 1, true);
				// }

				runnable(dataSize);
				if (mAdapter.getDataSize() > 1)
					handler.postDelayed(animateViewPager,
							ANIM_VIEWPAGER_DELAY_USER_VIEW);
			}
			break;

		case MotionEvent.ACTION_DOWN:
			dx1 = event.getX();
			break;

		case MotionEvent.ACTION_MOVE:
			// calls when ViewPager touch
			if (handler != null && stopSliding == false) {
				stopSliding = true;
				handler.removeCallbacks(animateViewPager);
			}
			break;
		}
		return false;
	}

	private void changeSlideShow(boolean toRight) {
		if (!toRight) {
			if (getCurrentItem() == dataSize - 1) {
				setCurrentItem(0);
			} else {
				setCurrentItem(getCurrentItem() + 1, true);
			}
		} else {
			if (getCurrentItem() == 0) {
				setCurrentItem(dataSize - 1);
			} else {
				setCurrentItem(getCurrentItem() - 1, true);
			}
		}
	}
}
