package com.citrineplexus.library.gallery;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.citrineplexus.library.slideshow.InfinitePagerAdapter;
import com.citrineplexus.library.slideshow.OnSlideShowIitemClickListener;
import com.citrineplexus.library.slideshow.SlideShowViewPager;
import com.citrineplexus.makandeal.R;

public class GalleryLooping extends LinearLayout implements
		ViewPager.OnPageChangeListener {

	// private ViewPager mViewPager = null;
	private SlideShowViewPager mViewPager = null;
	private CirclePageIndicator pageIndicator = null;
	// private ArrayList<String> imgData = new ArrayList<String>();
//	private InfinitePagerAdapter mAdapter = null;

	public GalleryLooping(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context);
	}

	public GalleryLooping(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public GalleryLooping(Context context) {
		super(context);
		init(context);
	}

	public void init(Context context) {
		View view = LayoutInflater.from(context).inflate(
				R.layout.gallery_looping, null);

//		mAdapter = new InfinitePagerAdapter(context);
		mViewPager = (SlideShowViewPager) view
				.findViewById(R.id.galleryViewPager);
//		mViewPager.setAdapter(mAdapter);
		mViewPager.setOnPageChangeListener(this);
		pageIndicator = (CirclePageIndicator) view
				.findViewById(R.id.circlePageIndicator);
		pageIndicator.setViewPager(mViewPager);
		pageIndicator.setOnPageChangeListener(this);
		addView(view);
	}

	public void setOnSlideShowIitemClickListener(
			OnSlideShowIitemClickListener onSlideShowClickListener) {
		mViewPager.setOnSlideShowIitemClickListener(onSlideShowClickListener);
	}

	public void addData(String imageUrl) {
		mViewPager.addData(imageUrl);
	}

	// public void addImage(String url) {
	// this.mAdapter.addData(url);
	// }

	public void cleanUp() {
		// if (!mAdapter.isEmpty())
		// mAdapter.cleanUp();
		if (!mViewPager.isEmpty())
			mViewPager.cleanUp();
	}

	public void notifyDataChanged() {
		mViewPager.notifyDataSetChanged();
		pageIndicator.setTotalImageSize(mViewPager.getDataSize());
		mViewPager.setCurrentItem(mViewPager.getDataSize() * 100);
//		pageIndicator.setTotalImageSize(mAdapter.getDataSize());
//		mViewPager.setCurrentItem(mAdapter.getDataSize() * 100);
		pageIndicator.notifyDataSetChanged();
	}

	// class GalleryAdapter extends PagerAdapter {
	//
	// LayoutInflater mLayoutInflater = null;
	//
	// public GalleryAdapter(Context mContext) {
	// mLayoutInflater = (LayoutInflater) mContext
	// .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	// }
	//
	// @Override
	// public int getCount() {
	// return imgData.size();
	// }
	//
	// @Override
	// public View instantiateItem(ViewGroup container, final int position) {
	// View view = mLayoutInflater.inflate(R.layout.slideshow, container,
	// false);
	// ImageView mImageView = (ImageView) view
	// .findViewById(R.id.slideshowImage);
	//
	// ImageLoader.getInstance(getContext()).DisplayImage(
	// imgData.get(position), mImageView, ScaleType.CENTER_CROP,
	// false);
	// container.addView(view);
	// return view;
	// }
	//
	// @Override
	// public void destroyItem(ViewGroup container, int position, Object object)
	// {
	// container.removeView((View) object);
	// }
	//
	// @Override
	// public boolean isViewFromObject(View view, Object object) {
	// return view == object;
	// }
	//
	// }

	@Override
	public void onPageScrollStateChanged(int arg0) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	@Override
	public void onPageSelected(int position) {

	}

}
